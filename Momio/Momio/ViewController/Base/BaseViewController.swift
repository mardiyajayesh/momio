//
//  BaseViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 01/10/21.
//

import UIKit
import MBProgressHUD

class BaseViewController: UIViewController {
    
    // MARK:- IBOutlets
    @IBOutlet weak var labelErrorMessage: DesignableLabel?
    @IBOutlet weak var viewMainBottomConstrant: NSLayoutConstraint!
    
    // MARK:- Properties -
    var imagePicker = UIImagePickerController()
    let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    
    // MARK:- View Life cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        
        // Add Notification whenever Keyboard Hides
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
        
        self.setupProgressHud()
    }
    
    // MARK:- View End Editing -
    func keyboardDismissAndHideError() {
        self.view.endEditing(true)
    }
    
    // MARK:- keyboard Will Show -
    @objc func keyboardWillShow(_ notification: Notification) {
        self.animateWithKeyboard(notification: notification) {
            (keyboardFrame) in
            let constant = keyboardFrame.height - 20
            self.viewMainBottomConstrant?.constant = constant
        }
    }
    
    // MARK:- keyboard Will Hide -
    @objc func keyboardWillHide(_ notification: Notification) {
        animateWithKeyboard(notification: notification) {
            (keyboardFrame) in
            self.viewMainBottomConstrant?.constant = 0
        }
    }
    
    // MARK:- get Screen Size -
    func getScreenSize() -> CGRect {
        let screenSize: CGRect = UIScreen.main.bounds
        return screenSize
    }
    
    func animateWithKeyboard(
        notification: Notification,
        animations: ((_ keyboardFrame: CGRect) -> Void)?
    ) {
        // Extract the duration of the keyboard animation
        let durationKey = UIResponder.keyboardAnimationDurationUserInfoKey
        let duration = notification.userInfo![durationKey] as! Double
        
        // Extract the final frame of the keyboard
        let frameKey = UIResponder.keyboardFrameEndUserInfoKey
        let keyboardFrameValue = notification.userInfo![frameKey] as! NSValue
        
        // Extract the curve of the iOS keyboard animation
        let curveKey = UIResponder.keyboardAnimationCurveUserInfoKey
        let curveValue = notification.userInfo![curveKey] as! Int
        let curve = UIView.AnimationCurve(rawValue: curveValue)!
        
        // Create a property animator to manage the animation
        let animator = UIViewPropertyAnimator(
            duration: duration,
            curve: curve
        ) {
            // Perform the necessary animation layout updates
            animations?(keyboardFrameValue.cgRectValue)
            
            // Required to trigger NSLayoutConstraint changes
            // to animate
            self.view?.layoutIfNeeded()
        }
        
        // Start the animation
        animator.startAnimation()
    }
    
    // MARK:- Email Validation -
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    // MARK:- Password Validation -
    public func isValidPassword(password: String) -> Bool {
        
        let passwordRegex = "(?=.*[$@$!%*?&#])[A-Za-z\\d$@$!%*?&#]{6,}"//"^(?=.*[$@$!%*#?&])(?=.{6})$"//(?=.{6,})$"//"^(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{6,}$"
        let passwordtesting = NSPredicate(format: "SELF MATCHES %@", passwordRegex)
        return passwordtesting.evaluate(with: password)
    }
    
    // MARK:- showCustomAlertMethod -
    func showCustomAlertAction(title: String,
                               message: String = "",
                               buttonType: AlertButtonType = .yesButton,
                               from vc: UIViewController,
                               topImageName: String = "ic_mail_alert",
                               buttonOneTitleText: String = GeneralConstants.ok,
                               buttonOneTitleColor: UIColor = UIColor.white,
                               buttonOneBGColor: UIColor = UIColor(named: "ThemButtonBGBlueColor") ?? .blue,
                               buttonTwoTitleText: String = "",
                               buttonTwoTitleColor: UIColor = UIColor.white,
                               buttonTwoBGColor: UIColor = UIColor(named: "ThemButtonBGBlueColor") ?? .blue,
                               isCloseButtonHide: Bool = true) {
        
        var customAlertActions: [CustomAlertAction] = []
        
        if buttonType == .bothButton {
            let yesAction = CustomAlertAction(title: buttonOneTitleText)
            let noAction = CustomAlertAction(title: buttonTwoTitleText, style: .cancel)
            customAlertActions.append(yesAction)
            customAlertActions.append(noAction)
        }else if buttonType == .yesButton {
            let yesAction = CustomAlertAction(title: buttonOneTitleText)
            customAlertActions.append(yesAction)
        }else if buttonType == .noButton {
            let noAction = CustomAlertAction(title: buttonTwoTitleText, style: .cancel)
            customAlertActions.append(noAction)
        }
        
        CustomeAlertView.instance.showCustomAlertWithActions(title: title, message: message, actions: customAlertActions, from: self, topImageName: topImageName, buttonOneTitleColor: buttonOneTitleColor, buttonOneBGColor: buttonOneBGColor, buttonTwoTitleColor: buttonTwoTitleColor, buttonTwoBGColor: buttonTwoBGColor, isCloseButtonHide: isCloseButtonHide)
        CustomeAlertView.instance.delegate = self
    }
    
    /**
     Custome Alert Ok Click
     */
    func buttonTapAlertOk() {
        
    }
    /**
     Custome Alert Cancel Click
     */
    func buttonTapAlertCancel() {
        
    }
    
    // MARK:- BackButtonAction
    @IBAction func buttonTapBack(_ sender: DesignableButton) {
        self.goBack()
    }
    /**
     go Back
     */
    func goBack() {
        
        if isModal {//self.navigationController?.presentingViewController != nil
            // Navigation controller is being presented modally
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    /**
     go To Root View
     */
    func goToRootView() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    // MARK:- OpenPhotoGalleryActionSheet -
    @IBAction func buttonTapProfile(_ sender: DesignableButton) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender
            alert.popoverPresentationController?.sourceRect = sender.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    /**
     open Camera
     */
    func openCamera() {
        
        if (UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            self.showCustomAlertAction(title: GeneralConstants.appName, message: Validation.noCamera, buttonType: .yesButton, from: self, buttonOneTitleText: GeneralConstants.ok)
        }
    }
    /**
     open gallary
     */
    func openGallary() {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
    /**
     open Web View With Title and Url
     */
    func openWebViewWith(title: String, strUrl: String) {
        let destination = UIStoryboard(name: "WebViewController", bundle: nil)
        let initialViewController = destination.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        initialViewController.strTitle = title
        initialViewController.strUrl = strUrl
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    
    func openShareDocumentController(message: String) {
        let fullText = message
        // text to share
        let text = fullText
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
}

// MARK:- ProgressHUD -
extension BaseViewController {
    
    func setupProgressHud() {}
    
    func showProgressHud(text: String = "Please wait...") {
        let indicator = MBProgressHUD.showAdded(to: self.view, animated: true)
        indicator.label.text = text
        indicator.isUserInteractionEnabled = false
        indicator.detailsLabel.text = ""
        indicator.show(animated: true)
    }
    
    func hideProgressHud() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}

// MARK:- CustomeAlertViewDelegate -
extension BaseViewController: CustomeAlertViewDelegate {
    
    func didTapOnOk(action: String) {
        self.buttonTapAlertOk()
    }
    
    func didTapOnCancel(action: String) {
        self.buttonTapAlertCancel()
    }
}
