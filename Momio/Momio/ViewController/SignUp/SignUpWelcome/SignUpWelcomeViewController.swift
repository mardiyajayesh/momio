//
//  SignUpWelcomeViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 07/10/21.
//

import UIKit
import Swinject
import RxSwift

class SignUpWelcomeViewController: BaseViewController {
    // MARK:- IBOutlet -
    @IBOutlet weak var labelTitle: DesignableLabel!
    @IBOutlet weak var imageViewProfile: DesignableImageView!
    @IBOutlet weak var labelPopUpTitle: DesignableLabel!
    @IBOutlet weak var textFieldFeeling: UITextField!
    @IBOutlet weak var buttonYeah: DesignableButton!
    
    // MARK:- Properties -
    public var signUp = SignUp()
    // ViewModel
    private var viewModel: SignUpWelcomeViewModel!
    // Bag
    private let bag = DisposeBag()
    
    // Swinject Initializer for the SignUpWelcomeViewController
    func configure(with ViewModel: SignUpWelcomeViewModel) {
        self.viewModel = ViewModel
    }
    // MARK:- View Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textFieldFeeling.becomeFirstResponder()
        //ViewModelConfig
        self.viewModel.signUpWelcomeViewStateObservable()
            .subscribe(onNext: { [weak self] viewState in
                self?.render(viewState: viewState)
            }, onError: { error in
                
            }).disposed(by: self.bag)
        //        self.viewModel.getLiveTvData(action: "get-timeline-post", page: 1)//, murchant_id: "", userType: ""
        
    }
}

// MARK:- Custom Methods -
extension SignUpWelcomeViewController {
    /**
     For Get Api Response
     */
    func render(viewState: SignUpWelcomeViewState) {
        if viewState.isLoading {
            self.showProgressHud(text: "Loading...")
        } else {
            self.hideProgressHud()
        }
        guard let status = viewState.status else  { return }
        switch (status) {
        case .success:
            print("success")
            if let model = viewState.apiDataModel, let data = model.data {
                print("Data: \(data)")
                UserDefaults.standard.setValue("1", forKey: UserDefaultKeysName.isLogin)
                let destination = UIStoryboard(name: "HighLightListViewController", bundle: nil)
                let initialViewController = destination.instantiateViewController(withIdentifier: "HighLightListViewController") as! HighLightListViewController
                self.navigationController?.pushViewController(initialViewController, animated: true)
            }
        case .failure:
            print("failure")
            
        case .error( _):
            print("error")
            
        }
    }
}


// MARK:- Button Actions -
extension SignUpWelcomeViewController {
    @IBAction func buttonTapYeah(_ sender: DesignableButton) {
        if self.textFieldFeeling.text?.count == 0 {
            
        } else {
            self.signUp.feeling = self.textFieldFeeling.text ?? ""
            self.viewModel.register(email: self.signUp.email ?? "", password: self.signUp.password ?? "", name: self.signUp.name ?? "", dob: self.signUp.dob ?? "", profilePic: self.signUp.profilePic ?? "", feeling: self.textFieldFeeling.text ?? "")
            
        }
    }
}

// MARK:- TextField Delegates -
extension SignUpWelcomeViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.keyboardDismissAndHideError()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {}
    
}

// MARK:- Assemble Registration
class SignUpWelcomeViewControllerAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.storyboardInitCompleted(SignUpWelcomeViewController.self) { (resolver, viewController) in
            
            let viewModel = resolver.resolve(SignUpWelcomeViewModel.self)!
            viewController.configure(with: viewModel)
        }
    }
}


