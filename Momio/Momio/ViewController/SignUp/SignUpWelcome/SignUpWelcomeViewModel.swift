//
//  SignUpWelcomeViewModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//

import Foundation
import UIKit
import Swinject
import RxSwift
import MBProgressHUD

// MARK:- SignUpWelcomeViewState -
struct SignUpWelcomeViewState {
    
    var isLoading: Bool
    var status: ApiStatus?
    var apiDataModel: RegisterModel?
    
    static func initial() -> SignUpWelcomeViewState {
        return SignUpWelcomeViewState(isLoading: false, status: nil)
    }
    
    static func loading() -> SignUpWelcomeViewState {
        return SignUpWelcomeViewState(isLoading: true, status: nil)
    }
}

// MARK:- Protocol -
protocol SignUpWelcomeViewModel {
    func register(email: String, password: String, name: String, dob: String, profilePic: String, feeling: String)
    func signUpWelcomeViewStateObservable() -> Observable<SignUpWelcomeViewState>
}
// MARK:- SignUpWelcomeViewModelImpl -
class SignUpWelcomeViewModelImpl {
    
    // DisposeBag for RxSwift (i.e.: RxKeyboard)
    private var disposeBag = DisposeBag()
    
    // SignUpWelcomeViewState Publisher
    private var signUpWelcomeViewStatePublisher = PublishSubject<SignUpWelcomeViewState>()
    
    // NetworkModule Object
    private let networkManager: NetworkModuleDao
    
    /**
     Designated Initializer
     - parameter networkManager: Network Manager instance used to make the requests
     */
    init(with networkManager: NetworkModuleDao) {
        self.networkManager = networkManager
    }
}
// MARK:- SignUpWelcomeViewModelImpl Extension -
extension SignUpWelcomeViewModelImpl: SignUpWelcomeViewModel {
    /**
     View State Observable
     */
    func signUpWelcomeViewStateObservable() -> Observable<SignUpWelcomeViewState> {
        return signUpWelcomeViewStatePublisher.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .startWith(.initial())
    }
    /**
     For Api Calling
     */
    func register(email: String, password: String, name: String, dob: String, profilePic: String, feeling: String) {
        
        print("register param: email: \(email), password: \(password), name: \(name), dob: \(dob), profilePic: \(profilePic), feeling: \(feeling)")
        self.signUpWelcomeViewStatePublisher.onNext(SignUpWelcomeViewState.loading())
        
        self.networkManager.register(email: email, password: password, name: name, dob: dob, profilePic: profilePic, feeling: feeling)
            .map { [unowned self] in
                self.mapResponse(apiDataModel: $0)
            }
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { (SignUpWelcomeViewState) in
                self.signUpWelcomeViewStatePublisher.onNext(SignUpWelcomeViewState)
            }, onError: { (error) in
                print(error)
                self.signUpWelcomeViewStatePublisher.onNext(
                    SignUpWelcomeViewState(
                        isLoading: false, status: ApiStatus.error(error: error.localizedDescription)
                    )
                )
            }).disposed(by: self.disposeBag)
        
    }
    /**
     For Mapping Api Response
     */
    private func mapResponse(apiDataModel: RegisterModel) -> SignUpWelcomeViewState {
        
        guard let success = apiDataModel.code, success == 200 || success == 1 else {
            return SignUpWelcomeViewState(isLoading: false, status: .failure, apiDataModel: apiDataModel)
        }
        return SignUpWelcomeViewState(isLoading: false, status: .success, apiDataModel: apiDataModel)
    }
}

// MARK:- Swinject Assembly -
class SignUpWelcomeViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        
        //  NetworkModule
        container.register(SignUpWelcomeViewModel.self) { resolver in
            let networkManager = resolver.resolve(NetworkModuleDao.self)!
            return SignUpWelcomeViewModelImpl(with: networkManager)
        }
    }
}
