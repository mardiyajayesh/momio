//
//  SignUpNameViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 04/10/21.
//

import UIKit
import Swinject
import RxSwift

class SignUpNameViewController: BaseViewController {
    
    // MARK:- IBOutlets -
    @IBOutlet weak var textFieldFullName: UITextField!
    @IBOutlet weak var buttonBack: DesignableButton!
    @IBOutlet weak var buttonRight: DesignableButton!
    @IBOutlet weak var lableTitle: DesignableLabel!
    
    // MARK:- Properties -

    private var text: String = ""
    public var signUp = SignUp()
    // ViewModel
    private var viewModel: SignUpNameViewModel!
    
    // Bag
    private let bag = DisposeBag()
    
    // Swinject Initializer for the SignUpNameViewController
    func configure(with ViewModel: SignUpNameViewModel) {
        self.viewModel = ViewModel
    }
    
    // MARK:- View Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.uiSetup()
        self.rxSetup()
    }
}

// MARK:- Custom Method -
extension SignUpNameViewController {
    
    func uiSetup() {
        
        self.textFieldFullName.becomeFirstResponder()
        
        self.viewModel.signUpNameViewStateObservable()
            .subscribe(onNext: { [weak self] viewState in
                self?.render(viewState: viewState)
            }, onError: { error in
                
            }).disposed(by: self.bag)
        
        self.textFieldFullName.rx.text
            .subscribe(onNext: { text in
                if text?.count ?? 0 > 0 {
                    self.buttonRight.setActive(true)
                } else {
                    self.buttonRight.setActive(false)
                }
            })
            .disposed(by: bag)
    }
    
    func rxSetup() {}
    
    /**
     For Get Api Response
     */
    func render(viewState: SignUpNameViewState) {
        
        if viewState.isLoading {
            self.showProgressHud(text: "Loading...")
        } else {
            self.hideProgressHud()
        }
        guard let status = viewState.status else  { return }
        switch (status) {
        case .success:
            print("success")
            
        case .failure:
            print("failure")
            
        case .error( _):
            print("error")
            
        }
    }
}

// MARK:- Button Action -
extension SignUpNameViewController {
    
    @IBAction func buttonTapNext(_ sender: DesignableButton) {
        
        if self.textFieldFullName.text?.count == 0 {
            self.showCustomAlertAction(title: Validation.enptyName, buttonType: .yesButton, from: self, topImageName: "ic_mail_alert", buttonOneTitleText: GeneralConstants.ok)
        } else {
            self.keyboardDismissAndHideError()
            self.signUp.name = self.textFieldFullName.text
            let destination = UIStoryboard(name: "SignUpPasswordViewController", bundle: nil)
            let initialViewController = destination.instantiateViewController(withIdentifier: "SignUpPasswordViewController") as! SignUpPasswordViewController
            initialViewController.signUp = self.signUp
            self.navigationController?.pushViewController(initialViewController, animated: true)
        }
    }
}

// MARK:- Assemble Registration -
class SignUpNameViewControllerAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.storyboardInitCompleted(SignUpNameViewController.self) { (resolver, viewController) in
            
            let viewModel = resolver.resolve(SignUpNameViewModel.self)!
            viewController.configure(with: viewModel)
        }
    }
}
