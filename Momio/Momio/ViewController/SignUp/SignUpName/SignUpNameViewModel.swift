//
//  SignUpNameViewModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//

import Foundation
import UIKit
import Swinject
import RxSwift
import MBProgressHUD

// MARK:- SignUpNameViewState -
struct SignUpNameViewState {
    
    var isLoading: Bool
    var status: ApiStatus?
    var apiDataModel: LoginModel?
    
    static func initial() -> SignUpNameViewState {
        return SignUpNameViewState(isLoading: false, status: nil)
    }
    
    static func loading() -> SignUpNameViewState {
        return SignUpNameViewState(isLoading: true, status: nil)
    }
}

// MARK:- Protocol -
protocol SignUpNameViewModel {
    func getLiveTvData(action: String, page: Int)
    func signUpNameViewStateObservable() -> Observable<SignUpNameViewState>
}

// MARK:- SignUpNameViewModelImpl -
class SignUpNameViewModelImpl {
    
    // DisposeBag for RxSwift (i.e.: RxKeyboard)
    private var disposeBag = DisposeBag()
    
    // SignUpNameViewState Publisher
    private var signUpNameViewStatePublisher = PublishSubject<SignUpNameViewState>()
    
    // NetworkModule Object
    private let networkManager: NetworkModuleDao
    
    /**
     Designated Initializer
     - parameter networkManager: Network Manager instance used to make the requests
     */
    init(with networkManager: NetworkModuleDao) {
        self.networkManager = networkManager
    }
}

// MARK:- SignUpNameViewModelImpl Extension -
extension SignUpNameViewModelImpl: SignUpNameViewModel {
    
    /**
     View State Observable
     */
    func signUpNameViewStateObservable() -> Observable<SignUpNameViewState> {
        return signUpNameViewStatePublisher.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .startWith(.initial())
    }
    
    /**
     For Api Calling
     */
    func getLiveTvData(action: String, page: Int) {
        /*
        print("getLiveTvData param: action: \(action), page: \(page)")
        self.signUpNameViewStatePublisher.onNext(SignUpNameViewState.loading())
        
        self.networkManager.getTimeLineList(action: action, page: page)
            .map { [unowned self] in
                self.mapResponse(apiDataModel: $0)
            }
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { (SignUpNameViewState) in
                self.signUpNameViewStatePublisher.onNext(SignUpNameViewState)
            }, onError: { (error) in
                print(error)
                self.signUpNameViewStatePublisher.onNext(
                    SignUpNameViewState(
                        isLoading: false, status: ApiStatus.error(error: error.localizedDescription)
                    )
                )
            }).disposed(by: self.disposeBag)
        */
    }
    
    /**
     For Mapping Api Response
     */
    private func mapResponse(apiDataModel: LoginModel) -> SignUpNameViewState {
        
        guard let success = apiDataModel.code, success == 200 || success == 1 else {
            return SignUpNameViewState(isLoading: false, status: .failure, apiDataModel: apiDataModel)
        }
        return SignUpNameViewState(isLoading: false, status: .success, apiDataModel: apiDataModel)
    }
}

// MARK:- Swinject Assembly -
class SignUpNameViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        
        //  NetworkModule
        container.register(SignUpNameViewModel.self) { resolver in
            let networkManager = resolver.resolve(NetworkModuleDao.self)!
            return SignUpNameViewModelImpl(with: networkManager)
        }
    }
}
