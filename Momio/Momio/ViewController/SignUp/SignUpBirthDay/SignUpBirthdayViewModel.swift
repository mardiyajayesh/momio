//
//  SignUpBirthdayViewModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//

import Foundation
import UIKit
import Swinject
import RxSwift
import MBProgressHUD

// MARK:- SignUpBirthdayViewState -
struct SignUpBirthdayViewState {
    
    var isLoading: Bool
    var status: ApiStatus?
    var apiDataModel: LoginModel?
    
    static func initial() -> SignUpBirthdayViewState {
        return SignUpBirthdayViewState(isLoading: false, status: nil)
    }
    
    static func loading() -> SignUpBirthdayViewState {
        return SignUpBirthdayViewState(isLoading: true, status: nil)
    }
}

// MARK:- Protocol -
protocol SignUpBirthdayViewModel {
    func getLiveTvData(action: String, page: Int)
    func signUpBirthdayViewStateObservable() -> Observable<SignUpBirthdayViewState>
}

// MARK:- SignUpBirthdayViewModelImpl -
class SignUpBirthdayViewModelImpl {
    
    // DisposeBag for RxSwift (i.e.: RxKeyboard)
    private var disposeBag = DisposeBag()
    
    // SignUpBirthdayViewState Publisher
    private var signUpBirthdayViewStatePublisher = PublishSubject<SignUpBirthdayViewState>()
    
    // NetworkModule Object
    private let networkManager: NetworkModuleDao
    
    /**
     Designated Initializer
     - parameter networkManager: Network Manager instance used to make the requests
     */
    init(with networkManager: NetworkModuleDao) {
        self.networkManager = networkManager
    }
}

// MARK:- SignUpBirthdayViewModelImpl Extension -
extension SignUpBirthdayViewModelImpl: SignUpBirthdayViewModel {
    
    /**
     View Sate Observable
     */
    func signUpBirthdayViewStateObservable() -> Observable<SignUpBirthdayViewState> {
        return signUpBirthdayViewStatePublisher.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .startWith(.initial())
    }
    
    /**
     For Api Calling
     */
    func getLiveTvData(action: String, page: Int) {
        /*
        print("getLiveTvData param: action: \(action), page: \(page)")
        self.signUpBirthdayViewStatePublisher.onNext(SignUpBirthdayViewState.loading())
        
        self.networkManager.getTimeLineList(action: action, page: page)
            .map { [unowned self] in
                self.mapResponse(apiDataModel: $0)
            }
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { (SignUpBirthdayViewState) in
                self.signUpBirthdayViewStatePublisher.onNext(SignUpBirthdayViewState)
            }, onError: { (error) in
                print(error)
                self.signUpBirthdayViewStatePublisher.onNext(
                    SignUpBirthdayViewState(
                        isLoading: false, status: ApiStatus.error(error: error.localizedDescription)
                    )
                )
            }).disposed(by: self.disposeBag)
        */
    }
    
    /**
     For Mapping Api Response
     */
    private func mapResponse(apiDataModel: LoginModel) -> SignUpBirthdayViewState {
        
        guard let success = apiDataModel.code, success == 200 || success == 1 else {
            return SignUpBirthdayViewState(isLoading: false, status: .failure, apiDataModel: apiDataModel)
        }
        return SignUpBirthdayViewState(isLoading: false, status: .success, apiDataModel: apiDataModel)
    }
}

// MARK:- Swinject Assembly -
class SignUpBirthdayViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        
        //  NetworkModule
        container.register(SignUpBirthdayViewModel.self) { resolver in
            let networkManager = resolver.resolve(NetworkModuleDao.self)!
            return SignUpBirthdayViewModelImpl(with: networkManager)
        }
    }
}
