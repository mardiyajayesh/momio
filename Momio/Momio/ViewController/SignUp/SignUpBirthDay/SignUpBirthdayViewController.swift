//
//  SignUpBirthdayViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 05/10/21.
//

import UIKit
import Swinject
import RxSwift

class SignUpBirthdayViewController: BaseViewController {
    
    // MARK:- IBOutlets -
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelYearText: UILabel!
    @IBOutlet weak var labelYearValue: UILabel!
    @IBOutlet weak var labelMonthText: UILabel!
    @IBOutlet weak var labelMonthValue: UILabel!
    @IBOutlet weak var labelDayText: UILabel!
    @IBOutlet weak var labelDayValue: UILabel!
    
    @IBOutlet weak var textFieldYear: UITextField!
    @IBOutlet weak var textFieldMonth: UITextField!
    @IBOutlet weak var textFieldDay: UITextField!
    
    @IBOutlet weak var buttonOpenDatePicker: UIButton!
    @IBOutlet weak var buttonSkip: UIButton!
    @IBOutlet weak var buttonBack: DesignableButton!
    @IBOutlet weak var buttonRight: DesignableButton!
    
    // MARK:- Properties -
    
    private var text: String = ""
    public var signUp = SignUp()
    
    private var toolBar = UIToolbar()
    private var datePicker = UIDatePicker()
    
    // ViewModel
    private var viewModel: SignUpBirthdayViewModel!
    
    // Bag
    private let bag = DisposeBag()
    
    // Swinject Initializer for the SignUpBirthdayViewController
    func configure(with ViewModel: SignUpBirthdayViewModel) {
        self.viewModel = ViewModel
    }
    
    // MARK:- View Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.uiSetup()
        self.rxSetup()
    }
}

// MARK:- Custom Methods -
private extension SignUpBirthdayViewController {
    
    func uiSetup() {
        self.setupTextFields()
    }
    
    func rxSetup() {
        
        self.viewModel.signUpBirthdayViewStateObservable()
            .subscribe(onNext: { [weak self] viewState in
                self?.render(viewState: viewState)
            }, onError: { error in
                
            }).disposed(by: self.bag)
    }
    
    /**
     For Get Api Response
     */
    func render(viewState: SignUpBirthdayViewState) {
        if viewState.isLoading {
            self.showProgressHud(text: "Loading...")
        } else {
            self.hideProgressHud()
        }
        guard let status = viewState.status else  { return }
        switch (status) {
        case .success:
            print("success")
            
        case .failure:
            print("failure")
            
        case .error( _):
            print("error")
            
        }
    }
    
    /**
     For setup TextFields
     */
    func setupTextFields() {
        self.textFieldYear.attributedPlaceholder = NSAttributedString(string: self.textFieldYear.placeholder ?? "",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "ThemBGColor") ?? .white])
        self.textFieldMonth.attributedPlaceholder = NSAttributedString(string: self.textFieldMonth.placeholder ?? "",
                                                                       attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "ThemBGColor") ?? .white])
        self.textFieldDay.attributedPlaceholder = NSAttributedString(string: self.textFieldDay.placeholder ?? "",
                                                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "ThemBGColor") ?? .white])
        
        // Open Keybord When Screen Load
        self.textFieldYear.becomeFirstResponder()
    }
    
    /**
     UpdateTextField
     */
    func updateTextField(textfield: UITextField, isEmpty: Bool = true) {
        if isEmpty {
            textfield.textColor = UIColor(named: "ThemBGColor")
            if textfield.placeholder == "YYYY" {
                self.labelYearText.textColor = UIColor(named: "ThemBGColor")
            } else if textfield.placeholder == "MM" {
                self.labelMonthText.textColor = UIColor(named: "ThemBGColor")
            } else {
                self.labelDayText.textColor = UIColor(named: "ThemBGColor")
            }
        } else {
            textfield.textColor = .white
            if textfield.placeholder == "YYYY" {
                self.labelYearText.textColor = .white
            } else if textfield.placeholder == "MM" {
                self.labelMonthText.textColor = .white
            } else {
                self.labelDayText.textColor = .white
            }
        }
    }
}

// MARK:- Button Actions -
extension SignUpBirthdayViewController {
    
    /**
     For skip
     */
    @IBAction func buttonTapSkip(_ sender: Any) {
        
        let destination = UIStoryboard(name: "SignUpProfilePicViewController", bundle: nil)
        let initialViewController = destination.instantiateViewController(withIdentifier: "SignUpProfilePicViewController") as! SignUpProfilePicViewController
        initialViewController.signUp = self.signUp
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    
    /**
     For  Done-Next
     */
    @IBAction func buttonTapRight(_ sender: Any) {
        
        if self.textFieldYear.text == "" || self.textFieldMonth.text == "" || self.textFieldDay.text == "" {
            self.labelErrorMessage?.isHidden = false
        } else {
            self.keyboardDismissAndHideError()
            let fullDob = String("\(self.textFieldYear.text ?? "")-\(self.textFieldMonth.text ?? "")-\(self.textFieldDay.text ?? "")")
            //            initialViewController.birthdate =
            self.signUp.dob = fullDob
            let destination = UIStoryboard(name: "SignUpProfilePicViewController", bundle: nil)
            let initialViewController = destination.instantiateViewController(withIdentifier: "SignUpProfilePicViewController") as! SignUpProfilePicViewController
            initialViewController.signUp = self.signUp
            self.navigationController?.pushViewController(initialViewController, animated: true)
        }
    }
}

// MARK:- TextField Delegate -
extension SignUpBirthdayViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.keyboardDismissAndHideError()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var maxLength = 4
        
        if string.isEmpty {
            text = String(text.dropLast())
        } else {
            text = textField.text! + string
        }
        
        if text.count > 0 {
            self.updateTextField(textfield: textField, isEmpty: false)
        } else {
            self.updateTextField(textfield: textField,isEmpty: true)
        }
        
        //set max lenght
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString = currentString.replacingCharacters(in: range, with: string) as NSString
        if textField == self.textFieldYear {
            maxLength = 4
            if newString.length == 5 {
                self.textFieldMonth.becomeFirstResponder()
            }
            return newString.length <= maxLength
        } else {
            maxLength = 2
            if newString.length == 3 && textField != self.textFieldDay {
                self.textFieldDay.becomeFirstResponder()
            }
            return newString.length <= maxLength
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {}
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let year = Calendar.current.component(.year, from: Date())
        let int = Int((textField.text ?? "0") as String) ?? 0
        
        if textField == self.textFieldMonth && int > 12 {
            self.labelErrorMessage?.isHidden = false
            self.textFieldMonth.text = ""
        } else if textField == self.textFieldDay && int > 31 {
            self.labelErrorMessage?.isHidden = false
            self.textFieldDay.text = ""
        } else if textField == self.textFieldYear && int > (year - 4) {
            self.labelErrorMessage?.isHidden = false
            self.textFieldYear.text = ""
        } else {
            self.labelErrorMessage?.isHidden = true
        }
    }
}

// MARK:- Assemble Registration -
class SignUpBirthdayViewControllerAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.storyboardInitCompleted(SignUpBirthdayViewController.self) { (resolver, viewController) in
            
            let viewModel = resolver.resolve(SignUpBirthdayViewModel.self)!
            viewController.configure(with: viewModel)
        }
    }
}
