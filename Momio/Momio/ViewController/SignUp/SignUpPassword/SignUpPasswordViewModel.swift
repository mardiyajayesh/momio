//
//  SignUpPasswordViewModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//

import Foundation
import UIKit
import Swinject
import RxSwift
import MBProgressHUD

// MARK:- SignUpPasswordViewState -
struct SignUpPasswordViewState {
    
    var isLoading: Bool
    var status: ApiStatus?
    var apiDataModel: LoginModel?
    
    static func initial() -> SignUpPasswordViewState {
        return SignUpPasswordViewState(isLoading: false, status: nil)
    }
    
    static func loading() -> SignUpPasswordViewState {
        return SignUpPasswordViewState(isLoading: true, status: nil)
    }
}

// MARK:- Protocol -
protocol SignUpPasswordViewModel {
    func getLiveTvData(action: String, page: Int)
    func signUpPasswordViewStateObservable() -> Observable<SignUpPasswordViewState>
}

// MARK:- SignUpPasswordViewModelImpl -
class SignUpPasswordViewModelImpl {
    
    // DisposeBag for RxSwift (i.e.: RxKeyboard)
    private var disposeBag = DisposeBag()
    
    // SignUpPasswordViewState Publisher
    private var signUpPasswordViewStatePublisher = PublishSubject<SignUpPasswordViewState>()
    
    // NetworkModule Object
    private let networkManager: NetworkModuleDao
    
    /**
     Designated Initializer
     - parameter networkManager: Network Manager instance used to make the requests
     */
    init(with networkManager: NetworkModuleDao) {
        self.networkManager = networkManager
    }
}

// MARK:- SignUpPasswordViewModelImpl Extension -
extension SignUpPasswordViewModelImpl: SignUpPasswordViewModel {
    
    /**
     View State Observable
     */
    func signUpPasswordViewStateObservable() -> Observable<SignUpPasswordViewState> {
        return signUpPasswordViewStatePublisher.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .startWith(.initial())
    }
    
    /**
     For Api Calling
     */
    func getLiveTvData(action: String, page: Int) {
        /*
        print("getLiveTvData param: action: \(action), page: \(page)")
        self.signUpPasswordViewStatePublisher.onNext(SignUpPasswordViewState.loading())
        
        self.networkManager.getTimeLineList(action: action, page: page)
            .map { [unowned self] in
                self.mapResponse(apiDataModel: $0)
            }
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { (SignUpPasswordViewState) in
                self.signUpPasswordViewStatePublisher.onNext(SignUpPasswordViewState)
            }, onError: { (error) in
                print(error)
                self.signUpPasswordViewStatePublisher.onNext(
                    SignUpPasswordViewState(
                        isLoading: false, status: ApiStatus.error(error: error.localizedDescription)
                    )
                )
            }).disposed(by: self.disposeBag)
        */
    }
    
    /**
     For Mapping Api Response
     */
    private func mapResponse(apiDataModel: LoginModel) -> SignUpPasswordViewState {
        
        guard let success = apiDataModel.code, success == 200 || success == 1 else {
            return SignUpPasswordViewState(isLoading: false, status: .failure, apiDataModel: apiDataModel)
        }
        return SignUpPasswordViewState(isLoading: false, status: .success, apiDataModel: apiDataModel)
    }
}

// MARK:- Swinject Assembly -
class SignUpPasswordViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        
        //  NetworkModule
        container.register(SignUpPasswordViewModel.self) { resolver in
            let networkManager = resolver.resolve(NetworkModuleDao.self)!
            return SignUpPasswordViewModelImpl(with: networkManager)
        }
    }
}
