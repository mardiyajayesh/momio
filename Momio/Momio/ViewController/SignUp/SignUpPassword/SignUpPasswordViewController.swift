//
//  SignUpPasswordViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 04/10/21.
//

import UIKit
import Swinject
import RxSwift

class SignUpPasswordViewController: BaseViewController {
    
    // MARK:- IBOutlet -
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var textFieldPassword: DesignableTextField!
    @IBOutlet weak var buttonShowPassward: DesignableButton!
    @IBOutlet weak var buttonBack: DesignableButton!
    @IBOutlet weak var buttonNext: DesignableButton!
    
    // MARK:- Properties -
    
    private var text: String = ""
    public var signUp = SignUp()
    
    // Check for valid passwordk
    private var isValidPassword: Bool = false
    
    // ViewModel
    private var viewModel: SignUpPasswordViewModel!
    
    // Bag
    private let bag = DisposeBag()
    
    // Swinject Initializer for the SignUpPasswordViewController
    func configure(with ViewModel: SignUpPasswordViewModel) {
        self.viewModel = ViewModel
    }
    
    // MARK:- View Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.uiSetup()
        self.rxSetup()
    }
}

// MARK:- Custom Methods -
private extension SignUpPasswordViewController {
    
    func uiSetup() {
        
        self.textFieldPassword.becomeFirstResponder()
        self.setupErrorPasswordLabel()
    }
    
    func rxSetup() {
        
        self.viewModel.signUpPasswordViewStateObservable()
            .subscribe(onNext: { [weak self] viewState in
                self?.render(viewState: viewState)
            }, onError: { error in
                
            }).disposed(by: self.bag)
        
        let passwordValidator = PasswordValidator()
        
        self.textFieldPassword.rx.text
            .orEmpty
            .map(passwordValidator.validate)
            .subscribe(onNext: { isValid in
                
                self.isValidPassword = isValid
                
                if self.isValidPassword {
                    self.labelErrorMessage?.isHidden = true
                    self.buttonNext.setEnabled(true)
                } else {
                    if self.textFieldPassword.text?.count ?? 0 > 6 {
                        self.labelErrorMessage?.isHidden = false
                    }
                    self.buttonNext.setEnabled(false)
                }
            })
            .disposed(by: self.bag)
    }
    
    /**
     For Get Api Response
     */
    func render(viewState: SignUpPasswordViewState) {
        if viewState.isLoading {
            self.showProgressHud(text: "Loading...")
        } else {
            self.hideProgressHud()
        }
        guard let status = viewState.status else  { return }
        switch (status) {
        case .success:
            print("success")
        case .failure:
            print("failure")
        case .error( _):
            print("error")
        }
    }
    
    /**
     For UI Setup
     */
    func setupErrorPasswordLabel() {
        
        let string = HomeConstants.pleaseSetPassword
        let range = (string as NSString).range(of: "abcd1%")
        let attributedString = NSMutableAttributedString(string: string )
        
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSNumber(value: 1), range: range)
        self.labelErrorMessage?.attributedText = attributedString
        self.labelErrorMessage?.isUserInteractionEnabled = true
    }
}

// MARK:- Button Action -
extension SignUpPasswordViewController {
    
    /**
     For Password SecureText Show Hide
     */
    @IBAction func buttonTapShowPassword(_ sender: DesignableButton) {
        
        if sender.isSelected {
            self.textFieldPassword.isSecureTextEntry = true
        } else {
            self.textFieldPassword.isSecureTextEntry = false
        }
        sender.isSelected = !sender.isSelected
    }
    
    /**
     For Done- Next
     */
    @IBAction func buttonTapNext(_ sender: DesignableButton) {
        
        if self.textFieldPassword.text?.count == 0 {
            self.labelErrorMessage?.isHidden = false
        } else if self.isValidPassword(password: self.textFieldPassword.text ?? "") == false {
            self.labelErrorMessage?.isHidden = false
        } else {
            self.labelErrorMessage?.isHidden = false
            self.keyboardDismissAndHideError()
            self.signUp.password = self.textFieldPassword.text
            let destination = UIStoryboard(name: "SignUpBirthdayViewController", bundle: nil)
            let initialViewController = destination.instantiateViewController(withIdentifier: "SignUpBirthdayViewController") as! SignUpBirthdayViewController
            initialViewController.signUp = self.signUp
            self.navigationController?.pushViewController(initialViewController, animated: true)
        }
    }
}

// MARK:- Assemble Ragistration -
class SignUpPasswordViewControllerAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.storyboardInitCompleted(SignUpPasswordViewController.self) { (resolver, viewController) in
            let viewModel = resolver.resolve(SignUpPasswordViewModel.self)!
            viewController.configure(with: viewModel)
        }
    }
}
