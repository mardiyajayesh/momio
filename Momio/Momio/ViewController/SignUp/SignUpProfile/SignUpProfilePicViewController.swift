//
//  SignUpProfilePicViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 05/10/21.
//

import UIKit
import Swinject
import RxSwift

class SignUpProfilePicViewController: BaseViewController {
    // MARK:- IBOutlets -
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var buttonProfile: DesignableButton!
    @IBOutlet weak var buttonBack: DesignableButton!
    @IBOutlet weak var buttonRight: DesignableButton!
    @IBOutlet weak var buttonSkip: DesignableButton!
    
    @IBOutlet weak var imageViewProfile: DesignableImageView!
    // MARK:- Properties -
    
    public var signUp = SignUp()
    
    // ViewModel
    private var viewModel: SignUpProfilePicViewModel!
    // Bag
    private let bag = DisposeBag()
    
    // Swinject Initializer for the SignUpProfilePicViewController
    func configure(with ViewModel: SignUpProfilePicViewModel) {
        self.viewModel = ViewModel
    }
    // MARK:- View Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        //ViewModelConfig
        self.viewModel.signUpProfilePicViewStateObservable()
            .subscribe(onNext: { [weak self] viewState in
                self?.render(viewState: viewState)
            }, onError: { error in
            }).disposed(by: self.bag)
        //        self.viewModel.getLiveTvData(action: "get-timeline-post", page: 1)//, murchant_id: "", userType: ""
        self.setupUI()
    }
}

// MARK:- Custom Methods -
extension SignUpProfilePicViewController {
    /**
     For Get Api Response
     */
    func render(viewState: SignUpProfilePicViewState) {
        if viewState.isLoading {
            self.showProgressHud(text: "Loading...")
        } else {
            self.hideProgressHud()
        }
        guard let status = viewState.status else  { return }
        switch (status) {
        case .success:
            print("success")
            if let model = viewState.apiDataModel, let data = model.data {
                self.signUp.profilePic = data.fileName
                let destination = UIStoryboard(name: "SignUpWelcomeViewController", bundle: nil)
                let initialViewController = destination.instantiateViewController(withIdentifier: "SignUpWelcomeViewController") as! SignUpWelcomeViewController
                initialViewController.signUp = self.signUp
                self.navigationController?.pushViewController(initialViewController, animated: true)
            }
        case .failure:
            print("failure")
            
        case .error( _):
            print("error")
            
        }
    }
    /**
     For UI Setup
     */
    func setupUI() {
        imagePicker.delegate = self
    }
    
    func faceCenterImage(_ image: UIImage) {
        
        guard let uncroppedCgImage = image.cgImage else {
            imageViewProfile.image = image
            return
        }
        
        DispatchQueue.global(qos: .userInteractive).async {
            if #available(iOS 11.0, *) {
                uncroppedCgImage.faceCrop(completion: { [weak self] result in
                    switch result {
                    case .success(let cgImage):
                        DispatchQueue.main.async { self?.imageViewProfile.image = UIImage(cgImage: cgImage) }
                    case .notFound, .failure( _):
                        print("error")
                    }
                })
            } else {
                
            }
        }
    }
}

// MARK:- button Actions -
extension SignUpProfilePicViewController {
    /**
     For Done-Next
     */
    @IBAction func buttonTapRight(_ sender: DesignableButton) {
        if self.imageViewProfile.image == nil {
            self.showCustomAlertAction(title: Validation.emptyProfilePic, from: self)
        } else {
            
            if let image = self.imageViewProfile.image {
                self.viewModel.uploadProfilePicture(file: "file", image: image)
            }
        }
    }
    /**
     For Skip
     */
    @IBAction func buttonTapSkip(_ sender: DesignableButton) {
        
        let destination = UIStoryboard(name: "SignUpWelcomeViewController", bundle: nil)
        let initialViewController = destination.instantiateViewController(withIdentifier: "SignUpWelcomeViewController") as! SignUpWelcomeViewController
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    
}

// MARK:- UIImagePickerControllerDelegate -
extension SignUpProfilePicViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        
        if let tempImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.faceCenterImage(tempImage)
        }
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK:- Assemble Registration
class SignUpProfilePicViewControllerAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.storyboardInitCompleted(SignUpProfilePicViewController.self) { (resolver, viewController) in
            
            let viewModel = resolver.resolve(SignUpProfilePicViewModel.self)!
            viewController.configure(with: viewModel)
        }
    }
}
