//
//  SignUpProfilePicViewModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//

import Foundation
import UIKit
import Swinject
import RxSwift
import MBProgressHUD

// MARK:- SignUpProfilePicViewState -
struct SignUpProfilePicViewState {
    
    var isLoading: Bool
    var status: ApiStatus?
    var apiDataModel: FileUploadModel?
    
    static func initial() -> SignUpProfilePicViewState {
        return SignUpProfilePicViewState(isLoading: false, status: nil)
    }
    
    static func loading() -> SignUpProfilePicViewState {
        return SignUpProfilePicViewState(isLoading: true, status: nil)
    }
}

// MARK:- Protocol -
protocol SignUpProfilePicViewModel {
    func uploadProfilePicture(file: String, image: UIImage)
    func signUpProfilePicViewStateObservable() -> Observable<SignUpProfilePicViewState>
}

// MARK:- SignUpProfilePicViewModelImpl -
class SignUpProfilePicViewModelImpl {
    
    // DisposeBag for RxSwift (i.e.: RxKeyboard)
    private var disposeBag = DisposeBag()
    
    // SignUpProfilePicViewState Publisher
    private var signUpProfilePicViewStatePublisher = PublishSubject<SignUpProfilePicViewState>()
    
    // NetworkModule Object
    private let networkManager: NetworkModuleDao
    
    /**
     Designated Initializer
     - parameter networkManager: Network Manager instance used to make the requests
     */
    init(with networkManager: NetworkModuleDao) {
        self.networkManager = networkManager
    }
}

// MARK:- SignUpProfilePicViewModelImpl Extension -
extension SignUpProfilePicViewModelImpl: SignUpProfilePicViewModel {
    
    /**
     View State Observable
     */
    func signUpProfilePicViewStateObservable() -> Observable<SignUpProfilePicViewState> {
        return signUpProfilePicViewStatePublisher.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .startWith(.initial())
    }
    
    /**
     For Api Calling
     */
    func uploadProfilePicture(file: String, image: UIImage) {
        print("AddPost Param: file:\(file) image:\(image)")
        self.signUpProfilePicViewStatePublisher.onNext(SignUpProfilePicViewState.loading())
        
        self.networkManager.uploadProfilePicture(file: file, image: image)
            .map { [unowned self] in
                self.mapResponse(apiDataModel: $0)
        }
        .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
        .observeOn(MainScheduler.instance)
        .subscribe(onSuccess: { (viewState) in
            self.signUpProfilePicViewStatePublisher.onNext(viewState)
        }, onError: { (error) in
            print(error)
            self.signUpProfilePicViewStatePublisher.onNext(
                SignUpProfilePicViewState(
                    isLoading: false, status: ApiStatus.error(error: error.localizedDescription)
                )
            )
        }).disposed(by: self.disposeBag)
        
    }
    
    /**
     For Mapping Api Response
     */
    private func mapResponse(apiDataModel: FileUploadModel) -> SignUpProfilePicViewState {
        
        guard let success = apiDataModel.code, success == 200 || success == 1 else {
            return SignUpProfilePicViewState(isLoading: false, status: .failure, apiDataModel: apiDataModel)
        }
        return SignUpProfilePicViewState(isLoading: false, status: .success, apiDataModel: apiDataModel)
    }
}

// MARK:- Swinject Assembly -
class SignUpProfilePicViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        
        //  NetworkModule
        container.register(SignUpProfilePicViewModel.self) { resolver in
            let networkManager = resolver.resolve(NetworkModuleDao.self)!
            return SignUpProfilePicViewModelImpl(with: networkManager)
        }
    }
}
