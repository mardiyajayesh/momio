//
//  HighLightListViewModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//

import Foundation
import UIKit
import Swinject
import RxSwift
import MBProgressHUD

// MARK:- HighLightListViewState -
struct HighLightListViewState {
    
    var isLoading: Bool
    var status: ApiStatus?
    var apiDataModel: UsersPostListModel?
    
    static func initial() -> HighLightListViewState {
        return HighLightListViewState(isLoading: false, status: nil)
    }
    
    static func loading() -> HighLightListViewState {
        return HighLightListViewState(isLoading: true, status: nil)
    }
}

// MARK:- Protocol -
protocol HighLightListViewModel {
    func getPostListData(limit: Int, page: Int)
    func highLightListViewStateObservable() -> Observable<HighLightListViewState>
}
// MARK:- HighLightListViewModelImpl -
class HighLightListViewModelImpl {
    
    // DisposeBag for RxSwift (i.e.: RxKeyboard)
    private var disposeBag = DisposeBag()
    
    // HighLightListViewState Publisher
    private var highLightListViewStatePublisher = PublishSubject<HighLightListViewState>()
    
    // NetworkModule Object
    private let networkManager: NetworkModuleDao
    
    /**
     Designated Initializer
     - parameter networkManager: Network Manager instance used to make the requests
     */
    init(with networkManager: NetworkModuleDao) {
        self.networkManager = networkManager
    }
}
// MARK:- HighLightListViewModelImpl Extension -
extension HighLightListViewModelImpl: HighLightListViewModel {
    /**
     View State Observable
     */
    func highLightListViewStateObservable() -> Observable<HighLightListViewState> {
        return highLightListViewStatePublisher.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .startWith(.initial())
    }
    /**
     For Api Calling
     */
    func getPostListData(limit: Int, page: Int) {
        
        print("getPostListData param: limit: \(limit), page: \(page)")
        self.highLightListViewStatePublisher.onNext(HighLightListViewState.loading())
        
        self.networkManager.getPostListData(limit: limit, page: page)
            .map { [unowned self] in
                self.mapResponse(apiDataModel: $0)
            }
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { (HighLightListViewState) in
                self.highLightListViewStatePublisher.onNext(HighLightListViewState)
            }, onError: { (error) in
                print(error)
                self.highLightListViewStatePublisher.onNext(
                    HighLightListViewState(
                        isLoading: false, status: ApiStatus.error(error: error.localizedDescription)
                    )
                )
            }).disposed(by: self.disposeBag)
        
    }
    /**
     For Mapping Api Response
     */
    private func mapResponse(apiDataModel: UsersPostListModel) -> HighLightListViewState {
        
        guard let success = apiDataModel.code, success == 200 || success == 1 else {
            return HighLightListViewState(isLoading: false, status: .failure, apiDataModel: apiDataModel)
        }
        return HighLightListViewState(isLoading: false, status: .success, apiDataModel: apiDataModel)
    }
}

// MARK:- Swinject Assembly -
class HighLightListViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        
        //  NetworkModule
        container.register(HighLightListViewModel.self) { resolver in
            let networkManager = resolver.resolve(NetworkModuleDao.self)!
            return HighLightListViewModelImpl(with: networkManager)
        }
    }
}
