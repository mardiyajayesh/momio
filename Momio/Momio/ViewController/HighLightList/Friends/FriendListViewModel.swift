//
//  FriendListViewModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 26/10/21.
//

import Foundation
import UIKit
import Swinject
import RxSwift
import MBProgressHUD

// MARK:- FriendListViewState -
struct FriendListViewState {
    
    var isLoading: Bool
    var status: ApiStatus?
    var apiDataModel: LoginModel?
    
    static func initial() -> FriendListViewState {
        return FriendListViewState(isLoading: false, status: nil)
    }
    
    static func loading() -> FriendListViewState {
        return FriendListViewState(isLoading: true, status: nil)
    }
}

// MARK:- Protocol -
protocol FriendListViewModel {
    func getLiveTvData(action: String, page: Int)
    func FriendListViewStateObservable() -> Observable<FriendListViewState>
}
// MARK:- FriendListViewModelImpl -
class FriendListViewModelImpl {
    
    // DisposeBag for RxSwift (i.e.: RxKeyboard)
    private var disposeBag = DisposeBag()
    
    // FriendListViewState Publisher
    private var FriendListViewStatePublisher = PublishSubject<FriendListViewState>()
    
    // NetworkModule Object
    private let networkManager: NetworkModuleDao
    
    /**
     Designated Initializer
     - parameter networkManager: Network Manager instance used to make the requests
     */
    init(with networkManager: NetworkModuleDao) {
        self.networkManager = networkManager
    }
}
// MARK:- FriendListViewModelImpl Extension -
extension FriendListViewModelImpl: FriendListViewModel {
    /**
     View State Observable
     */
    func FriendListViewStateObservable() -> Observable<FriendListViewState> {
        return FriendListViewStatePublisher.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .startWith(.initial())
    }
    /**
     For Api Calling
     */
    func getLiveTvData(action: String, page: Int) {
        /*
        print("getLiveTvData param: action: \(action), page: \(page)")
        self.FriendListViewStatePublisher.onNext(FriendListViewState.loading())
        
        self.networkManager.getTimeLineList(action: action, page: page)
            .map { [unowned self] in
                self.mapResponse(apiDataModel: $0)
            }
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { (FriendListViewState) in
                self.FriendListViewStatePublisher.onNext(FriendListViewState)
            }, onError: { (error) in
                print(error)
                self.FriendListViewStatePublisher.onNext(
                    FriendListViewState(
                        isLoading: false, status: ApiStatus.error(error: error.localizedDescription)
                    )
                )
            }).disposed(by: self.disposeBag)
        */
    }
    /**
     For Mapping Api Response
     */
    private func mapResponse(apiDataModel: LoginModel) -> FriendListViewState {
        
        guard let success = apiDataModel.code, success == 200 || success == 1 else {
            return FriendListViewState(isLoading: false, status: .failure, apiDataModel: apiDataModel)
        }
        return FriendListViewState(isLoading: false, status: .success, apiDataModel: apiDataModel)
    }
}

// MARK:- Swinject Assembly -
class FriendListViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        
        //  NetworkModule
        container.register(FriendListViewModel.self) { resolver in
            let networkManager = resolver.resolve(NetworkModuleDao.self)!
            return FriendListViewModelImpl(with: networkManager)
        }
    }
}
