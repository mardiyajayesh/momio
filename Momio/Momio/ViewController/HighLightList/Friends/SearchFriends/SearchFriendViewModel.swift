//
//  SearchFriendViewModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 27/10/21.
//

import Foundation
import UIKit
import Swinject
import RxSwift
import MBProgressHUD

// MARK:- SearchFriendViewState -
struct SearchFriendViewState {
    
    var isLoading: Bool
    var status: ApiStatus?
    var apiDataModel: LoginModel?
    
    static func initial() -> SearchFriendViewState {
        return SearchFriendViewState(isLoading: false, status: nil)
    }
    
    static func loading() -> SearchFriendViewState {
        return SearchFriendViewState(isLoading: true, status: nil)
    }
}

// MARK:- Protocol -
protocol SearchFriendViewModel {
    func getLiveTvData(action: String, page: Int)
    func SearchFriendViewStateObservable() -> Observable<SearchFriendViewState>
}
// MARK:- SearchFriendViewModelImpl -
class SearchFriendViewModelImpl {
    
    // DisposeBag for RxSwift (i.e.: RxKeyboard)
    private var disposeBag = DisposeBag()
    
    // SearchFriendViewState Publisher
    private var SearchFriendViewStatePublisher = PublishSubject<SearchFriendViewState>()
    
    // NetworkModule Object
    private let networkManager: NetworkModuleDao
    
    /**
     Designated Initializer
     - parameter networkManager: Network Manager instance used to make the requests
     */
    init(with networkManager: NetworkModuleDao) {
        self.networkManager = networkManager
    }
}
// MARK:- SearchFriendViewModelImpl Extension -
extension SearchFriendViewModelImpl: SearchFriendViewModel {
    /**
     View State Observable
     */
    func SearchFriendViewStateObservable() -> Observable<SearchFriendViewState> {
        return SearchFriendViewStatePublisher.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .startWith(.initial())
    }
    /**
     For Api Calling
     */
    func getLiveTvData(action: String, page: Int) {
        /*
        print("getLiveTvData param: action: \(action), page: \(page)")
        self.SearchFriendViewStatePublisher.onNext(SearchFriendViewState.loading())
        
        self.networkManager.getTimeLineList(action: action, page: page)
            .map { [unowned self] in
                self.mapResponse(apiDataModel: $0)
            }
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { (SearchFriendViewState) in
                self.SearchFriendViewStatePublisher.onNext(SearchFriendViewState)
            }, onError: { (error) in
                print(error)
                self.SearchFriendViewStatePublisher.onNext(
                    SearchFriendViewState(
                        isLoading: false, status: ApiStatus.error(error: error.localizedDescription)
                    )
                )
            }).disposed(by: self.disposeBag)
        */
    }
    /**
     For Mapping Api Response
     */
    private func mapResponse(apiDataModel: LoginModel) -> SearchFriendViewState {
        
        guard let success = apiDataModel.code, success == 200 || success == 1 else {
            return SearchFriendViewState(isLoading: false, status: .failure, apiDataModel: apiDataModel)
        }
        return SearchFriendViewState(isLoading: false, status: .success, apiDataModel: apiDataModel)
    }
}

// MARK:- Swinject Assembly -
class SearchFriendViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        
        //  NetworkModule
        container.register(SearchFriendViewModel.self) { resolver in
            let networkManager = resolver.resolve(NetworkModuleDao.self)!
            return SearchFriendViewModelImpl(with: networkManager)
        }
    }
}

