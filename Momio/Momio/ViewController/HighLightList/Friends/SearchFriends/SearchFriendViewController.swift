//
//  SearchFriendViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 27/10/21.
//

import UIKit
import Swinject
import RxSwift

class SearchFriendViewController: BaseViewController {

    //MARK: - IBOutlets -
    @IBOutlet weak var labelTitle: DesignableLabel!
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var labelNoData: UILabel!
    
    //MARK: - Properties -
    var friends: [FriendListData] = []
    var filterData: [FriendListData] = []
    var searching:Bool = false
    // ViewModel
    private var viewModel: SearchFriendViewModel!
    // Bag
    private let bag = DisposeBag()
    
    // Swinject Initializer for the SearchFriendViewController
    func configure(with ViewModel: SearchFriendViewModel) {
        self.viewModel = ViewModel
    }
    //MARK: - Constraints -
    
    //MARK: -  View Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: - Custome Methods -
extension SearchFriendViewController {
    
    /**
     For Get Api Response
     */
    func render(viewState: SearchFriendViewState) {
        if viewState.isLoading {
            self.showProgressHud(text: "Loading...")
        } else {
            self.hideProgressHud()
        }
        guard let status = viewState.status else  { return }
        switch (status) {
        case .success:
            print("success")
            
        case .failure:
            print("failure")
            
        case .error( _):
            print("error")
            
        }
    }
    /**
     For Setup UI
     */
    func setupUI() {
        
        self.labelTitle.text = FriendList.searchByName
        
        //Register Table Cell
        let cellNib = UINib.init(nibName: "FriendListTableCell", bundle: nil)
        self.tableView.register(cellNib,forCellReuseIdentifier: "FriendListTableCell")
//        self.filterData = self.friends
        self.textFieldSearch.becomeFirstResponder()
    }
}

// MARK: - Table view delegate - data source -
extension SearchFriendViewController: UITableViewDataSource, UITableViewDelegate {
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filterData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendListTableCell") as? FriendListTableCell
        cell?.setData(model: self.filterData[indexPath.row])
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //        let cell: ImageVideoTableViewCell = tableView.cellForRow(at: indexPath) as! ImageVideoTableViewCell
        //        print("willDisplay Cell Index: \(indexPath.row)")
        
//        if indexPath.row == self.timeLineListData.count - 1 {
//            if timeLineListData.count < self.totalPost {
//                if let user_Id = UserDefaults.standard.string(forKey: "merchant_id"), let userType = UserDefaults.standard.value(forKey: "userType") as? String  {
//                    self.viewModel.getTimeLineList(action: "get-timeline-post", page: self.start, userId: user_Id, userType: userType)
//                }else{
//                    self.viewModel.getTimeLineList(action: "get-timeline-post", page: self.start, userId: "", userType: "")
//                }
//            }
//        }
    }
}

extension SearchFriendViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.keyboardDismissAndHideError()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var searchText  = textField.text! + string

//            if searchText.count >= 3 {
        
        if string == "" { //string.isEmpty {
            // do something for backSpace pressed
            searchText = textField.text!
            searchText = String(searchText.dropLast())
        } else {
            searchText = textField.text! + string
        }
        
            self.tableView.isHidden = false
            self.viewNoData.isHidden = true
                
                //add matching text to arrya
                self.filterData = self.friends.filter({(($0.name).localizedCaseInsensitiveContains(searchText))})
                
                if(self.filterData.count == 0){
//                    if searchText.count != 0 {
                        self.searching = false
                        self.tableView.isHidden = true
                        self.viewNoData.isHidden = false
//                    }
                  } else {
                    self.searching = true
                    self.tableView.isHidden = false
                    self.viewNoData.isHidden = true
                 }
                  self.tableView.reloadData();
            
//
//                tableView.reloadData()
//            }
//            else{
//                self.filterData = []
//            }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {}
    
}

// MARK:- Assemble Registration
class SearchFriendViewControllerAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.storyboardInitCompleted(SearchFriendViewController.self) { (resolver, viewController) in
            
            let viewModel = resolver.resolve(SearchFriendViewModel.self)!
            viewController.configure(with: viewModel)
        }
    }
}
