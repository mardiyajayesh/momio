//
//  InviteFriendCollectionCell.swift
//  Momio
//
//  Created by Jayesh Mardiya on 26/10/21.
//

import UIKit

class InviteFriendCollectionCell: UICollectionViewCell {

    //MARK: - IBOutlates -
    @IBOutlet weak var ImageViewShareIcon: UIImageView!
    @IBOutlet weak var labelShareName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
