//
//  FriendListViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 26/10/21.
//

import UIKit
import Swinject
import RxSwift

struct FriendListData {
    var image: String = ""
    var name: String = ""
    var user_name: String = ""
    var is_follow: Bool = false
}


class FriendListViewController: BaseViewController {

    //MARK: - IBOutlets -
    @IBOutlet weak var labelTitle: DesignableLabel!
    @IBOutlet weak var labelSearchByName: UILabel!
    @IBOutlet weak var labelYouMayInterest: UILabel!
    @IBOutlet weak var labelInviteFriendHere: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var buttonSearchByNameIcon: DesignableButton!
    @IBOutlet weak var buttonSearchByName: UIButton!
    @IBOutlet weak var buttonSearchByQrCodeIcon: DesignableButton!
    @IBOutlet weak var buttonSearchByQrCode: UIButton!
    @IBOutlet weak var buttonMyQrCode: DesignableButton!
    @IBOutlet weak var buttonRefresh: UIButton!
    
    //MARK: - Properties -
    var friends: [FriendListData] = []
    
    
    // ViewModel
    private var viewModel: FriendListViewModel!
    // Bag
    private let bag = DisposeBag()
    
    // Swinject Initializer for the FriendListViewController
    func configure(with ViewModel: FriendListViewModel) {
        self.viewModel = ViewModel
    }
    //MARK: - Constraints -
    
    //MARK: - View Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: - Custome Methods -
extension FriendListViewController {
    
    /**
     For Get Api Response
     */
    func render(viewState: FriendListViewState) {
        if viewState.isLoading {
            self.showProgressHud(text: "Loading...")
        } else {
            self.hideProgressHud()
        }
        guard let status = viewState.status else  { return }
        switch (status) {
        case .success:
            print("success")
            
        case .failure:
            print("failure")
            
        case .error( _):
            print("error")
            
        }
    }
    /**
     For Setup UI
     */
    func setupUI() {
        self
        self.labelTitle.text = FriendList.findAndConnect
        self.labelYouMayInterest.text = FriendList.youMyInterrest
        self.labelInviteFriendHere.text = FriendList.inviteFriendHere
        //Register Table Cell
        let cellNib = UINib.init(nibName: "FriendListTableCell", bundle: nil)
        self.tableView.register(cellNib,forCellReuseIdentifier: "FriendListTableCell")
        
        let collcellNib = UINib.init(nibName: "InviteFriendCollectionCell", bundle: nil)
//        self.collectionView.register(InviteFriendCollectionCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "InviteFriendCollectionCell")
        self.collectionView.register(collcellNib, forCellWithReuseIdentifier: "InviteFriendCollectionCell")
        
        for i in 0..<12 {
            if i == 2 || i == 4 {
                friends.append(FriendListData(image: "ic_profile_placeholder", name: "name_0\(i)", user_name: "userName_0\(i)", is_follow: true))
            } else {
                friends.append(FriendListData(image: "ic_profile_placeholder", name: "name_0\(i)", user_name: "userName_0\(i)", is_follow: false))
            }
        } 
    }
}

//MARK: - Button Actions
extension FriendListViewController {
    /**
     For Click on Search By Name
     */
    @IBAction func buttonTapSearchByNameIcon(_ sender: DesignableButton) {
        let destination = UIStoryboard(name: "SearchFriendViewController", bundle: nil)
        let initialViewController = destination.instantiateViewController(withIdentifier: "SearchFriendViewController") as! SearchFriendViewController
        initialViewController.friends = self.friends
        initialViewController.modalPresentationStyle = .fullScreen
//        self.navigationController?.pushViewController(initialViewController, animated: true)
        self.present(initialViewController, animated: true, completion: nil)
    }
    
    /**
     For Click on Search By Qr Code
     */
    @IBAction func buttonTapSearchByQrCodeIcon(_ sender: DesignableButton) {
    }
    
    /**
     For Click on My Qr Code
     */
    @IBAction func buttonTapMyQrCode(_ sender: DesignableButton) {

        let qrPopUp = MyQrCodePopUpViewController()
        qrPopUp.modalPresentationStyle = .fullScreen
        self.present(qrPopUp, animated: true, completion: nil)
    }
    
    /**
     For Click on Refresh
     */
    @IBAction func buttonTapRefresh(_ sender: UIButton) {
    }
}

// MARK: - Table view delegate - data source -
extension FriendListViewController: UITableViewDataSource, UITableViewDelegate {
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.friends.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendListTableCell") as? FriendListTableCell
        
        cell?.setData(model: self.friends[indexPath.row])
        /*if indexPath.row == 2 || indexPath.row == 5 {
            cell?.buttonFollow.setTitle(GeneralConstants.unFollow, for: .normal)
            cell?.buttonFollow.backgroundColor = UIColor(named: "ThemButtonBGLightGrayColor")
        } else {
            cell?.buttonFollow.setTitle(GeneralConstants.following, for: .normal)
            cell?.buttonFollow.backgroundColor = UIColor(named: "ThemButtonBGColor")
        }*/
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //        let cell: ImageVideoTableViewCell = tableView.cellForRow(at: indexPath) as! ImageVideoTableViewCell
        //        print("willDisplay Cell Index: \(indexPath.row)")
        
//        if indexPath.row == self.timeLineListData.count - 1 {
//            if timeLineListData.count < self.totalPost {
//                if let user_Id = UserDefaults.standard.string(forKey: "merchant_id"), let userType = UserDefaults.standard.value(forKey: "userType") as? String  {
//                    self.viewModel.getTimeLineList(action: "get-timeline-post", page: self.start, userId: user_Id, userType: userType)
//                }else{
//                    self.viewModel.getTimeLineList(action: "get-timeline-post", page: self.start, userId: "", userType: "")
//                }
//            }
//        }
    }
}

// MARK: - Collection view delegate - data source -
extension FriendListViewController: UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: InviteFriendCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier:"InviteFriendCollectionCell", for: indexPath) as! InviteFriendCollectionCell
//        cell.lblTitle.text = pickeUpDeliveryTimeArray[indexPath.row].time
        
        return cell
    }
    
   /* func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 60, height: 30)
    }*/
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
    }
}


// MARK:- Assemble Registration
class FriendListViewControllerAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.storyboardInitCompleted(FriendListViewController.self) { (resolver, viewController) in
            
            let viewModel = resolver.resolve(FriendListViewModel.self)!
            viewController.configure(with: viewModel)
        }
    }
}
