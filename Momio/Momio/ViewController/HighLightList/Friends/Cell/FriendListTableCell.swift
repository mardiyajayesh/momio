//
//  FriendListTableCell.swift
//  Momio
//
//  Created by Jayesh Mardiya on 26/10/21.
//

import UIKit
import SDWebImage

class FriendListTableCell: UITableViewCell {

    //MARK: - IBOutlets -
    @IBOutlet weak var imageViewProfile: DesignableImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var buttonFollow: DesignableButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    func setData(model: FriendListData) {
//        if let url = model.image {
//        self.imageViewProfile.sd_setImage(with: URL(string: "http://www.domain.com/path/to/image.jpg"), placeholderImage: nil)
//        }
        self.imageViewProfile.image = UIImage(named: model.image)
        self.labelName.text = model.name
        let userNameToDisplay = "@" + model.user_name
        self.labelUserName.text = userNameToDisplay
        if model.is_follow {
            self.buttonFollow.setTitle(GeneralConstants.unFollow, for: .normal)
            self.buttonFollow.backgroundColor = UIColor(named: "ThemButtonBGLightGrayColor")
        } else {
            self.buttonFollow.setTitle(GeneralConstants.following, for: .normal)
            self.buttonFollow.backgroundColor = UIColor(named: "ThemButtonBGColor")
        }
    }
    
}
