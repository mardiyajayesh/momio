//
//  ListViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 25/10/21.
//

import UIKit

class ListViewController: UIViewController {

    //MARK: - IBOotlets -
    @IBOutlet weak var tableViewFriend: UITableView!
    @IBOutlet weak var tableViewHighLight: UITableView!
    
    @IBOutlet weak var tableViewHeightConstariant: NSLayoutConstraint!
    //MARK: - Properties -
    var isExpand: Bool = true
    var dataCount: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupUI()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: - Custome Methodes -
extension ListViewController {
    func setupUI() {
        //Register Table Cell
        let cellNib = UINib.init(nibName: "HeighLightTableCell", bundle: nil)
        self.tableViewFriend.register(cellNib,forCellReuseIdentifier: "HeighLightTableCell")
        self.tableViewHighLight.register(cellNib,forCellReuseIdentifier: "HeighLightTableCell")
//        self.tableViewFriend.reloadData()
//        self.tableViewHighLight.reloadData()
        
//        self.tableViewFriend.isScrollEnabled = false
//        self.tableViewHighLight.isScrollEnabled = false
        
        self.tableViewHeightConstariant.constant = CGFloat(self.dataCount * 132)
        
//        NotificationCenter.default.addObserver(
//            self,
//            selector: #selector(self.scrollContentZero),
//            name: Notification.Name("bottomTableScrollEnable"),
//            object: nil)
    }
    /**
     For Orserver method
     */
    @objc private func scrollContentZero(notification: NSNotification){
        //do stuff using the userInfo property of the notification object
        self.tableViewFriend.isScrollEnabled = true
        self.tableViewHighLight.isScrollEnabled = true
    }
}

// MARK: - Table view delegate - data source -
extension ListViewController: UITableViewDataSource, UITableViewDelegate {
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableViewFriend {
            //Friend Table
            return 20
        } else {
            // highLight Table
            return 20
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeighLightTableCell") as? HeighLightTableCell
        if tableView == self.tableViewFriend {
            if indexPath.row == 0 {
                cell?.imageViewFeeling.isHidden = false
                cell?.imageViewFeeling1.isHidden = true
            } else if indexPath.row == 1 {
                cell?.imageViewFeeling.isHidden = false
                cell?.imageViewFeeling1.isHidden = false
            } else {
                cell?.imageViewFeeling.isHidden = true
                cell?.imageViewFeeling1.isHidden = true
            }
            cell?.isExpand = isExpand
            cell?.handleTap()
            
        } else {
            if indexPath.row == 0 {
                cell?.imageViewFeeling.isHidden = false
                cell?.imageViewFeeling1.isHidden = true
            } else if indexPath.row == 1 {
                cell?.imageViewFeeling.isHidden = false
                cell?.imageViewFeeling1.isHidden = false
            } else {
                cell?.imageViewFeeling.isHidden = true
                cell?.imageViewFeeling1.isHidden = true
            }
            
            cell?.isExpand = isExpand
            cell?.handleTap()
//            if (cell?.isExpand) == nil {
//                cell?.isExpand = false
//            }
            
            cell?.handleTap()
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tableViewFriend {
            self.isExpand = !self.isExpand
            self.tableViewFriend.reloadRows(at: [indexPath], with: .fade)
            self.tableViewFriend.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        } else {
            self.isExpand = !self.isExpand
            self.tableViewHighLight.reloadRows(at: [indexPath], with: .fade)
            self.tableViewHighLight.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        if tableView == self.tableViewFriend {
            self.isExpand = true
            tableViewFriend.reloadRows(at: [indexPath], with: .fade)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //        let cell: ImageVideoTableViewCell = tableView.cellForRow(at: indexPath) as! ImageVideoTableViewCell
        //        print("willDisplay Cell Index: \(indexPath.row)")
        
//        if indexPath.row == self.timeLineListData.count - 1 {
//            if timeLineListData.count < self.totalPost {
//                if let user_Id = UserDefaults.standard.string(forKey: "merchant_id"), let userType = UserDefaults.standard.value(forKey: "userType") as? String  {
//                    self.viewModel.getTimeLineList(action: "get-timeline-post", page: self.start, userId: user_Id, userType: userType)
//                }else{
//                    self.viewModel.getTimeLineList(action: "get-timeline-post", page: self.start, userId: "", userType: "")
//                }
//            }
//        }
    }
}

//MARK: - ScrollView Delegate -
extension ListViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let yoffset: CGFloat = scrollView.contentOffset.y
        print("yOffset: \(yoffset)")
        
        if yoffset < 0 {
            print("yoffset is 0 plus come")
            
//            self.tableViewFriend.isScrollEnabled = false
//            self.tableViewHighLight.isScrollEnabled = false
//            NotificationCenter.default.post(name: Notification.Name("bottomTableZero"), object: nil)
            
        }else {
            
        }
    }
}
