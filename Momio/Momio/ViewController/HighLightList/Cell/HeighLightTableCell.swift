//
//  HeighLightTableCell.swift
//  Momio
//
//  Created by Jayesh Mardiya on 15/10/21.
//

import UIKit
import SDWebImage

class HeighLightTableCell: UITableViewCell {
    
    //MARK: - IBOutlets -
    @IBOutlet weak var imageViewProfile: DesignableImageView!
    @IBOutlet weak var imageViewTopRightArrow: UIImageView!
    @IBOutlet weak var imageViewBottomRightArrow: UIImageView!
    @IBOutlet weak var imageViewFeeling: DesignableImageView!
    @IBOutlet weak var imageViewFeeling1: DesignableImageView!
    
    @IBOutlet weak var buttonProfile: UIButton!
    @IBOutlet weak var buttonEdit: UIButton!
    @IBOutlet weak var buttonStarCount: UIButton!
    @IBOutlet weak var buttonCamera: DesignableButton!
    
    @IBOutlet weak var labelFeelingTitle: UILabel!
    @IBOutlet weak var labelFeelingTitleValue: UILabel!
    @IBOutlet weak var labelFeelingTime: UILabel!
    @IBOutlet weak var labelFeelingDescription: UILabel!
    @IBOutlet weak var labelBottomFeelingDescription: UILabel!
    
    @IBOutlet weak var viewDesignableFeelingDetail: DesignableView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var viewDesignableStarCount: DesignableView!
    
    @IBOutlet weak var stackViewTop: UIStackView!
    
    @IBOutlet weak var topStackTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewBottomImageBottomConstraint: NSLayoutConstraint!
    // MARK: - Properties -
    var isExpand: Bool!
    
    var maxHeightForBigTopView: CGFloat = 0.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
//        self.viewDesignableFeelingDetail.addGestureRecognizer(tap)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    /**
     For Handel TapGesture
     */
    @objc func handleTap() {
        
        //isGestureTapped = !isGestureTapped
        
        if isExpand {
            print("isTapped true")
            //show profile top right arrow
            self.imageViewTopRightArrow.isHidden = false
            //hide profile bottom right arrow
            self.imageViewBottomRightArrow.isHidden = true
            //change top stack topn constraint
            self.topStackTopConstraint.constant = 20
            //hide time label
            self.labelFeelingTime.isHidden = true
            //hide edit button
            self.buttonEdit.isHidden = true
            //change top stack view spacing
            self.stackViewTop.spacing = 10
            //show top feeling description label
            self.labelFeelingDescription.isHidden = false
            //hide bottom feeling description label
            self.labelBottomFeelingDescription.isHidden = true
            //show bottom view
            self.viewBottom.isHidden = false
            //change bottom view imageview bottom constraint
            self.viewBottomImageBottomConstraint.constant = 0
            //hide top start count view
            self.viewDesignableStarCount.isHidden = true
            //hide bottom imageviews
            self.imageViewFeeling.isHidden = true
            self.imageViewFeeling1.isHidden = true
            // layout animation
            UIView.animate(withDuration: 0.4, delay: 0.0,  animations: {
                self.layoutIfNeeded()
            }, completion: nil)
            // change top designable view alpha
            self.viewDesignableFeelingDetail.alpha = 0.6
            // change top designable view alpha with animation
            UIView.animate(withDuration: 0.3, delay: 0.0,  animations: {
                self.viewDesignableFeelingDetail.alpha = 1.0
                
            }, completion: nil)
        } else {
            print("isTapped false")
            //hide profile top right arrow
            self.imageViewTopRightArrow.isHidden = true
            //show profile bottom right arrow
            self.imageViewBottomRightArrow.isHidden = false
            //change top stack top constraint
            self.topStackTopConstraint.constant = 35
            //show time label
            self.labelFeelingTime.isHidden = false
            //show edit button
            self.buttonEdit.isHidden = false
            //change top stack view spacing
            self.stackViewTop.spacing = 5
            //hide top feeling description label
            self.labelFeelingDescription.isHidden = true
            //show bottom feeling description label
            self.labelBottomFeelingDescription.isHidden = false
            //hide bottom view
            self.viewBottom.isHidden = false
            //change bottom view imageview bottom constraint
            self.viewBottomImageBottomConstraint.constant = 30
            //show top start count view
            self.viewDesignableStarCount.isHidden = false
            //show bottom imageviews
//            if 1 {
            self.imageViewFeeling.isHidden = false
//            } else {
//                self.imageViewFeeling.isHidden = false
//                self.imageViewFeeling1.isHidden = false
//            }
            
            //set dynamic hight of main detail view with bottom feeling description
            let labelBottomFeelingDescriptionHeight = self.labelBottomFeelingDescription.text?.height(constraintedWidth: self.imageViewFeeling.frame.width, font: UIFont.systemFont(ofSize: 12))
            
            /*if #available(iOS 14.0, *) {
                self.viewMainBgHeightConstraint.constant = CGFloat(Float16(self.totalViewHeight + self.viewBottomImageBottomConstraint.constant + self.topStackTopConstraint.constant + self.labelFeelingTitle.frame.height)) + 130.0 + CGFloat(labelBottomFeelingDescriptionHeight!)
                maxHeightForBigTopView = self.viewMainBgHeightConstraint.constant
            } else {
                // Fallback on earlier versions
            }*/
            
            // change top designable view alpha
            self.viewDesignableFeelingDetail.alpha = 0.6
            // layout animation
            UIView.animate(withDuration: 0.4, delay: 0.0,  animations: {
                self.layoutIfNeeded()
                
            }, completion: nil)
            // change top designable view alpha with animation and show post button in completion
            UIView.animate(withDuration: 0.3, delay: 0.0,  animations: {
                self.viewDesignableFeelingDetail.alpha = 1.0
            }, completion: { isDone in
                
            })
        }
    }
    
    func setData(model: Posts) {
        if let title = model.feeling {
            self.labelFeelingTitleValue.text = title
        }
        if let time = model.createdAt {
            self.labelFeelingTime.text = time
        }
        if let whatsUp = model.whatsUp {
            self.labelFeelingDescription.text = whatsUp
            self.labelBottomFeelingDescription.text = whatsUp
        }
        self.buttonStarCount.setTitle("", for: .normal)
        
//        if model.postImages?.count == 1 {
//            self.imageViewFeeling.sd_setImage(with: URL(string: "http://www.domain.com/path/to/image.jpg"), placeholderImage: nil)
//        } else if model.postImages?.count ?? 0 > 1 {
//                self.imageViewFeeling.sd_setImage(with: URL(string: "http://www.domain.com/path/to/image.jpg"), placeholderImage: nil)
//                self.imageViewFeeling1.sd_setImage(with: URL(string: "http://www.domain.com/path/to/image.jpg"), placeholderImage: nil)
//        }
    }
}
