//
//  HighLightListViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 07/10/21.
//

import UIKit
import Swinject
import RxSwift
import FittedSheets

private let maxHeightForSmallTopView: CGFloat = 197.0
private var maxHeightForBigTopView: CGFloat = 414.0
private let minHeight: CGFloat = 124.0

class HighLightListViewController: BaseViewController {
    
    //MARK: - IBOutlets -
    @IBOutlet weak var imageViewProfile: DesignableImageView!
    @IBOutlet weak var imageViewTopRightArrow: UIImageView!
    @IBOutlet weak var imageViewBottomRightArrow: UIImageView!
    @IBOutlet weak var imageViewFeeling: DesignableImageView!
    @IBOutlet weak var imageViewFeeling1: DesignableImageView!
    @IBOutlet weak var imageViewFriendButtonIndicator: DesignableImageView!
    @IBOutlet weak var imageViewHighLightButtonIndicator: DesignableImageView!
    
    @IBOutlet weak var buttonProfile: UIButton!
    @IBOutlet weak var buttonEdit: UIButton!
    @IBOutlet weak var buttonPost: UIButton!
    @IBOutlet weak var buttonStarCount: UIButton!
    @IBOutlet weak var buttonCamera: DesignableButton!
    @IBOutlet weak var buttonFriend: UIButton!
    @IBOutlet weak var buttonHighLight: UIButton!
    @IBOutlet weak var buttonNoData: UIButton!
    @IBOutlet weak var buttonPlusFriend: DesignableButton!
    @IBOutlet weak var buttonAddToHighlightFavorite: UIButton!
    
    @IBOutlet weak var labelFeelingTitle: UILabel!
    @IBOutlet weak var labelFeelingTitleValue: UILabel!
    @IBOutlet weak var labelFeelingTime: UILabel!
    @IBOutlet weak var labelFeelingDescription: UILabel!
    @IBOutlet weak var labelBottomFeelingDescription: UILabel!
    @IBOutlet weak var labelNoData: UILabel!
    @IBOutlet weak var labelTostMessage: UILabel!
    
    @IBOutlet weak var viewDesignableFeelingDetail: DesignableView!
    //    @IBOutlet weak var viewNodata: UIView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var viewProfilePic: UIView!
    @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var viewBgTostMessage: DesignableView!
    
    @IBOutlet weak var stackViewFriendsHighlightButtons: UIStackView!
    @IBOutlet weak var stackViewTop: UIStackView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var tableViewFriend: UITableView!
    @IBOutlet weak var tableViewHighLight: UITableView!
    
    
    //MARK: - Constraints -
    @IBOutlet weak var topStackTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewBottomImageBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewHeightConstariant: NSLayoutConstraint!
    @IBOutlet weak var viewMainBgHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewProfilePictureWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonCameraTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonCameraTrailingConstraint: NSLayoutConstraint!
    
    // MARK: - Properties -
    var isGestureTapped: Bool = false
    var selectedIndex: Int?
    var isExpand: Bool = true
    var totalViewHeight: CGFloat = 197
    var listView = ListViewController()
    var totalDataLimit: Int = 10
    var start: Int = 1
    var postListData: [Posts] = []
    
    // ViewModel
    private var viewModel: HighLightListViewModel!
    // Bag
    private let bag = DisposeBag()
    
    // Swinject Initializer for the HighLightListViewController
    func configure(with ViewModel: HighLightListViewModel) {
        self.viewModel = ViewModel
    }
    
    //MARK: - View Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        //ViewModelConfig
        self.viewModel.highLightListViewStateObservable()
            .subscribe(onNext: { [weak self] viewState in
                self?.render(viewState: viewState)
            }, onError: { error in
                
            }).disposed(by: self.bag)
        
        self.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
        self.viewModel.getPostListData(limit: self.totalDataLimit, page: self.start)
    }
    
}

// MARK:- Custom Methods -
extension HighLightListViewController {
    /**
     For Get Api Response
     */
    func render(viewState: HighLightListViewState) {
        if viewState.isLoading {
            self.showProgressHud(text: "Loading...")
        } else {
            self.hideProgressHud()
        }
        guard let status = viewState.status else  { return }
        switch (status) {
        case .success:
            print("success")
            if let apiPostListModel = viewState.apiDataModel, let data = apiPostListModel.data, let posts = data.posts {
                if self.start == 1 {
                    self.postListData = posts
                } else {
                    for item in posts {
                        self.postListData.append(item)
                    }
                }
                //                if let total = apiPostListModel.numCountResult {
                //                    self.start += 1
                //                    self.totalPost = Int(total) ?? 0
                //                }
                //set Table Height with Data
                //                self.tableViewHeightConstariant.constant = CGFloat(self.postListData.count * 132)
                self.tableViewFriend.reloadData()
                self.tableViewHighLight.reloadData()
            }
            
        case .failure:
            print("failure")
            
        case .error( _):
            print("error")
            
        }
    }
    /**
     For UI Setup
     */
    func setupUI() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.viewDesignableFeelingDetail.addGestureRecognizer(tap)
        
        //Register Table Cell
        let cellNib = UINib.init(nibName: "HeighLightTableCell", bundle: nil)
        self.tableViewFriend.register(cellNib,forCellReuseIdentifier: "HeighLightTableCell")
        self.tableViewHighLight.register(cellNib,forCellReuseIdentifier: "HeighLightTableCell")
        
        self.setData()
        self.buttonFriend.isSelected = true
        
        //        for i in 0..<20 {
        //            self.arrayCount = i
        //        }
        //        self.tableViewHeightConstariant.constant = CGFloat(self.arrayCount * 132)
    }
    
    /**
     For Handel TapGesture
     */
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        
        if isGestureTapped {
            print("isTapped true")
            //For small Action
            self.setSmallView()
        } else {
            print("isTapped false")
            //For Big Action
            self.setBigView()
        }
        isGestureTapped = !isGestureTapped
    }
    
    func setSmallView() {
        // Main view height change
        self.viewMainBgHeightConstraint.constant = self.totalViewHeight
        //show profile image top right arrow
        self.imageViewTopRightArrow.isHidden = false
        //hide profile image bottom right arrow
        self.imageViewBottomRightArrow.isHidden = true
        //change top title and description stack view top constraint
        self.topStackTopConstraint.constant = 20
        //hide time label
        self.labelFeelingTime.isHidden = true
        //hide edit button
        self.buttonEdit.isHidden = true
        //change top title and description stack view spacing
        self.stackViewTop.spacing = 10
        //show top description label
        self.labelFeelingDescription.isHidden = false
        //hide bottom description label
        self.labelBottomFeelingDescription.isHidden = true
        //show bottom main view
        self.viewBottom.isHidden = false
        //change bottom view imageView Bottom Constraint
        self.viewBottomImageBottomConstraint.constant = 0
        //hide bottom imageViews
        self.imageViewFeeling.isHidden = true
        self.imageViewFeeling1.isHidden = true
        //hide post button
        self.buttonPost.isHidden = true
        // layout animation
        UIView.animate(withDuration: 0.4, delay: 0.0,  animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
        // change top designable view alpha
        self.viewDesignableFeelingDetail.alpha = 0.6
        // change top designable view alpha with animation
        UIView.animate(withDuration: 0.3, delay: 0.0,  animations: {
            self.viewDesignableFeelingDetail.alpha = 1.0
            
        }, completion: nil)
    }
    
    func setBigView() {
        //hide profile image top right arrow
        self.imageViewTopRightArrow.isHidden = true
        //show profile image bottom right arrow
        self.imageViewBottomRightArrow.isHidden = false
        //change top title and description stack view top constraint
        self.topStackTopConstraint.constant = 35
        //show time label
        self.labelFeelingTime.isHidden = false
        //show edit button
        self.buttonEdit.isHidden = false
        //change top title and description stack view spacing
        self.stackViewTop.spacing = 5
        //hide top description label
        self.labelFeelingDescription.isHidden = true
        //show bottom description label
        self.labelBottomFeelingDescription.isHidden = false
        //show bottom main view
        self.viewBottom.isHidden = false
        //change bottom view imageView Bottom Constraint
        self.viewBottomImageBottomConstraint.constant = 30
        //show bottom imageViews
        self.imageViewFeeling.isHidden = false
        
        //self.buttonPost.isHidden = false
        
        //set dynamic hight of main detail view with bottom feeling description
        let labelBottomFeelingDescriptionHeight = self.labelBottomFeelingDescription.text?.height(constraintedWidth: self.imageViewFeeling.frame.width, font: UIFont.systemFont(ofSize: 12))
        
        if #available(iOS 14.0, *) {
            self.viewMainBgHeightConstraint.constant = CGFloat(Float16(self.totalViewHeight + self.viewBottomImageBottomConstraint.constant + self.topStackTopConstraint.constant + self.labelFeelingTitle.frame.height)) + 130.0 + CGFloat(labelBottomFeelingDescriptionHeight!)
            maxHeightForBigTopView = self.viewMainBgHeightConstraint.constant
        } else {
            // Fallback on earlier versions
        }
        // change top designable view alpha
        self.viewDesignableFeelingDetail.alpha = 0.6
        // layout animation
        UIView.animate(withDuration: 0.4, delay: 0.0,  animations: {
            self.view.layoutIfNeeded()
            
        }, completion: nil)
        // change top designable view alpha with animation and show post button in completion
        UIView.animate(withDuration: 0.3, delay: 0.0,  animations: {
            self.viewDesignableFeelingDetail.alpha = 1.0
        }, completion: { isDone in
            self.buttonPost.isHidden = false
        })
    }
    
    /**
     For Set All Current User Data
     */
    func setData() {
        //        self.labelFeelingTitle.text = ""
        self.labelFeelingTitleValue.text = "Sucks".uppercased()
        //        self.labelFeelingTime.text = ""
        //        self.labelFeelingDescription.text = ""
        //        self.labelBottomFeelingDescription.text = ""
        //        self.buttonStarCount.setTitle("", for: .normal)
        //        if 1 {
        //        self.imageViewFeeling.sd_setImage(with: URL(string: "http://www.domain.com/path/to/image.jpg"), placeholderImage: nil)
        //        } else {
        //        self.imageViewFeeling.sd_setImage(with: URL(string: "http://www.domain.com/path/to/image.jpg"), placeholderImage: UIImage(named: "placeholder.png"))
        //        self.imageViewFeeling1.sd_setImage(with: URL(string: "http://www.domain.com/path/to/image.jpg"), placeholderImage: UIImage(named: "placeholder.png"))
        //        }
    }
    
}

//MARK: - Button Action -
extension HighLightListViewController {
    /**
     For Edit Post
     */
    @IBAction func buttonTapEdit(_ sender: UIButton) {
        
    }
    
    /**
     For Post Feeling
     */
    @IBAction func buttonTapPost(_ sender: UIButton) {
    }
    
    /**
     For Star Count
     */
    @IBAction func buttonTapStarCount(_ sender: UIButton) {
    }
    
    /**
     For camera
     */
    @IBAction func buttonTapCamera(_ sender: DesignableButton) {
    }
    
    /**
     For Friends
     */
    @IBAction func buttonTapFriends(_ sender: UIButton) {
        self.buttonHighLight.isSelected = false
        sender.isSelected = true
        self.tableViewFriend.isHidden = false
        self.tableViewHighLight.isHidden = true
    }
    
    /**
     For HighLight
     */
    @IBAction func buttonTapHighLight(_ sender: UIButton) {
        self.buttonFriend.isSelected = false
        sender.isSelected = true
        self.tableViewFriend.isHidden = true
        //        self.tableViewHighLight.isHidden = false
        self.viewNoData.isHidden = false
    }
    
    /**
     For Open Friend
     */
    @IBAction func buttonTapPlusFriend(_ sender: DesignableButton) {
        
        let destination = UIStoryboard(name: "FriendListViewController", bundle: nil)
        let initialViewController = destination.instantiateViewController(withIdentifier: "FriendListViewController") as! FriendListViewController
        initialViewController.modalPresentationStyle = .fullScreen
        self.navigationController?.present(initialViewController, animated: true, completion: nil)
    }
}

// MARK: - Table view delegate - data source -
extension HighLightListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableViewFriend {
            //Friend Table
            return self.postListData.count
        } else {
            // highLight Table
            return self.postListData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeighLightTableCell") as? HeighLightTableCell
        if tableView == self.tableViewFriend {
            if indexPath.row == 0 {
                cell?.imageViewFeeling.isHidden = false
                cell?.imageViewFeeling1.isHidden = true
            } else if indexPath.row == 1 {
                cell?.imageViewFeeling.isHidden = false
                cell?.imageViewFeeling1.isHidden = false
            } else {
                cell?.imageViewFeeling.isHidden = true
                cell?.imageViewFeeling1.isHidden = true
            }
            cell?.isExpand = isExpand
            cell?.handleTap()
            cell?.setData(model: self.postListData[indexPath.row])
            
        } else {
            if indexPath.row == 0 {
                cell?.imageViewFeeling.isHidden = false
                cell?.imageViewFeeling1.isHidden = true
            } else if indexPath.row == 1 {
                cell?.imageViewFeeling.isHidden = false
                cell?.imageViewFeeling1.isHidden = false
            } else {
                cell?.imageViewFeeling.isHidden = true
                cell?.imageViewFeeling1.isHidden = true
            }
            
            cell?.isExpand = isExpand
            cell?.handleTap()
            cell?.setData(model: self.postListData[indexPath.row])
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tableViewFriend {
            self.isExpand = !self.isExpand
            self.tableViewFriend.reloadRows(at: [indexPath], with: .fade)
            self.tableViewFriend.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        } else {
            self.isExpand = !self.isExpand
            self.tableViewHighLight.reloadRows(at: [indexPath], with: .fade)
            self.tableViewHighLight.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        if tableView == self.tableViewFriend {
            self.isExpand = true
            tableViewFriend.reloadRows(at: [indexPath], with: .fade)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //        let cell: ImageVideoTableViewCell = tableView.cellForRow(at: indexPath) as! ImageVideoTableViewCell
        //        print("willDisplay Cell Index: \(indexPath.row)")
        
        //                if indexPath.row == self.postListData.count - 1 {
        //                    if self.postListData.count < self.totalPost {
        //                    self.start += 1
        //                        self.viewModel.getPostListData(limit: self.totalDataLimit, page: self.start)
        //                    }
        //                }
    }
    
    /*func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    private func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [AnyObject]? {

        let contextItem = UIContextualAction(style: .destructive, title: "More") {  (contextualAction, view, boolValue) in
                //Code I want to do here
            }
        let swipeActions = UISwipeActionsConfiguration(actions: [contextItem])
        return [swipeActions]
        
        
        /*let moreRowAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: "More", handler:{action, indexpath in
            print("MORE•ACTION");
        });
        moreRowAction.backgroundColor = UIColor(red: 0.298, green: 0.851, blue: 0.3922, alpha: 1.0);

        let deleteRowAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: "Delete", handler:{action, indexpath in
            print("DELETE•ACTION");
        });
        return [deleteRowAction, moreRowAction];*/
    }*/
    
    
}

//MARK: - ScrollView Delegate -
extension HighLightListViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y: CGFloat = scrollView.contentOffset.y
        let newHeaderViewHeight: CGFloat = self.viewMainBgHeightConstraint.constant - y
        print("newHeaderViewHeight: \(newHeaderViewHeight)")
        
        if isGestureTapped {
            
            // When Open the Post Details
            if newHeaderViewHeight > maxHeightForBigTopView {
                
                print("400 + Exicute")
                
                // Main view height change
                self.viewMainBgHeightConstraint.constant = maxHeightForBigTopView
                
                // Top button posstion move
                self.buttonCameraTopConstraint.constant = 5
                self.buttonCameraTrailingConstraint.constant = 0
                
                // Top posstion move Down
                self.topStackTopConstraint.constant = 35
                
                // Show the Profile Picture
                self.imageViewProfilePictureWidthConstraint.constant = 64
                self.viewProfilePic.isHidden = false
                
                // Show the Time lable
                self.labelFeelingTime.isHidden = false
                
                // Show the Button post
                self.buttonPost.isHidden = false
                
                // Constant Animation
                UIView.animate(withDuration: 0.2) {
                    self.view.layoutIfNeeded()
                }
                
            } else if newHeaderViewHeight < minHeight {
                
                print("118 Exicute")
                
                // Show the Profile Picture
                self.imageViewProfilePictureWidthConstraint.constant = 0
                self.viewProfilePic.isHidden = true
                
                // Top posstion move Up
                self.topStackTopConstraint.constant = 12
                
                // Top button posstion move
                self.buttonCameraTopConstraint.constant = 26
                self.buttonCameraTrailingConstraint.constant = 10
                
                // Main view height change
                self.viewMainBgHeightConstraint.constant = minHeight
                
                // Hide the Time lable
                self.labelFeelingTime.isHidden = true
                
                // Hide the Button post
                self.buttonPost.isHidden = true
                
                // Constant Animation
                UIView.animate(withDuration: 0.2) {
                    self.view.layoutIfNeeded()
                }
            } else {
                
                print("Other Exicute")
                
                // Main view height change
                self.viewMainBgHeightConstraint.constant = newHeaderViewHeight
                scrollView.contentOffset.y = 0 // block scroll view
                
                // Top button posstion move
                self.buttonCameraTopConstraint.constant = 5
                self.buttonCameraTrailingConstraint.constant = 0
                
                // Top posstion move Down
                self.topStackTopConstraint.constant = 35
                
                // Show the Profile Picture
                self.imageViewProfilePictureWidthConstraint.constant = 64
                self.viewProfilePic.isHidden = false
                
                // Show the Time lable
                self.labelFeelingTime.isHidden = false
                
                // Show the Button post
                self.buttonPost.isHidden = false
                
                // Constant Animation
                UIView.animate(withDuration: 0.2) {
                    self.view.layoutIfNeeded()
                }
            }
        }else {
            
            // When Close the Post Details
            if newHeaderViewHeight > maxHeightForSmallTopView {
                
                print("197 + Exicute")
                
                // Show the Profile Picture
                self.imageViewProfilePictureWidthConstraint.constant = 64
                self.viewProfilePic.isHidden = false
                
                // Main view height change
                self.viewMainBgHeightConstraint.constant = maxHeightForSmallTopView
                
                // Top posstion move up
                self.topStackTopConstraint.constant = 20
                
                // Top button posstion move
                self.buttonCameraTopConstraint.constant = 5
                self.buttonCameraTrailingConstraint.constant = 0
                
                // Constant Animation
                UIView.animate(withDuration: 0.2, animations: {
                    self.view.layoutIfNeeded()
                }, completion: { (success) in
                    
                })
                
            } else if newHeaderViewHeight < minHeight {
                
                print("118 Exicute")
                // Show the Profile Picture
                self.imageViewProfilePictureWidthConstraint.constant = 0
                self.viewProfilePic.isHidden = true
                
                // Top posstion move Down
                self.topStackTopConstraint.constant = 12
                
                // Top button posstion move
                self.buttonCameraTopConstraint.constant = 26
                self.buttonCameraTrailingConstraint.constant = 10
                
                // Main view height change
                self.viewMainBgHeightConstraint.constant = minHeight
                
                // Constant Animation
                UIView.animate(withDuration: 0.2) {
                    self.view.layoutIfNeeded()
                }
            } else {
                
                print("Other Exicute")
                
                // Main view height change
                self.viewMainBgHeightConstraint.constant = newHeaderViewHeight
                scrollView.contentOffset.y = 0 // block scroll view
                
                // Top button posstion move
                self.buttonCameraTopConstraint.constant = 5
                self.buttonCameraTrailingConstraint.constant = 0
                
                // Top posstion move Down
                self.topStackTopConstraint.constant = 12
                
                // Show the Profile Picture
                self.imageViewProfilePictureWidthConstraint.constant = 64
                self.viewProfilePic.isHidden = false
                
                // Constant Animation
                UIView.animate(withDuration: 0.2) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
}

// MARK:- Assemble Registration
class HighLightListViewControllerAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.storyboardInitCompleted(HighLightListViewController.self) { (resolver, viewController) in
            
            let viewModel = resolver.resolve(HighLightListViewModel.self)!
            viewController.configure(with: viewModel)
        }
    }
}

