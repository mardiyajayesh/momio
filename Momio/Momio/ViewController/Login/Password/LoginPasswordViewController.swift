//
//  LoginPasswordViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 01/10/21.
//

import UIKit
import Swinject
import RxSwift

class LoginPasswordViewController: BaseViewController {
    // MARK:- IBOutlets
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var buttonPasswordShow: DesignableButton!
    @IBOutlet weak var buttonBack: DesignableButton!
    @IBOutlet weak var buttonRight: DesignableButton!
    @IBOutlet weak var buttonForgotPassword: DesignableButton!
    @IBOutlet weak var labelTitle: DesignableLabel!
    // MARK:- Properties
    private var text: String = ""
    private var isFirstAlertClick: Bool = false
    public var signUp = SignUp()
    
    // ViewModel
    private var viewModel: LoginPasswordViewModel!
    // Bag
    private let bag = DisposeBag()
    
    // Swinject Initializer for the LoginPasswordViewController
    func configure(with ViewModel: LoginPasswordViewModel) {
        self.viewModel = ViewModel
    }
    // MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textFieldPassword.becomeFirstResponder()
        //ViewModelConfig
        self.viewModel.loginPasswordViewStateObservable()
            .subscribe(onNext: { [weak self] viewState in
                self?.render(viewState: viewState)
            }, onError: { error in
                
            }).disposed(by: self.bag)
    }
    
    // MARK:- AlertAction Method
    override func buttonTapAlertOk() {
        
        if !isFirstAlertClick {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.isFirstAlertClick = true
                self.showCustomAlertAction(title: Validation.forgotPasswordMailSend, buttonType: .yesButton, from: self, topImageName: "ic_mail_red_alert", buttonOneTitleText: GeneralConstants.ok, buttonOneBGColor: UIColor(named: "ThemButtonBGColor") ?? .purple, isCloseButtonHide: true)
            }
        }
    }
}

// MARK:- Custom Method
extension LoginPasswordViewController {
    /**
     For Get Api Response
     */
    func render(viewState: LoginPasswordViewState) {
        if viewState.isLoading {
            self.showProgressHud(text: "Loading...")
        } else {
            self.hideProgressHud()
        }
        guard let status = viewState.status else  { return }
        switch (status) {
        case .success:
            print("success")
            if let model = viewState.apiDataModel, let data = model.data {
                UserDefaults.standard.setValue("1", forKey: UserDefaultKeysName.isLogin)
                UserDefaults.standard.setValue(data.token, forKey: UserDefaultKeysName.token)
                let destination = UIStoryboard(name: "HighLightListViewController", bundle: nil)
                let initialViewController = destination.instantiateViewController(withIdentifier: "HighLightListViewController") as! HighLightListViewController
                self.navigationController?.pushViewController(initialViewController, animated: true)
            }
            
        case .failure:
            
            print("failure")
            
        case .error( _):
            print("error")
//            //SighUp screen
//            let destination = UIStoryboard(name: "SignUpNameViewController", bundle: nil)
//            let initialViewController = destination.instantiateViewController(withIdentifier: "SignUpNameViewController") as! SignUpNameViewController
////            initialViewController.strEmail = self.textFieldEmail.text ?? ""
//            self.signUp.password = self.textFieldPassword.text
//            self.navigationController?.pushViewController(initialViewController, animated: true)
        }
    }
}

// MARK:- Button Action -
extension LoginPasswordViewController {
    /**
     For Password SecureText Show Hide
     */
    @IBAction func buttonTapPasswordShow(_ sender: DesignableButton) {
        if sender.isSelected {
            self.textFieldPassword.isSecureTextEntry = true
        } else {
            self.textFieldPassword.isSecureTextEntry = false
        }
        sender.isSelected = !sender.isSelected
    }
    /**
     For Done-Next
     */
    @IBAction func buttonTapRight(_ sender: DesignableButton) {
        
        if self.textFieldPassword.text?.count == 0 {
            self.labelErrorMessage?.isHidden = false
        } else {
            self.keyboardDismissAndHideError()
            self.signUp.password = self.textFieldPassword.text
            print("signUp Model : \(self.signUp.email) and password \(self.signUp.password)")
            self.viewModel.login(email: self.signUp.email ?? "", password: self.textFieldPassword.text ?? "")
        }
    }
    /**
     For Go To Forgote Password Screen
     */
    @IBAction func buttonTapForgotPassword(_ sender: DesignableButton) {
        self.showCustomAlertAction(title: Validation.forgotPassword, buttonType: .yesButton, from: self, topImageName: "ic_mail_alert", buttonOneTitleText: GeneralConstants.sendEmail, buttonOneBGColor: UIColor(named: "ThemButtonBGBlueColor") ?? .blue, isCloseButtonHide: true)
    }
}

// MARK:- TextField Delegates -
extension LoginPasswordViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.keyboardDismissAndHideError()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string.isEmpty {
            text = String(text.dropLast())
        } else {
            text = textField.text! + string
        }
        
        if text.count > 0 {
            self.buttonRight.backgroundColor = UIColor(named: "ThemButtonBGColor")
        } else {
            self.buttonRight.backgroundColor = UIColor(named: "ThemButtonBGLightGrayColor")
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if self.labelErrorMessage?.isHidden == false {
            self.labelErrorMessage?.isHidden = true
        }
    }
}

// MARK:- Assemble Registration
class LoginPasswordViewControllerAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.storyboardInitCompleted(LoginPasswordViewController.self) { (resolver, viewController) in
            
            let viewModel = resolver.resolve(LoginPasswordViewModel.self)!
            viewController.configure(with: viewModel)
        }
    }
}
