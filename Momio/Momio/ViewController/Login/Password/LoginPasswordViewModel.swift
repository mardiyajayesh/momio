//
//  LoginPasswordViewModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//

import Foundation
import UIKit
import Swinject
import RxSwift
import MBProgressHUD

// MARK:- LoginPasswordViewState -
struct LoginPasswordViewState {
    
    var isLoading: Bool
    var status: ApiStatus?
    var apiDataModel: LoginModel?
    
    static func initial() -> LoginPasswordViewState {
        return LoginPasswordViewState(isLoading: false, status: nil)
    }
    
    static func loading() -> LoginPasswordViewState {
        return LoginPasswordViewState(isLoading: true, status: nil)
    }
}

// MARK:- Protocol -
protocol LoginPasswordViewModel {
    func login(email: String, password: String)
    func loginPasswordViewStateObservable() -> Observable<LoginPasswordViewState>
}

// MARK:- LoginPasswordViewModelImpl -
class LoginPasswordViewModelImpl {
    
    // DisposeBag for RxSwift (i.e.: RxKeyboard)
    private var disposeBag = DisposeBag()
    
    // LoginPasswordViewState Publisher
    private var loginPasswordViewStatePublisher = PublishSubject<LoginPasswordViewState>()
    
    // NetworkModule Object
    private let networkManager: NetworkModuleDao
    
    /**
     Designated Initializer
     - parameter networkManager: Network Manager instance used to make the requests
     */
    init(with networkManager: NetworkModuleDao) {
        self.networkManager = networkManager
    }
}

// MARK:- LoginPasswordViewModelImpl Extension -
extension LoginPasswordViewModelImpl: LoginPasswordViewModel {
    
    /**
     View State Observable
     */
    func loginPasswordViewStateObservable() -> Observable<LoginPasswordViewState> {
        return loginPasswordViewStatePublisher.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .startWith(.initial())
    }
    
    /**
     For Api calling
     */
    func login(email: String, password: String) {
        print("getLiveTvData param: email: \(email), password: \(password)")
        self.loginPasswordViewStatePublisher.onNext(LoginPasswordViewState.loading())
        
        self.networkManager.login(email: email, password: password)
            .map { [unowned self] in
                self.mapResponse(apiDataModel: $0)
            }
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { (LoginPasswordViewState) in
                self.loginPasswordViewStatePublisher.onNext(LoginPasswordViewState)
            }, onError: { (error) in
                print(error)
                self.loginPasswordViewStatePublisher.onNext(
                    LoginPasswordViewState(
                        isLoading: false, status: ApiStatus.error(error: error.localizedDescription)
                    )
                )
            }).disposed(by: self.disposeBag)
    }
    
    /**
     For Mapping Api Response
     */
    private func mapResponse(apiDataModel: LoginModel) -> LoginPasswordViewState {
        
        guard let success = apiDataModel.code, success == 200 || success == 1 else {
            return LoginPasswordViewState(isLoading: false, status: .failure, apiDataModel: apiDataModel)
        }
        return LoginPasswordViewState(isLoading: false, status: .success, apiDataModel: apiDataModel)
    }
}

// MARK:- Swinject Assembly -
class LoginPasswordViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        
        //  NetworkModule
        container.register(LoginPasswordViewModel.self) { resolver in
            let networkManager = resolver.resolve(NetworkModuleDao.self)!
            return LoginPasswordViewModelImpl(with: networkManager)
        }
    }
}
