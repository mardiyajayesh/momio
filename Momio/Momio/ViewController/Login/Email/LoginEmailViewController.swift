//
//  LoginEmailViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 30/09/21.
//

import UIKit
import Swinject
import RxSwift
import RxCocoa

class LoginEmailViewController: BaseViewController {
    
    // MARK:- IBOutlets -
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var buttonBack: DesignableButton!
    @IBOutlet weak var buttonRight: DesignableButton!
    @IBOutlet weak var labelTitle: DesignableLabel!
    // MARK:- Properties -
    private var signUp = SignUp()
    
    // ViewModel
    private var viewModel: LoginEmailViewModel!
    
    // Bag
    private let bag = DisposeBag()
    
    // Check for valid email
    private var isValidEmail: Bool = false
    
    // Swinject Initializer for the LoginEmailViewController
    func configure(with ViewModel: LoginEmailViewModel) {
        self.viewModel = ViewModel
    }
    
    // MARK:- View Life cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.uiSetup()
        self.rxSetup()
    }
}

// MARK:- Custom Method -
extension LoginEmailViewController {
    
    func uiSetup() {
        self.textFieldEmail.becomeFirstResponder()
    }
    
    func rxSetup() {
        
        self.viewModel.loginEmailViewStateObservable()
            .subscribe(onNext: { [weak self] viewState in
                self?.render(viewState: viewState)
            }, onError: { error in
                
            }).disposed(by: self.bag)
        
        //        let emailValidator = EmailValidator()
        //
        //        self.textFieldEmail.rx.text
        //            .orEmpty
        //            .map(emailValidator.validate)
        //            .subscribe(onNext: { isValid in
        //
        //                self.isValidEmail = isValid
        //
        //                if self.isValidEmail {
        //                    self.buttonRight.setEnabled(true)
        //                } else {
        //                    self.buttonRight.setEnabled(false)
        //                }
        //            })
        //            .disposed(by: self.bag)
    }
    
    
    /**
     For  Get Api Response
     */
    func render(viewState: LoginEmailViewState) {
        if viewState.isLoading {
            self.showProgressHud(text: "Loading...")
        } else {
            self.hideProgressHud()
        }
        guard let status = viewState.status else  { return }
        switch (status) {
        case .success:
            print("success")
            if let model = viewState.apiDataModel, model.code == 500 {
                //Login Password Screen
                let destination = UIStoryboard(name: "LoginPasswordViewController", bundle: nil)
                let initialViewController = destination.instantiateViewController(withIdentifier: "LoginPasswordViewController") as! LoginPasswordViewController
                initialViewController.signUp = self.signUp
                self.navigationController?.pushViewController(initialViewController, animated: true)
            } else {
                if viewState.apiDataModel != nil{
                    //SighUp screen
                    let destination = UIStoryboard(name: "SignUpNameViewController", bundle: nil)
                    let initialViewController = destination.instantiateViewController(withIdentifier: "SignUpNameViewController") as! SignUpNameViewController
                    initialViewController.signUp = self.signUp
                    self.navigationController?.pushViewController(initialViewController, animated: true)
                    
                }
            }
        case .failure:
            print("failure")
        case .error( _):
            print("error")
        }
    }
}

// MARK:- Button Actions -
extension LoginEmailViewController {
    
    @IBAction func buttonTapRight(_ sender: DesignableButton) {
        
        self.view.endEditing(true)
        if self.textFieldEmail.text?.count == 0 {
            self.showCustomAlertAction(title: Validation.accountInvalid, buttonType: .yesButton, from: self, topImageName: "ic_mail_alert", buttonOneTitleText: GeneralConstants.goToSupport, buttonOneBGColor: UIColor(named: "ThemButtonBGBlueColor") ?? .blue, isCloseButtonHide: false)
            
        } else if self.isValidEmail(self.textFieldEmail.text ?? "") == false {
            self.showCustomAlertAction(title: Validation.accountInvalid, buttonType:.yesButton , from: self, topImageName: "ic_mail_alert", buttonOneTitleText: GeneralConstants.goToSupport, buttonOneBGColor: UIColor(named: "ThemButtonBGBlueColor") ?? .blue, isCloseButtonHide: false)
        } else {
            self.keyboardDismissAndHideError()
            self.signUp.email = self.textFieldEmail.text
            
            self.viewModel.checkEmail(email: self.textFieldEmail.text ?? "")
            
        }
    }
}

// MARK:- TextField Delegates -
extension LoginEmailViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.keyboardDismissAndHideError()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
}

// MARK:- Assemble Registration -
class LoginEmailViewControllerAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.storyboardInitCompleted(LoginEmailViewController.self) { (resolver, viewController) in
            
            let viewModel = resolver.resolve(LoginEmailViewModel.self)!
            viewController.configure(with: viewModel)
        }
    }
}
