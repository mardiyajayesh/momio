//
//  LoginEmailViewModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//

import Foundation
import UIKit
import Swinject
import RxSwift
import MBProgressHUD

enum ApiStatus {
    case success
    case failure
    case error(error: String? = nil)
}

// MARK:- LoginEmailViewState -
struct LoginEmailViewState {
    
    var isLoading: Bool
    var status: ApiStatus?
    var apiDataModel: CheckEmailModel?
    
    static func initial() -> LoginEmailViewState {
        return LoginEmailViewState(isLoading: false, status: nil)
    }

    static func loading() -> LoginEmailViewState {
        return LoginEmailViewState(isLoading: true, status: nil)
    }
}

// MARK:- Protocol -
protocol LoginEmailViewModel {
    func checkEmail(email: String)
    func loginEmailViewStateObservable() -> Observable<LoginEmailViewState>
}

// MARK:- LoginEmailViewModelImpl -
class LoginEmailViewModelImpl {
    
    // DisposeBag for RxSwift (i.e.: RxKeyboard)
    private var disposeBag = DisposeBag()
    
    // LoginEmailViewState Publisher
    private var loginEmailViewStatePublisher = PublishSubject<LoginEmailViewState>()
    
    // NetworkModule Object
    private let networkManager: NetworkModuleDao
    
    /**
     Designated Initializer
     - parameter networkManager: Network Manager instance used to make the requests
     */
    init(with networkManager: NetworkModuleDao) {
        self.networkManager = networkManager
    }
}

// MARK:- LoginEmailViewModelImpl Extension -
extension LoginEmailViewModelImpl: LoginEmailViewModel {

    /**
     View State Observable
     */
    func loginEmailViewStateObservable() -> Observable<LoginEmailViewState> {
        return loginEmailViewStatePublisher.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .startWith(.initial())
    }

    /**
     For api Calling
     */
    func checkEmail(email: String) {
        
        print("checkEmail param: email: \(email)")
        self.loginEmailViewStatePublisher.onNext(LoginEmailViewState.loading())
        
        self.networkManager.checkEmail(email: email)
            .map { [unowned self] in
                self.mapResponse(apiDataModel: $0)
        }
        .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
        .observeOn(MainScheduler.instance)
        .subscribe(onSuccess: { (LoginEmailViewState) in
            self.loginEmailViewStatePublisher.onNext(LoginEmailViewState)
        }, onError: { (error) in
            print("error is: \(error)")
            self.loginEmailViewStatePublisher.onNext(
                LoginEmailViewState(
                    isLoading: false, status: ApiStatus.error(error: error.localizedDescription)
                )
            )
        }).disposed(by: self.disposeBag)
        
    }

    /**
     For Mapping response
     */
    private func mapResponse(apiDataModel: CheckEmailModel) -> LoginEmailViewState {
        
        guard let success = apiDataModel.code, success == 200 || success == 500 else {
            return LoginEmailViewState(isLoading: false, status: .failure, apiDataModel: apiDataModel)
        }
        return LoginEmailViewState(isLoading: false, status: .success, apiDataModel: apiDataModel)
    }
}

// MARK:- Swinject Assembly -
class LoginEmailViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        
        //  NetworkModule
        container.register(LoginEmailViewModel.self) { resolver in
            let networkManager = resolver.resolve(NetworkModuleDao.self)!
            return LoginEmailViewModelImpl(with: networkManager)
        }
    }
}
