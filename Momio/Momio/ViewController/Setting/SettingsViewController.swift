//
//  SettingsViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 07/10/21.
//

import UIKit
import Swinject
import RxSwift

class SettingsViewController: BaseViewController {
    // MARK:- IBOutlets -
    @IBOutlet weak var viewProfilePhoto: UIView!
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var viewUserName: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var viewBirthday: UIView!
    @IBOutlet weak var viewPosting: UIView!
    @IBOutlet weak var viewResetHistory: UIView!
    @IBOutlet weak var viewLanguage: UIView!
    @IBOutlet weak var viewVersion: UIView!
    @IBOutlet weak var viewNotification: UIView!
    @IBOutlet weak var viewLegal: UIView!
    @IBOutlet weak var viewSupport: UIView!
    @IBOutlet weak var viewDelete: UIView!
    @IBOutlet weak var viewLogout: UIView!
    @IBOutlet weak var viewLine1: UIView!
    @IBOutlet weak var viewLine2: UIView!
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelProfileSettingTitle: UILabel!
    @IBOutlet weak var labelProfilePhotoText: UILabel!
    @IBOutlet weak var labelNameText: UILabel!
    @IBOutlet weak var labelNameValue: UILabel!
    @IBOutlet weak var labelUserNameText: UILabel!
    @IBOutlet weak var labelUserNameValue: UILabel!
    @IBOutlet weak var labelEmailText: UILabel!
    @IBOutlet weak var labelEmailValue: UILabel!
    @IBOutlet weak var labelPasswordText: UILabel!
    @IBOutlet weak var labelBirthDayText: UILabel!
    @IBOutlet weak var labelBirthdayValue: UILabel!
    @IBOutlet weak var labelPostingText: UILabel!
    @IBOutlet weak var labelPostingValue: UILabel!
    @IBOutlet weak var labelResetMyFeelingText: UILabel!
    @IBOutlet weak var labelSettingInfoTitle: UILabel!
    @IBOutlet weak var labelLanguageText: UILabel!
    @IBOutlet weak var labelLanguageValue: UILabel!
    @IBOutlet weak var labelVersionText: UILabel!
    @IBOutlet weak var labelVersionValue: UILabel!
    @IBOutlet weak var labelNotificationText: UILabel!
    @IBOutlet weak var labelLegalText: UILabel!
    @IBOutlet weak var labelSupportText: UILabel!
    @IBOutlet weak var labelDeleteAccount: UILabel!
    @IBOutlet weak var labelLogOutTitle: UILabel!
    @IBOutlet weak var labelLogOutText: UILabel!
    
    @IBOutlet weak var buttonSelectProfilePic: UIButton!
    @IBOutlet weak var buttonSetName: UIButton!
    @IBOutlet weak var buttonSetUserName: UIButton!
    @IBOutlet weak var buttonSetEmail: UIButton!
    @IBOutlet weak var buttonSetPassword: UIButton!
    @IBOutlet weak var buttonSetBirthday: UIButton!
    @IBOutlet weak var buttonPosting: UIButton!
    @IBOutlet weak var buttonResetMyFeeling: UIButton!
    @IBOutlet weak var buttonSetLanguage: UIButton!
    @IBOutlet weak var ButtonUpdate: DesignableButton!
    @IBOutlet weak var buttonGetUpdate: UIButton!
    @IBOutlet weak var buttonNotificationArrow: DesignableButton!
    @IBOutlet weak var buttonSetNotification: UIButton!
    @IBOutlet weak var buttonLegalArrow: DesignableButton!
    @IBOutlet weak var buttonLegal: UIButton!
    @IBOutlet weak var buttonSupportArrow: DesignableButton!
    @IBOutlet weak var buttonSupport: UIButton!
    @IBOutlet weak var buttonDeleteAccountArrow: DesignableButton!
    @IBOutlet weak var buttonDeleteAccount: UIButton!
    @IBOutlet weak var buttonLogOutArrow: DesignableButton!
    @IBOutlet weak var buttonLogout: UIButton!
    
    @IBOutlet weak var imageViewProfilePic: DesignableImageView!
    
    // MARK:- Properties -
    var updateType: UpdateType = .name
    var isLogOutClick: Bool = false
    
    // ViewModel
    private var viewModel: SettingsViewModel!
    // Bag
    private let bag = DisposeBag()
    
    // Swinject Initializer for the SettingsViewController
    func configure(with ViewModel: SettingsViewModel) {
        self.viewModel = ViewModel
    }
    // MARK:- View Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        //ViewModelConfig
        self.viewModel.settingsViewStateObservable()
            .subscribe(onNext: { [weak self] viewState in
                self?.render(viewState: viewState)
            }, onError: { error in
                
            }).disposed(by: self.bag)
        //        self.viewModel.getLiveTvData(action: "get-timeline-post", page: 1)//, murchant_id: "", userType: ""
        self.setupUI()
    }
    
    // MARK:- Alert Action Method -
    override func buttonTapAlertOk() {
        print("Alert ok click")
        if self.isLogOutClick {
            print("logOut alert ok Click")
            UserDefaults.standard.removeObject(forKey: UserDefaultKeysName.isLogin)
            self.goToRootView()
        } else {
            print("other alert ok Click")
        }
    }
}

// MARK:- Custom Methods -
extension SettingsViewController {
    /**
     For Get Api Response
     */
    func render(viewState: SettingsViewState) {
        if viewState.isLoading {
            self.showProgressHud(text: "Loading...")
        } else {
            self.hideProgressHud()
        }
        guard let status = viewState.status else  { return }
        switch (status) {
        case .success:
            print("success")
            
        case .failure:
            print("failure")
            
        case .error( _):
            print("error")
            
        }
    }
    /**
     For UI Setup
     */
    func setupUI() {
        
        // set birthday
        self.labelBirthdayValue.text = GeneralConstants.notSet
        self.labelVersionValue.text = appVersion
    }
}

// MARK:- Button Action -
extension SettingsViewController {
    /**
     For Open Profile Picture Setting
     */
    @IBAction func buttonTapSelectProfilePic(_ sender: UIButton) {
        let destination = UIStoryboard(name: "UpdateProfilePicViewController", bundle: nil)
        let initialViewController = destination.instantiateViewController(withIdentifier: "UpdateProfilePicViewController") as! UpdateProfilePicViewController
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    /**
     For Open Name Setting
     */
    @IBAction func buttonTapSetName(_ sender: UIButton) {
        let destination = UIStoryboard(name: "UpdateNameViewController", bundle: nil)
        let initialViewController = destination.instantiateViewController(withIdentifier: "UpdateNameViewController") as! UpdateNameViewController
        let strName = self.labelNameValue.text?.replacingOccurrences(of: "@", with: "")
        initialViewController.nameText = strName ?? ""
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    /**
     For Open User Name Setting
     */
    @IBAction func buttonTapSetUserName(_ sender: UIButton) {
        let destination = UIStoryboard(name: "UpdateNameViewController", bundle: nil)
        let initialViewController = destination.instantiateViewController(withIdentifier: "UpdateNameViewController") as! UpdateNameViewController
        self.updateType = .username
        initialViewController.updateType = self.updateType
        let strUserName = self.labelUserNameValue.text?.replacingOccurrences(of: "@", with: "")
        initialViewController.nameText = strUserName ?? ""
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    /**
     For Open Email Setting
     */
    @IBAction func buttonTapSetEmail(_ sender: UIButton) {
        let destination = UIStoryboard(name: "UpdateNameViewController", bundle: nil)
        let initialViewController = destination.instantiateViewController(withIdentifier: "UpdateNameViewController") as! UpdateNameViewController
        self.updateType = .email
        initialViewController.updateType = self.updateType
        initialViewController.nameText = self.labelEmailValue.text ?? GeneralConstants.notSet
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    /**
     For Open Password Setting
     */
    @IBAction func buttonTapSetPassword(_ sender: UIButton) {
        
        let destination = UIStoryboard(name: "UpdatePasswordViewController", bundle: nil)
        let initialViewController = destination.instantiateViewController(withIdentifier: "UpdatePasswordViewController") as! UpdatePasswordViewController
        initialViewController.email = self.labelEmailValue.text ?? ""
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    /**
     For Open Birthday Setting
     */
    @IBAction func buttonTapSetBirthday(_ sender: UIButton) {
        let destination = UIStoryboard(name: "UpdatePasswordTypePasswordViewController", bundle: nil)
        let initialViewController = destination.instantiateViewController(withIdentifier: "UpdatePasswordTypePasswordViewController") as! UpdatePasswordTypePasswordViewController
        initialViewController.isForBirthDay = true
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    /**
     For Open Posting Setting
     */
    @IBAction func buttonTapPosting(_ sender: UIButton) {}
    /**
     For Open My Feeling Setting
     */
    @IBAction func buttonTapResetMyFeeling(_ sender: UIButton) {
        self.showCustomAlertAction(title: Validation.resetMyFeelingHistory,buttonType: .yesButton, from: self,topImageName: "ic_clear_feeling_history_alert",buttonOneTitleText: GeneralConstants.yesDelete)
    }
    /**
     For Open Language Setting
     */
    @IBAction func buttonTapSetLanguage(_ sender: UIButton) {
        
        let destination = UIStoryboard(name: "ChangeLanguageViewController", bundle: nil)
        let initialViewController = destination.instantiateViewController(withIdentifier: "ChangeLanguageViewController") as! ChangeLanguageViewController
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    /**
     For Open Update Apps Setting
     */
    @IBAction func buttonTapUpdate(_ sender: DesignableButton) {
    }
    
    @IBAction func buttonTapGetUpdate(_ sender: UIButton) {
    }
    /**
     For Open Notification Setting
     */
    @IBAction func buttonTapSetNotifiacation(_ sender: UIButton) {
        let destination = UIStoryboard(name: "NotificationSettingViewController", bundle: nil)
        let initialViewController = destination.instantiateViewController(withIdentifier: "NotificationSettingViewController") as! NotificationSettingViewController
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    /**
     For Open Legal Setting
     */
    @IBAction func ButtonTapLegal(_ sender: UIButton) {
        let destination = UIStoryboard(name: "LegalViewController", bundle: nil)
        let initialViewController = destination.instantiateViewController(withIdentifier: "LegalViewController") as! LegalViewController
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    /**
     For Open Support Setting
     */
    @IBAction func buttonTapSupport(_ sender: UIButton) {
        let destination = UIStoryboard(name: "SupportViewController", bundle: nil)
        let initialViewController = destination.instantiateViewController(withIdentifier: "SupportViewController") as! SupportViewController
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    /**
     For Open Delete Account Setting
     */
    @IBAction func buttonTapDeleteAccount(_ sender: UIButton) {
        let destination = UIStoryboard(name: "DeleteAccountViewController", bundle: nil)
        let initialViewController = destination.instantiateViewController(withIdentifier: "DeleteAccountViewController") as! DeleteAccountViewController
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    /**
     For Logout
     */
    @IBAction func buttonTapLogOut(_ sender: UIButton) {
        self.isLogOutClick = true
        self.showCustomAlertAction(title: Validation.logOut, buttonType: .yesButton, from: self, topImageName: "ic_logout", buttonOneTitleText: GeneralConstants.logOut, buttonOneBGColor: UIColor(named: "ThemButtonBGLightGrayColor") ?? .gray)//buttonOneTitleColor: ,
    }
}

// MARK:- Assemble Registration
class SettingsViewControllerAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.storyboardInitCompleted(SettingsViewController.self) { (resolver, viewController) in
            
            let viewModel = resolver.resolve(SettingsViewModel.self)!
            viewController.configure(with: viewModel)
        }
    }
}
