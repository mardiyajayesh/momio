//
//  UpdatePasswordViewModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//

import Foundation
import UIKit
import Swinject
import RxSwift
import MBProgressHUD

// MARK:- UpdatePasswordViewState -
struct UpdatePasswordViewState {
    
    var isLoading: Bool
    var status: ApiStatus?
    var apiDataModel: LoginModel?
    
    static func initial() -> UpdatePasswordViewState {
        return UpdatePasswordViewState(isLoading: false, status: nil)
    }
    
    static func loading() -> UpdatePasswordViewState {
        return UpdatePasswordViewState(isLoading: true, status: nil)
    }
}

// MARK:- Protocol -
protocol UpdatePasswordViewModel {
    func getLiveTvData(action: String, page: Int)
    func updatePasswordViewStateObservable() -> Observable<UpdatePasswordViewState>
}
// MARK:- UpdatePasswordViewModelImpl -
class UpdatePasswordViewModelImpl {
    
    // DisposeBag for RxSwift (i.e.: RxKeyboard)
    private var disposeBag = DisposeBag()
    
    // UpdatePasswordViewState Publisher
    private var updatePasswordViewStatePublisher = PublishSubject<UpdatePasswordViewState>()
    
    // NetworkModule Object
    private let networkManager: NetworkModuleDao
    
    /**
     Designated Initializer
     - parameter networkManager: Network Manager instance used to make the requests
     */
    init(with networkManager: NetworkModuleDao) {
        self.networkManager = networkManager
    }
}
// MARK:- UpdatePasswordViewModelImpl Extension -
extension UpdatePasswordViewModelImpl: UpdatePasswordViewModel {
    /**
     View State Observable
     */
    func updatePasswordViewStateObservable() -> Observable<UpdatePasswordViewState> {
        return updatePasswordViewStatePublisher.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .startWith(.initial())
    }
    /**
     For Api Calling
     */
    func getLiveTvData(action: String, page: Int) {
        /*
        print("getLiveTvData param: action: \(action), page: \(page)")
        self.updatePasswordViewStatePublisher.onNext(UpdatePasswordViewState.loading())
        
        self.networkManager.getTimeLineList(action: action, page: page)
            .map { [unowned self] in
                self.mapResponse(apiDataModel: $0)
            }
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { (UpdatePasswordViewState) in
                self.updatePasswordViewStatePublisher.onNext(UpdatePasswordViewState)
            }, onError: { (error) in
                print(error)
                self.updatePasswordViewStatePublisher.onNext(
                    UpdatePasswordViewState(
                        isLoading: false, status: ApiStatus.error(error: error.localizedDescription)
                    )
                )
            }).disposed(by: self.disposeBag)
        */
    }
    /**
     For Mapping Api Response
     */
    private func mapResponse(apiDataModel: LoginModel) -> UpdatePasswordViewState {
        
        guard let success = apiDataModel.code, success == 200 || success == 1 else {
            return UpdatePasswordViewState(isLoading: false, status: .failure, apiDataModel: apiDataModel)
        }
        return UpdatePasswordViewState(isLoading: false, status: .success, apiDataModel: apiDataModel)
    }
}

// MARK:- Swinject Assembly -
class UpdatePasswordViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        
        //  NetworkModule
        container.register(UpdatePasswordViewModel.self) { resolver in
            let networkManager = resolver.resolve(NetworkModuleDao.self)!
            return UpdatePasswordViewModelImpl(with: networkManager)
        }
    }
}
