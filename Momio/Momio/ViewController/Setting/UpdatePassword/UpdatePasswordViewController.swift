//
//  UpdatePasswordViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 12/10/21.
//

import UIKit
import Swinject
import RxSwift

class UpdatePasswordViewController: BaseViewController {
    // MARK:- IBOutlets -
    @IBOutlet weak var labelTitle: DesignableLabel!
    @IBOutlet weak var labelNote: UILabel!
    @IBOutlet weak var labelSecondNote: UILabel!
    @IBOutlet weak var buttonOkPleaseSend: DesignableButton!
    // MARK:- Properties -
    public var email = ""
    
    // ViewModel
    private var viewModel: UpdatePasswordViewModel!
    // Bag
    private let bag = DisposeBag()
    
    // Swinject Initializer for the UpdatePasswordViewController
    func configure(with ViewModel: UpdatePasswordViewModel) {
        self.viewModel = ViewModel
    }
    // MARK:- View Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        //ViewModelConfig
        self.viewModel.updatePasswordViewStateObservable()
            .subscribe(onNext: { [weak self] viewState in
                self?.render(viewState: viewState)
            }, onError: { error in
            }).disposed(by: self.bag)
        //        self.viewModel.getLiveTvData(action: "get-timeline-post", page: 1)//, murchant_id: "", userType: ""
        self.setupUI()
    }
}

// MARK:- Custom Methods -
extension UpdatePasswordViewController {
    /**
     For Get  Api Response
     */
    func render(viewState: UpdatePasswordViewState) {
        if viewState.isLoading {
            self.showProgressHud(text: "Loading...")
        } else {
            self.hideProgressHud()
        }
        guard let status = viewState.status else  { return }
        switch (status) {
        case .success:
            print("success")
            
        case .failure:
            print("failure")
            
        case .error( _):
            print("error")
            
        }
    }
    /**
     For UI Setup
     */
    func setupUI() {
        self.labelTitle.text = SettingConstants.password
        self.labelNote.text = SettingConstants.weWillSendVerificationCodeToYourEmail
        let fullNoteText = SettingConstants.setEmailAddress + self.email
        self.labelSecondNote.text = fullNoteText
        self.buttonOkPleaseSend.setTitle(GeneralConstants.okPleaseSend, for: .normal)
    }
    /**
     For Generate rendom Digit Code 
     */
    func randomCode(numDigits: Int) -> String {
        var string = ""
        for _ in 0..<numDigits {
            string += String(Int.random(in: 0...9))
        }
        return string
    }
}

// MARK:- Button Actions -
extension UpdatePasswordViewController {
    
    @IBAction func buttonTapOkPleaseSend(_ sender: DesignableButton) {
        
        let destination = UIStoryboard(name: "OtpVerificationViewController", bundle: nil)
        let initialViewController = destination.instantiateViewController(withIdentifier: "OtpVerificationViewController") as! OtpVerificationViewController
        initialViewController.targetCode = self.randomCode(numDigits: 4)
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
}

// MARK:- Assemble Registration
class UpdatePasswordViewControllerAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.storyboardInitCompleted(UpdatePasswordViewController.self) { (resolver, viewController) in
            
            let viewModel = resolver.resolve(UpdatePasswordViewModel.self)!
            viewController.configure(with: viewModel)
        }
    }
}

