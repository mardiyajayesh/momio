//
//  WebViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 13/10/21.
//

import UIKit
import WebKit
import Swinject
import RxSwift

class WebViewController: BaseViewController {
    // MARK:- IBOutlets -
    @IBOutlet weak var labelTitle: DesignableLabel!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var textView: UITextView!
    // MARK:- Properties -
    public var strTitle = ""
    public var strText = ""
    public var strUrl = ""
    
    // ViewModel
    private var viewModel: WebViewModel!
    // Bag
    private let bag = DisposeBag()
    
    // Swinject Initializer for the WebViewController
    func configure(with ViewModel: WebViewModel) {
        self.viewModel = ViewModel
    }
    // MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //ViewModelConfig
        self.viewModel.webViewStateObservable()
            .subscribe(onNext: { [weak self] viewState in
                self?.render(viewState: viewState)
            }, onError: { error in
            }).disposed(by: self.bag)
        //        self.viewModel.getLiveTvData(action: "get-timeline-post", page: 1)//, murchant_id: "", userType: ""
        self.setupUI()
    }
}

// MARK:- Custom Method -
extension WebViewController {
    /**
     For Get Api Response
     */
    func render(viewState: WebViewState) {
        if viewState.isLoading {
            self.showProgressHud(text: "Loading...")
        } else {
            self.hideProgressHud()
        }
        guard let status = viewState.status else  { return }
        switch (status) {
        case .success:
            print("success")
            
        case .failure:
            print("failure")
            
        case .error( _):
            print("error")
            
        }
    }
    /**
     For  UI Setup
     */
    func setupUI() {
        self.webView.navigationDelegate = self
        self.labelTitle.text = strTitle
        self.openUrl()
    }
    /**
     For Open URL
     */
    func openUrl() {
        self.showProgressHud()
        let url = URL(string: self.strUrl)
        self.webView.load(URLRequest(url: url!))
    }
}

// MARK:- WebKit Delegate Method -
extension WebViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        print("Start loading")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("End loading")
        self.hideProgressHud()
    }
}

// MARK:- Assemble Registration
class WebViewControllerAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.storyboardInitCompleted(WebViewController.self) { (resolver, viewController) in
            
            let viewModel = resolver.resolve(WebViewModel.self)!
            viewController.configure(with: viewModel)
        }
    }
}
