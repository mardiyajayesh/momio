//
//  WebViewModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//

import Foundation
import UIKit
import Swinject
import RxSwift
import MBProgressHUD

// MARK:- WebViewState -
struct WebViewState {
    
    var isLoading: Bool
    var status: ApiStatus?
    var apiDataModel: LoginModel?
    
    static func initial() -> WebViewState {
        return WebViewState(isLoading: false, status: nil)
    }
    
    static func loading() -> WebViewState {
        return WebViewState(isLoading: true, status: nil)
    }
}

// MARK:- Protocol -
protocol WebViewModel {
    func getLiveTvData(action: String, page: Int)
    func webViewStateObservable() -> Observable<WebViewState>
}
// MARK:- WebViewModelImpl -
class WebViewModelImpl {
    
    // DisposeBag for RxSwift (i.e.: RxKeyboard)
    private var disposeBag = DisposeBag()
    
    // WebViewState Publisher
    private var webViewStatePublisher = PublishSubject<WebViewState>()
    
    // NetworkModule Object
    private let networkManager: NetworkModuleDao
    
    /**
     Designated Initializer
     - parameter networkManager: Network Manager instance used to make the requests
     */
    init(with networkManager: NetworkModuleDao) {
        self.networkManager = networkManager
    }
}
// MARK:- WebViewModelImpl Extension -
extension WebViewModelImpl: WebViewModel {
    /**
     View State Observable
     */
    func webViewStateObservable() -> Observable<WebViewState> {
        return webViewStatePublisher.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .startWith(.initial())
    }
    /**
     For Api Calling
     */
    func getLiveTvData(action: String, page: Int) {
        /*
        print("getLiveTvData param: action: \(action), page: \(page)")
        self.webViewStatePublisher.onNext(WebViewState.loading())
        
        self.networkManager.getTimeLineList(action: action, page: page)
            .map { [unowned self] in
                self.mapResponse(apiDataModel: $0)
            }
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { (WebViewState) in
                self.webViewStatePublisher.onNext(WebViewState)
            }, onError: { (error) in
                print(error)
                self.webViewStatePublisher.onNext(
                    WebViewState(
                        isLoading: false, status: ApiStatus.error(error: error.localizedDescription)
                    )
                )
            }).disposed(by: self.disposeBag)
        */
    }
    /**
     For Mapping Api Response
     */
    private func mapResponse(apiDataModel: LoginModel) -> WebViewState {
        
        guard let success = apiDataModel.code, success == 200 || success == 1 else {
            return WebViewState(isLoading: false, status: .failure, apiDataModel: apiDataModel)
        }
        return WebViewState(isLoading: false, status: .success, apiDataModel: apiDataModel)
    }
}

// MARK:- Swinject Assembly -
class WebViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        
        //  NetworkModule
        container.register(WebViewModel.self) { resolver in
            let networkManager = resolver.resolve(NetworkModuleDao.self)!
            return WebViewModelImpl(with: networkManager)
        }
    }
}
