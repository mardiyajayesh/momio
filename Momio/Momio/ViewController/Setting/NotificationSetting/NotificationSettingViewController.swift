//
//  NotificationSettingViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 13/10/21.
//

import UIKit
import Swinject
import RxSwift

class NotificationSettingViewController: BaseViewController {
    // MARK:- IBOutlets -
    @IBOutlet weak var labelTitle: DesignableLabel!
    @IBOutlet weak var labelTurnOnOffNotification: UILabel!
    @IBOutlet weak var labelYouCanControlNotification: UILabel!
    @IBOutlet weak var label1TapNotification: UILabel!
    @IBOutlet weak var label2TurnAllowNotification: UILabel!
    
    @IBOutlet weak var buttonGotoIosSetting: DesignableButton!
    
    // ViewModel
    private var viewModel: NotificationSettingViewModel!
    // Bag
    private let bag = DisposeBag()
    
    // Swinject Initializer for the NotificationSettingViewController
    func configure(with ViewModel: NotificationSettingViewModel) {
        self.viewModel = ViewModel
    }
    // MARK:- View Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        //ViewModelConfig
        self.viewModel.notificationSettingViewStateObservable()
            .subscribe(onNext: { [weak self] viewState in
                self?.render(viewState: viewState)
            }, onError: { error in
                
            }).disposed(by: self.bag)
        //        self.viewModel.getLiveTvData(action: "get-timeline-post", page: 1)//, murchant_id: "", userType: ""
        self.setupUI()
    }
}

// MARK:- Custom Methods -
extension NotificationSettingViewController {
    /**
     For Get Api Response
     */
    func render(viewState: NotificationSettingViewState) {
        if viewState.isLoading {
            self.showProgressHud(text: "Loading...")
        } else {
            self.hideProgressHud()
        }
        guard let status = viewState.status else  { return }
        switch (status) {
        case .success:
            print("success")
            
        case .failure:
            print("failure")
            
        case .error( _):
            print("error")
            
        }
    }
    /**
     For UI Setup
     */
    func setupUI() {
        labelTitle.text = SettingConstants.pushNotification
    }
}

// MARK:- Button Action -
extension NotificationSettingViewController {
    
    @IBAction func buttonTapGoToIosSettings(_ sender: DesignableButton) {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                
            })
        }
    }
}

// MARK:- Assemble Registration
class NotificationSettingViewControllerAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.storyboardInitCompleted(NotificationSettingViewController.self) { (resolver, viewController) in
            
            let viewModel = resolver.resolve(NotificationSettingViewModel.self)!
            viewController.configure(with: viewModel)
        }
    }
}
