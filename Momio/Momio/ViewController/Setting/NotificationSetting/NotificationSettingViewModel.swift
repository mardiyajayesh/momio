//
//  NotificationSettingViewModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//

import Foundation
import UIKit
import Swinject
import RxSwift
import MBProgressHUD

// MARK:- NotificationSettingViewState -
struct NotificationSettingViewState {
    
    var isLoading: Bool
    var status: ApiStatus?
    var apiDataModel: LoginModel?
    
    static func initial() -> NotificationSettingViewState {
        return NotificationSettingViewState(isLoading: false, status: nil)
    }
    
    static func loading() -> NotificationSettingViewState {
        return NotificationSettingViewState(isLoading: true, status: nil)
    }
}

// MARK:- Protocol -
protocol NotificationSettingViewModel {
    func getLiveTvData(action: String, page: Int)
    func notificationSettingViewStateObservable() -> Observable<NotificationSettingViewState>
}
// MARK:- NotificationSettingViewModelImpl -
class NotificationSettingViewModelImpl {
    
    // DisposeBag for RxSwift (i.e.: RxKeyboard)
    private var disposeBag = DisposeBag()
    
    // NotificationSettingViewState Publisher
    private var notificationSettingViewStatePublisher = PublishSubject<NotificationSettingViewState>()
    
    // NetworkModule Object
    private let networkManager: NetworkModuleDao
    
    /**
     Designated Initializer
     - parameter networkManager: Network Manager instance used to make the requests
     */
    init(with networkManager: NetworkModuleDao) {
        self.networkManager = networkManager
    }
}
// MARK:- NotificationSettingViewModelImpl Extension -
extension NotificationSettingViewModelImpl: NotificationSettingViewModel {
    /**
     View State Observable
     */
    func notificationSettingViewStateObservable() -> Observable<NotificationSettingViewState> {
        return notificationSettingViewStatePublisher.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .startWith(.initial())
    }
    /**
     For api Calling
     */
    func getLiveTvData(action: String, page: Int) {
        /*
        print("getLiveTvData param: action: \(action), page: \(page)")
        self.notificationSettingViewStatePublisher.onNext(NotificationSettingViewState.loading())
        
        self.networkManager.getTimeLineList(action: action, page: page)
            .map { [unowned self] in
                self.mapResponse(apiDataModel: $0)
            }
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { (NotificationSettingViewState) in
                self.notificationSettingViewStatePublisher.onNext(NotificationSettingViewState)
            }, onError: { (error) in
                print(error)
                self.notificationSettingViewStatePublisher.onNext(
                    NotificationSettingViewState(
                        isLoading: false, status: ApiStatus.error(error: error.localizedDescription)
                    )
                )
            }).disposed(by: self.disposeBag)
        */
    }
    /**
     For Mapping Api Response
     */
    private func mapResponse(apiDataModel: LoginModel) -> NotificationSettingViewState {
        
        guard let success = apiDataModel.code, success == 200 || success == 1 else {
            return NotificationSettingViewState(isLoading: false, status: .failure, apiDataModel: apiDataModel)
        }
        return NotificationSettingViewState(isLoading: false, status: .success, apiDataModel: apiDataModel)
    }
}

// MARK:- Swinject Assembly -
class NotificationSettingViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        
        //  NetworkModule
        container.register(NotificationSettingViewModel.self) { resolver in
            let networkManager = resolver.resolve(NetworkModuleDao.self)!
            return NotificationSettingViewModelImpl(with: networkManager)
        }
    }
}
