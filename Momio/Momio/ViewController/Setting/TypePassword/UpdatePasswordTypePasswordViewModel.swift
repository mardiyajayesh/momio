//
//  UpdatePasswordTypePasswordViewModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//

import Foundation
import UIKit
import Swinject
import RxSwift
import MBProgressHUD

// MARK:- UpdatePasswordTypePasswordViewState -
struct UpdatePasswordTypePasswordViewState {
    
    var isLoading: Bool
    var status: ApiStatus?
    var apiDataModel: LoginModel?
    
    static func initial() -> UpdatePasswordTypePasswordViewState {
        return UpdatePasswordTypePasswordViewState(isLoading: false, status: nil)
    }
    
    static func loading() -> UpdatePasswordTypePasswordViewState {
        return UpdatePasswordTypePasswordViewState(isLoading: true, status: nil)
    }
}

// MARK:- Protocol -
protocol UpdatePasswordTypePasswordViewModel {
    func getLiveTvData(action: String, page: Int)
    func updatePasswordTypePasswordViewStateObservable() -> Observable<UpdatePasswordTypePasswordViewState>
}
// MARK:- UpdatePasswordTypePasswordViewModelImpl -
class UpdatePasswordTypePasswordViewModelImpl {
    
    // DisposeBag for RxSwift (i.e.: RxKeyboard)
    private var disposeBag = DisposeBag()
    
    // UpdatePasswordTypePasswordViewState Publisher
    private var updatePasswordTypePasswordViewStatePublisher = PublishSubject<UpdatePasswordTypePasswordViewState>()
    
    // NetworkModule Object
    private let networkManager: NetworkModuleDao
    
    /**
     Designated Initializer
     - parameter networkManager: Network Manager instance used to make the requests
     */
    init(with networkManager: NetworkModuleDao) {
        self.networkManager = networkManager
    }
}
// MARK:- UpdatePasswordTypePasswordViewModelImpl Extension -
extension UpdatePasswordTypePasswordViewModelImpl: UpdatePasswordTypePasswordViewModel {
    /**
     View State Observable
     */
    func updatePasswordTypePasswordViewStateObservable() -> Observable<UpdatePasswordTypePasswordViewState> {
        return updatePasswordTypePasswordViewStatePublisher.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .startWith(.initial())
    }
    /**
     For Api Calling
     */
    func getLiveTvData(action: String, page: Int) {
        /*
        print("getLiveTvData param: action: \(action), page: \(page)")
        self.updatePasswordTypePasswordViewStatePublisher.onNext(UpdatePasswordTypePasswordViewState.loading())
        
        self.networkManager.getTimeLineList(action: action, page: page)
            .map { [unowned self] in
                self.mapResponse(apiDataModel: $0)
            }
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { (UpdatePasswordTypePasswordViewState) in
                self.updatePasswordTypePasswordViewStatePublisher.onNext(UpdatePasswordTypePasswordViewState)
            }, onError: { (error) in
                print(error)
                self.updatePasswordTypePasswordViewStatePublisher.onNext(
                    UpdatePasswordTypePasswordViewState(
                        isLoading: false, status: ApiStatus.error(error: error.localizedDescription)
                    )
                )
            }).disposed(by: self.disposeBag)
        */
    }
    /**
     For Mapping Api Response
     */
    private func mapResponse(apiDataModel: LoginModel) -> UpdatePasswordTypePasswordViewState {
        
        guard let success = apiDataModel.code, success == 200 || success == 1 else {
            return UpdatePasswordTypePasswordViewState(isLoading: false, status: .failure, apiDataModel: apiDataModel)
        }
        return UpdatePasswordTypePasswordViewState(isLoading: false, status: .success, apiDataModel: apiDataModel)
    }
}

// MARK:- Swinject Assembly -
class UpdatePasswordTypePasswordViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        
        //  NetworkModule
        container.register(UpdatePasswordTypePasswordViewModel.self) { resolver in
            let networkManager = resolver.resolve(NetworkModuleDao.self)!
            return UpdatePasswordTypePasswordViewModelImpl(with: networkManager)
        }
    }
}
