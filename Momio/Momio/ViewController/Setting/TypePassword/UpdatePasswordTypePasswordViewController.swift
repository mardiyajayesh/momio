//
//  UpdatePasswordTypePasswordViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 12/10/21.
//

import UIKit
import Swinject
import RxSwift

class UpdatePasswordTypePasswordViewController: BaseViewController {
    // MARK:- IBOutlets -
    @IBOutlet weak var labelTitle: DesignableLabel!
    @IBOutlet weak var labelNote: DesignableLabel!
    @IBOutlet weak var labelYear: UILabel!
    @IBOutlet weak var labelYearValue: UILabel!
    @IBOutlet weak var labelMonth: UILabel!
    @IBOutlet weak var labelMonthValue: UILabel!
    @IBOutlet weak var labelDay: UILabel!
    @IBOutlet weak var labelDayValue: UILabel!
    
    @IBOutlet weak var textFieldPassword: DesignableTextField!
    @IBOutlet weak var textFieldYear: UITextField!
    @IBOutlet weak var textFieldMonth: UITextField!
    @IBOutlet weak var textFieldDay: UITextField!
    
    @IBOutlet weak var buttonShowHidePassword: DesignableButton!
    @IBOutlet weak var buttonRight: DesignableButton!
    @IBOutlet weak var buttonBack: DesignableButton!
    @IBOutlet weak var viewBirthday: UIView!
    @IBOutlet weak var viewPassword: UIView!
    // MARK:- Properties -
    private var text: String = ""
    public var isForBirthDay: Bool = false
    
    // ViewModel
    private var viewModel: UpdatePasswordTypePasswordViewModel!
    // Bag
    private let bag = DisposeBag()
    
    // Swinject Initializer for the UpdatePasswordTypePasswordViewController
    func configure(with ViewModel: UpdatePasswordTypePasswordViewModel) {
        self.viewModel = ViewModel
    }
    // MARK:- View Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        if isForBirthDay {
            
        } else {
            self.textFieldPassword.becomeFirstResponder()
        }
        //ViewModelConfig
        self.viewModel.updatePasswordTypePasswordViewStateObservable()
            .subscribe(onNext: { [weak self] viewState in
                self?.render(viewState: viewState)
            }, onError: { error in
            }).disposed(by: self.bag)
        //        self.viewModel.getLiveTvData(action: "get-timeline-post", page: 1)//, murchant_id: "", userType: ""
        self.setupUI()
    }
}

// MARK:- Custom Methods
extension UpdatePasswordTypePasswordViewController {
    /**
     For Get Api Response
     */
    func render(viewState: UpdatePasswordTypePasswordViewState) {
        if viewState.isLoading {
            self.showProgressHud(text: "Loading...")
        } else {
            self.hideProgressHud()
        }
        guard let status = viewState.status else  { return }
        switch (status) {
        case .success:
            print("success")
            
        case .failure:
            print("failure")
            
        case .error( _):
            print("error")
            
        }
    }
    /**
     For UI Setup
     */
    func setupUI() {
        self.viewBirthday.isHidden = !isForBirthDay
        self.viewPassword.isHidden = isForBirthDay
        if isForBirthDay {
            self.labelNote.isHidden = true
            self.labelTitle.text = SettingConstants.myBirthDay
            self.labelErrorMessage?.text = Validation.wrongPassword
            self.buttonBack.setImage(UIImage(named: "close"), for: .normal)
            self.buttonRight.backgroundColor = UIColor(named: "ThemButtonBGRedColor")
        } else {
            self.labelTitle.text = SettingConstants.password
            self.labelNote.text = SettingConstants.pleaseInputNewPassword
            self.buttonBack.setImage(UIImage(named: "ic_back_blue"), for: .normal)
            self.buttonRight.backgroundColor = UIColor(named: "ThemButtonBGLightGrayColor")
            //        self.labelErrorMessage?.isHidden = false
            self.setupErrorAttributeLabel()
        }
        self.textFieldYear.attributedPlaceholder = NSAttributedString(string: self.textFieldYear.placeholder ?? "",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "ThemButtonBGLightGrayColor") ?? .blue])
        self.textFieldMonth.attributedPlaceholder = NSAttributedString(string: self.textFieldMonth.placeholder ?? "",
                                                                       attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "ThemButtonBGLightGrayColor") ?? .blue])
        self.textFieldDay.attributedPlaceholder = NSAttributedString(string: self.textFieldDay.placeholder ?? "",
                                                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "ThemButtonBGLightGrayColor") ?? .blue])
    }
    /**
     For Setup Attribute Label
     */
    func setupErrorAttributeLabel() {
        let fullString = Validation.pleaseSetPasswordAtLeast + " " + Validation.passwordForExample + Validation.commaSpace + Validation.passwordFormate
        self.labelErrorMessage?.text = fullString
        
        let textColor: UIColor = .red
        let underLineColor: UIColor = .black
        let underLineStyle = NSUnderlineStyle.single.rawValue
        
        let attributedString1 = NSMutableAttributedString(string: Validation.pleaseSetPasswordAtLeast, attributes: [NSAttributedString.Key.foregroundColor: textColor])
        let appendCommString = Validation.passwordForExample + Validation.commaSpace
        let attributedString2 = NSMutableAttributedString(string: appendCommString, attributes: [NSAttributedString.Key.foregroundColor: underLineColor])
        let attributedString3 = NSMutableAttributedString(string: Validation.passwordFormate, attributes: [NSAttributedString.Key.font: UIFont(name: "SF Pro Display Bold", size: 12)!,NSAttributedString.Key.underlineStyle: underLineStyle, NSAttributedString.Key.underlineColor: underLineColor, NSAttributedString.Key.foregroundColor: underLineColor])
        
        attributedString2.append(attributedString3)
        attributedString1.append(attributedString2)
        
        self.labelErrorMessage?.attributedText = attributedString1
    }
}

// MARK:- Button Actions -
extension UpdatePasswordTypePasswordViewController {
    /**
     For Password SecureText Show Hide
     */
    @IBAction func buttonTapShowHidePassword(_ sender: DesignableButton) {
        if sender.isSelected {
            self.textFieldPassword.isSecureTextEntry = true
            self.buttonShowHidePassword.tintColor = UIColor(named: "ThemButtonBGLightGrayColor")
        } else {
            self.textFieldPassword.isSecureTextEntry = false
            self.buttonShowHidePassword.tintColor = UIColor(named: "ThemButtonBGBlueColor")
        }
        sender.isSelected = !sender.isSelected
    }
    /**
     For Done-Next
     */
    @IBAction func buttonTapRight(_ sender: DesignableButton) {
        if isForBirthDay {
            if self.textFieldYear.text == "" || self.textFieldMonth.text == "" || self.textFieldDay.text == "" {
                self.labelErrorMessage?.isHidden = false
            } else {
                labelErrorMessage?.isHidden = true
                self.keyboardDismissAndHideError()
            }
        } else {
            if self.textFieldPassword.text?.count == 0 {
                self.labelErrorMessage?.isHidden = false
            } else if self.isValidPassword(password: self.textFieldPassword.text ?? "") == false {
                self.labelErrorMessage?.isHidden = false
            } else {
                //Done
                self.labelErrorMessage?.isHidden = true
                self.keyboardDismissAndHideError()
            }
        }
    }
}

// MARK:- TextField Delegates -
extension UpdatePasswordTypePasswordViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.keyboardDismissAndHideError()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if isForBirthDay {
            var maxLength = 4
            if string.isEmpty {
                text = String(text.dropLast())
            } else {
                text = textField.text! + string
            }
            
            if text.count > 0 {
            } else {
            }
            
            //set max lenght
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString = currentString.replacingCharacters(in: range, with: string) as NSString
            if textField == self.textFieldYear {
                maxLength = 4
                return newString.length <= maxLength
            } else {
                maxLength = 2
                return newString.length <= maxLength //( && intvalue <= 12)
            }
        } else {
            if string.isEmpty {
                text = String(text.dropLast())
            } else {
                text = textField.text! + string
            }
            
            if text.count > 0 {
                self.buttonRight.backgroundColor = UIColor(named: "ThemButtonBGColor")
            } else {
                self.buttonRight.backgroundColor = UIColor(named: "ThemButtonBGLightGrayColor")
            }
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {}
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let year = Calendar.current.component(.year, from: Date())
        let int = Int((textField.text ?? "0") as String) ?? 0
        
        if textField == self.textFieldMonth && int > 12 {
            self.labelErrorMessage?.isHidden = false
            self.textFieldMonth.text = ""
        } else if textField == self.textFieldDay && int > 31 {
            self.labelErrorMessage?.isHidden = false
            self.textFieldDay.text = ""
        } else if textField == self.textFieldYear && int > (year - 4) {
            self.labelErrorMessage?.isHidden = false
            self.textFieldYear.text = ""
        } else {
            self.labelErrorMessage?.isHidden = true
        }
    }
}
// MARK:- Assemble Registration
class UpdatePasswordTypePasswordViewControllerAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.storyboardInitCompleted(UpdatePasswordTypePasswordViewController.self) { (resolver, viewController) in
            
            let viewModel = resolver.resolve(UpdatePasswordTypePasswordViewModel.self)!
            viewController.configure(with: viewModel)
        }
    }
}
