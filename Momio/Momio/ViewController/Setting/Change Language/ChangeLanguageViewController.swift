//
//  ChangeLanguageViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 13/10/21.
//

import UIKit
import Swinject
import RxSwift

class ChangeLanguageViewController: BaseViewController {
    // MARK:- IBOutlets -
    @IBOutlet weak var labelTitle: DesignableLabel!
    @IBOutlet weak var labelNote: DesignableLabel!
    
    @IBOutlet weak var buttonEnglish: DesignableButton!
    @IBOutlet weak var buttonJapanese: DesignableButton!
    @IBOutlet weak var buttonKorean: DesignableButton!
    @IBOutlet weak var buttonLatin: DesignableButton!
    @IBOutlet weak var buttonChinese: DesignableButton!
    @IBOutlet weak var buttonThai: DesignableButton!
    
    // ViewModel
    private var viewModel: ChangeLanguageViewModel!
    // Bag
    private let bag = DisposeBag()
    
    // Swinject Initializer for the ChangeLanguageViewController
    func configure(with ViewModel: ChangeLanguageViewModel) {
        self.viewModel = ViewModel
    }
    // MARK:- View Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        //ViewModelConfig
        self.viewModel.changeLanguageViewStateObservable()
            .subscribe(onNext: { [weak self] viewState in
                self?.render(viewState: viewState)
            }, onError: { error in
            }).disposed(by: self.bag)
        //        self.viewModel.getLiveTvData(action: "get-timeline-post", page: 1)//, murchant_id: "", userType: ""
        self.setupUI()
    }
}

// MARK:- Custome Methods -
extension ChangeLanguageViewController {
    /**
     For Get Api Response
     */
    func render(viewState: ChangeLanguageViewState) {
        if viewState.isLoading {
            self.showProgressHud(text: "Loading...")
        } else {
            self.hideProgressHud()
        }
        guard let status = viewState.status else  { return }
        switch (status) {
        case .success:
            print("success")
            
        case .failure:
            print("failure")
            
        case .error( _):
            print("error")
            
        }
    }
    /**
     Fro UI Setup
     */
    func setupUI() {
        self.labelTitle.text = SettingConstants.language
        self.labelNote.text = SettingConstants.languageNote
        
        self.buttonEnglish.setTitle(SettingConstants.english, for: .normal)
        self.buttonJapanese.setTitle(SettingConstants.japanese, for: .normal)
        self.buttonKorean.setTitle(SettingConstants.korean, for: .normal)
        self.buttonLatin.setTitle(SettingConstants.latin, for: .normal)
        self.buttonChinese.setTitle(SettingConstants.chinese, for: .normal)
        self.buttonThai.setTitle(SettingConstants.thai, for: .normal)
    }
    /**
     For Set UnClicked Button UI
     */
    func deselectAllButton() {
        
        self.buttonEnglish.setTitleColor(UIColor(named: "ThemButtonBGGrayColor"), for: .normal)
        self.buttonEnglish.backgroundColor = UIColor(named: "ThemButtonTextColor")
        
        self.buttonThai.setTitleColor(UIColor(named: "ThemButtonBGGrayColor"), for: .normal)
        self.buttonThai.backgroundColor = UIColor(named: "ThemButtonTextColor")
        
        self.buttonJapanese.setTitleColor(UIColor(named: "ThemButtonBGGrayColor"), for: .normal)
        self.buttonJapanese.backgroundColor = UIColor(named: "ThemButtonTextColor")
        
        self.buttonKorean.setTitleColor(UIColor(named: "ThemButtonBGGrayColor"), for: .normal)
        self.buttonKorean.backgroundColor = UIColor(named: "ThemButtonTextColor")
        
        self.buttonLatin.setTitleColor(UIColor(named: "ThemButtonBGGrayColor"), for: .normal)
        self.buttonLatin.backgroundColor = UIColor(named: "ThemButtonTextColor")
        
        self.buttonChinese.setTitleColor(UIColor(named: "ThemButtonBGGrayColor"), for: .normal)
        self.buttonChinese.backgroundColor = UIColor(named: "ThemButtonTextColor")
        
        self.buttonThai.setTitleColor(UIColor(named: "ThemButtonBGGrayColor"), for: .normal)
        self.buttonThai.backgroundColor = UIColor(named: "ThemButtonTextColor")
    }
    /**
     For Set Clicked Button UI
     */
    func setButtonSelect(sender: DesignableButton) {
        sender.setTitleColor(.white, for: .normal)
        sender.backgroundColor = UIColor(named: "ThemButtonBGBlueColor")
    }
}

// MARK:- Button Actions -
extension ChangeLanguageViewController {
    /**
     For Set English Language
     */
    @IBAction func ButtonTapEnglish(_ sender: DesignableButton) {
        print("English tap")
        self.deselectAllButton()
        self.setButtonSelect(sender: sender)
        UserDefaults.standard.setValue(SelectedLanguage.english.rawValue, forKey: UserDefaultKeysName.language)
    }
    /**
     For Set Japanese Language
     */
    @IBAction func buttonTapJapanese(_ sender: DesignableButton) {
        print("japanese tap")
        self.deselectAllButton()
        self.setButtonSelect(sender: sender)
        UserDefaults.standard.setValue(SelectedLanguage.japanese.rawValue, forKey: UserDefaultKeysName.language)
    }
    /**
     For Set Korean Language
     */
    @IBAction func buttonTapKorean(_ sender: DesignableButton) {
        print("korean tap")
        self.deselectAllButton()
        self.setButtonSelect(sender: sender)
        UserDefaults.standard.setValue(SelectedLanguage.korean.rawValue, forKey: UserDefaultKeysName.language)
    }
    /**
     For Set Latin Language
     */
    @IBAction func buttonTapLatin(_ sender: DesignableButton) {
        print("latin tap")
        self.deselectAllButton()
        self.setButtonSelect(sender: sender)
        UserDefaults.standard.setValue(SelectedLanguage.latin.rawValue, forKey: UserDefaultKeysName.language)
    }
    /**
     For Set Chinese Language
     */
    @IBAction func buttonTapChinese(_ sender: DesignableButton) {
        print("chinese tap")
        self.deselectAllButton()
        self.setButtonSelect(sender: sender)
        UserDefaults.standard.setValue(SelectedLanguage.chinese.rawValue, forKey: UserDefaultKeysName.language)
    }
    /**
     For Set Thai Language
     */
    @IBAction func buttonTapThai(_ sender: DesignableButton) {
        print("thai tap")
        self.deselectAllButton()
        self.setButtonSelect(sender: sender)
        UserDefaults.standard.setValue(SelectedLanguage.thai.rawValue, forKey: UserDefaultKeysName.language)
    }
}

// MARK:- Assemble Registration
class ChangeLanguageViewControllerAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.storyboardInitCompleted(ChangeLanguageViewController.self) { (resolver, viewController) in
            
            let viewModel = resolver.resolve(ChangeLanguageViewModel.self)!
            viewController.configure(with: viewModel)
        }
    }
}
