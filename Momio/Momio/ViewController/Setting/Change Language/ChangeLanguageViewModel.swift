//
//  ChangeLanguageViewModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//

import Foundation
import UIKit
import Swinject
import RxSwift
import MBProgressHUD

// MARK:- ChangeLanguageViewState -
struct ChangeLanguageViewState {
    
    var isLoading: Bool
    var status: ApiStatus?
    var apiDataModel: LoginModel?
    
    static func initial() -> ChangeLanguageViewState {
        return ChangeLanguageViewState(isLoading: false, status: nil)
    }
    
    static func loading() -> ChangeLanguageViewState {
        return ChangeLanguageViewState(isLoading: true, status: nil)
    }
}

// MARK:- Protocol -
protocol ChangeLanguageViewModel {
    func getLiveTvData(action: String, page: Int)
    func changeLanguageViewStateObservable() -> Observable<ChangeLanguageViewState>
}
// MARK:- ChangeLanguageViewModelImpl -
class ChangeLanguageViewModelImpl {
    
    // DisposeBag for RxSwift (i.e.: RxKeyboard)
    private var disposeBag = DisposeBag()
    
    // ChangeLanguageViewState Publisher
    private var changeLanguageViewStatePublisher = PublishSubject<ChangeLanguageViewState>()
    
    // NetworkModule Object
    private let networkManager: NetworkModuleDao
    
    /**
     Designated Initializer
     - parameter networkManager: Network Manager instance used to make the requests
     */
    init(with networkManager: NetworkModuleDao) {
        self.networkManager = networkManager
    }
}
// MARK:- ChangeLanguageViewModelImpl Extension -
extension ChangeLanguageViewModelImpl: ChangeLanguageViewModel {
    /**
     View State Observable
     */
    func changeLanguageViewStateObservable() -> Observable<ChangeLanguageViewState> {
        return changeLanguageViewStatePublisher.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .startWith(.initial())
    }
    /**
     For  Api Calling
     */
    func getLiveTvData(action: String, page: Int) {
        /*
        print("getLiveTvData param: action: \(action), page: \(page)")
        self.changeLanguageViewStatePublisher.onNext(ChangeLanguageViewState.loading())
        
        self.networkManager.getTimeLineList(action: action, page: page)
            .map { [unowned self] in
                self.mapResponse(apiDataModel: $0)
            }
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { (ChangeLanguageViewState) in
                self.changeLanguageViewStatePublisher.onNext(ChangeLanguageViewState)
            }, onError: { (error) in
                print(error)
                self.changeLanguageViewStatePublisher.onNext(
                    ChangeLanguageViewState(
                        isLoading: false, status: ApiStatus.error(error: error.localizedDescription)
                    )
                )
            }).disposed(by: self.disposeBag)
        */
    }
    /**
     For Mapping Api Response
     */
    private func mapResponse(apiDataModel: LoginModel) -> ChangeLanguageViewState {
        
        guard let success = apiDataModel.code, success == 200 || success == 1 else {
            return ChangeLanguageViewState(isLoading: false, status: .failure, apiDataModel: apiDataModel)
        }
        return ChangeLanguageViewState(isLoading: false, status: .success, apiDataModel: apiDataModel)
    }
}

// MARK:- Swinject Assembly -
class ChangeLanguageViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        
        //  NetworkModule
        container.register(ChangeLanguageViewModel.self) { resolver in
            let networkManager = resolver.resolve(NetworkModuleDao.self)!
            return ChangeLanguageViewModelImpl(with: networkManager)
        }
    }
}
