//
//  DeleteAccountViewModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//

import Foundation
import UIKit
import Swinject
import RxSwift
import MBProgressHUD

// MARK:- DeleteAccountViewState -
struct DeleteAccountViewState {
    
    var isLoading: Bool
    var status: ApiStatus?
    var apiDataModel: LoginModel?
    
    static func initial() -> DeleteAccountViewState {
        return DeleteAccountViewState(isLoading: false, status: nil)
    }
    
    static func loading() -> DeleteAccountViewState {
        return DeleteAccountViewState(isLoading: true, status: nil)
    }
}

// MARK:- Protocol -
protocol DeleteAccountViewModel {
    func getLiveTvData(action: String, page: Int)
    func deleteAccountViewStateObservable() -> Observable<DeleteAccountViewState>
}
// MARK:- DeleteAccountViewModelImpl -
class DeleteAccountViewModelImpl {
    
    // DisposeBag for RxSwift (i.e.: RxKeyboard)
    private var disposeBag = DisposeBag()
    
    // DeleteAccountViewState Publisher
    private var deleteAccountViewStatePublisher = PublishSubject<DeleteAccountViewState>()
    
    // NetworkModule Object
    private let networkManager: NetworkModuleDao
    
    /**
     Designated Initializer
     - parameter networkManager: Network Manager instance used to make the requests
     */
    init(with networkManager: NetworkModuleDao) {
        self.networkManager = networkManager
    }
}
// MARK:- DeleteAccountViewModelImpl Extension -
extension DeleteAccountViewModelImpl: DeleteAccountViewModel {
    /**
     View State Observable
     */
    func deleteAccountViewStateObservable() -> Observable<DeleteAccountViewState> {
        return deleteAccountViewStatePublisher.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .startWith(.initial())
    }
    /**
     For Api Calling
     */
    func getLiveTvData(action: String, page: Int) {
        /*
        print("getLiveTvData param: action: \(action), page: \(page)")
        self.deleteAccountViewStatePublisher.onNext(DeleteAccountViewState.loading())
        
        self.networkManager.getTimeLineList(action: action, page: page)
            .map { [unowned self] in
                self.mapResponse(apiDataModel: $0)
            }
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { (DeleteAccountViewState) in
                self.deleteAccountViewStatePublisher.onNext(DeleteAccountViewState)
            }, onError: { (error) in
                print(error)
                self.deleteAccountViewStatePublisher.onNext(
                    DeleteAccountViewState(
                        isLoading: false, status: ApiStatus.error(error: error.localizedDescription)
                    )
                )
            }).disposed(by: self.disposeBag)
        */
    }
    /**
     For Mapping Api Response
     */
    private func mapResponse(apiDataModel: LoginModel) -> DeleteAccountViewState {
        
        guard let success = apiDataModel.code, success == 200 || success == 1 else {
            return DeleteAccountViewState(isLoading: false, status: .failure, apiDataModel: apiDataModel)
        }
        return DeleteAccountViewState(isLoading: false, status: .success, apiDataModel: apiDataModel)
    }
}

// MARK:- Swinject Assembly -
class DeleteAccountViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        
        //  NetworkModule
        container.register(DeleteAccountViewModel.self) { resolver in
            let networkManager = resolver.resolve(NetworkModuleDao.self)!
            return DeleteAccountViewModelImpl(with: networkManager)
        }
    }
}
