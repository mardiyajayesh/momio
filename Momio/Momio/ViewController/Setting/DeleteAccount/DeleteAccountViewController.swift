//
//  DeleteAccountViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 13/10/21.
//

import UIKit
import Swinject
import RxSwift

class DeleteAccountViewController: BaseViewController {
    // MARK:- IBOutlets -
    @IBOutlet weak var labelTitle: DesignableLabel!
    @IBOutlet weak var labelIamFeeling: UILabel!
    @IBOutlet weak var labelFeelingCount: UILabel!
    @IBOutlet weak var labelDeleteAccountNote: UILabel!
    
    @IBOutlet weak var viewFeelingDetail: DesignableView!
    @IBOutlet weak var buttonDeleteMyAccount: DesignableButton!
    
    // ViewModel
    private var viewModel: DeleteAccountViewModel!
    // Bag
    private let bag = DisposeBag()
    
    // Swinject Initializer for the DeleteAccountViewController
    func configure(with ViewModel: DeleteAccountViewModel) {
        self.viewModel = ViewModel
    }
    // MARK:- View Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        //ViewModelConfig
        self.viewModel.deleteAccountViewStateObservable()
            .subscribe(onNext: { [weak self] viewState in
                self?.render(viewState: viewState)
            }, onError: { error in
            }).disposed(by: self.bag)
        //        self.viewModel.getLiveTvData(action: "get-timeline-post", page: 1)//, murchant_id: "", userType: ""
        self.setupUI()
    }
}

// MARK:- Custome Methods -
extension DeleteAccountViewController {
    /**
     For Get Api Response
     */
    func render(viewState: DeleteAccountViewState) {
        if viewState.isLoading {
            self.showProgressHud(text: "Loading...")
        } else {
            self.hideProgressHud()
        }
        guard let status = viewState.status else  { return }
        switch (status) {
        case .success:
            print("success")
            
        case .failure:
            print("failure")
            
        case .error( _):
            print("error")
            
        }
    }
    /**
     For UI Setup
     */
    func setupUI() {
        self.labelTitle.text = SettingConstants.deleteAccount
        
        let mainString = SettingConstants.iAmFeelingHappy
        let stringToColor = "HAPPY!"
        
        let range = (mainString as NSString).range(of: stringToColor)
        let mutableAttributedString = NSMutableAttributedString.init(string: mainString )
        mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(named: "ThemSkyBlueColor") ?? .systemBlue, range: range)
        self.labelIamFeeling.attributedText = mutableAttributedString
    }
}

// MARK:- Button Actions -
extension DeleteAccountViewController {
    
    @IBAction func buttonTapDeleteMyAccount(_ sender: DesignableButton) {}
}

// MARK:- Assemble Registration
class DeleteAccountViewControllerAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.storyboardInitCompleted(DeleteAccountViewController.self) { (resolver, viewController) in
            
            let viewModel = resolver.resolve(DeleteAccountViewModel.self)!
            viewController.configure(with: viewModel)
        }
    }
}
