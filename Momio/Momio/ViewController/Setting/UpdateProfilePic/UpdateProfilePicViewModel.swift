//
//  UpdateProfilePicViewModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//

import Foundation
import UIKit
import Swinject
import RxSwift
import MBProgressHUD

// MARK:- UpdateProfilePicViewState -
struct UpdateProfilePicViewState {
    
    var isLoading: Bool
    var status: ApiStatus?
    var apiDataModel: LoginModel?
    
    static func initial() -> UpdateProfilePicViewState {
        return UpdateProfilePicViewState(isLoading: false, status: nil)
    }
    
    static func loading() -> UpdateProfilePicViewState {
        return UpdateProfilePicViewState(isLoading: true, status: nil)
    }
}

// MARK:- Protocol -
protocol UpdateProfilePicViewModel {
    func getLiveTvData(action: String, page: Int)
    func updateProfilePicViewStateObservable() -> Observable<UpdateProfilePicViewState>
}
// MARK:- UpdateProfilePicViewModelImpl -
class UpdateProfilePicViewModelImpl {
    
    // DisposeBag for RxSwift (i.e.: RxKeyboard)
    private var disposeBag = DisposeBag()
    
    // UpdateProfilePicViewState Publisher
    private var updateProfilePicViewStatePublisher = PublishSubject<UpdateProfilePicViewState>()
    
    // NetworkModule Object
    private let networkManager: NetworkModuleDao
    
    /**
     Designated Initializer
     - parameter networkManager: Network Manager instance used to make the requests
     */
    init(with networkManager: NetworkModuleDao) {
        self.networkManager = networkManager
    }
}
// MARK:- UpdateProfilePicViewModelImpl Extension -
extension UpdateProfilePicViewModelImpl: UpdateProfilePicViewModel {
    /**
     View State Observable
     */
    func updateProfilePicViewStateObservable() -> Observable<UpdateProfilePicViewState> {
        return updateProfilePicViewStatePublisher.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .startWith(.initial())
    }
    /**
     For Api Calling
     */
    func getLiveTvData(action: String, page: Int) {
        /*
        print("getLiveTvData param: action: \(action), page: \(page)")
        self.updateProfilePicViewStatePublisher.onNext(UpdateProfilePicViewState.loading())
        
        self.networkManager.getTimeLineList(action: action, page: page)
            .map { [unowned self] in
                self.mapResponse(apiDataModel: $0)
            }
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { (UpdateProfilePicViewState) in
                self.updateProfilePicViewStatePublisher.onNext(UpdateProfilePicViewState)
            }, onError: { (error) in
                print(error)
                self.updateProfilePicViewStatePublisher.onNext(
                    UpdateProfilePicViewState(
                        isLoading: false, status: ApiStatus.error(error: error.localizedDescription)
                    )
                )
            }).disposed(by: self.disposeBag)
        */
    }
    /**
     For Mapping Api Response
     */
    private func mapResponse(apiDataModel: LoginModel) -> UpdateProfilePicViewState {
        
        guard let success = apiDataModel.code, success == 200 || success == 1 else {
            return UpdateProfilePicViewState(isLoading: false, status: .failure, apiDataModel: apiDataModel)
        }
        return UpdateProfilePicViewState(isLoading: false, status: .success, apiDataModel: apiDataModel)
    }
}

// MARK:- Swinject Assembly -
class UpdateProfilePicViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        
        //  NetworkModule
        container.register(UpdateProfilePicViewModel.self) { resolver in
            let networkManager = resolver.resolve(NetworkModuleDao.self)!
            return UpdateProfilePicViewModelImpl(with: networkManager)
        }
    }
}
