//
//  UpdateProfilePicViewController.swift
// Momio //
//  Created by Jayesh Mardiya on 08/10/21.
//

import UIKit
import RSKImageCropper
import MobileCoreServices
import Swinject
import RxSwift

class UpdateProfilePicViewController: BaseViewController {
    // MARK:- IBOutlets -
    @IBOutlet weak var labelUpdateProfileTitle: DesignableLabel!
    @IBOutlet weak var imageViewProfilePic: DesignableImageView!
    @IBOutlet weak var imageViewProfileCamera: UIImageView!
    
    @IBOutlet weak var buttonBack: DesignableButton!
    @IBOutlet weak var buttonSelectProfile: DesignableButton!
    @IBOutlet weak var buttonRight: DesignableButton!
    // MARK:- Properties -
    var isImageSelected: Bool = false
    var image: UIImage!
    
    // ViewModel
    private var viewModel: UpdateProfilePicViewModel!
    // Bag
    private let bag = DisposeBag()
    
    // Swinject Initializer for the UpdateProfilePicViewController
    func configure(with ViewModel: UpdateProfilePicViewModel) {
        self.viewModel = ViewModel
    }
    // MARK:- View Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        //ViewModelConfig
        self.viewModel.updateProfilePicViewStateObservable()
            .subscribe(onNext: { [weak self] viewState in
                self?.render(viewState: viewState)
            }, onError: { error in
                
            }).disposed(by: self.bag)
        //        self.viewModel.getLiveTvData(action: "get-timeline-post", page: 1)//, murchant_id: "", userType: ""
        self.setupUI()
    }
}

// MARK:- Custom Method -
extension UpdateProfilePicViewController {
    /**
     For Get Api Response
     */
    func render(viewState: UpdateProfilePicViewState) {
        if viewState.isLoading {
            self.showProgressHud(text: "Loading...")
        } else {
            self.hideProgressHud()
        }
        guard let status = viewState.status else  { return }
        switch (status) {
        case .success:
            print("success")
            
        case .failure:
            print("failure")
            
        case .error( _):
            print("error")
            
        }
    }
    /**
     For UI Setup
     */
    func setupUI() {
        imagePicker.delegate = self
    }
    
}

// MARK:- Button Action -
extension UpdateProfilePicViewController {
    @IBAction func buttonTapRight(_ sender: DesignableButton) {
        if !self.isImageSelected {
            self.showCustomAlertAction(title: Validation.emptyProfileImage, buttonType: .yesButton, from: self, buttonOneTitleText: GeneralConstants.ok)
        } else {}
    }
}

// MARK:- UIImagePickerControllerDelegate -
extension UpdateProfilePicViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.isImageSelected = true
            self.image = image
        }
        self.imagePicker.dismiss(animated: false, completion: {})
        let imageCropVC = RSKImageCropViewController.init(image: self.image, cropMode: RSKImageCropMode.circle)
        imageCropVC.chooseButton.setImage(UIImage(named: "right"), for: .normal)
        imageCropVC.chooseButton.setTitle(nil, for: .normal)
        imageCropVC.cancelButton.setImage(UIImage(named: "close"), for: .normal)
        imageCropVC.cancelButton.setTitle(nil, for: .normal)
        imageCropVC.delegate = self;
        self.present(imageCropVC, animated: true, completion: nil)
    }
}

// MARK:- RSKImageCropViewControllerDelegate -
extension UpdateProfilePicViewController: RSKImageCropViewControllerDelegate {
    
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect, rotationAngle: CGFloat) {
        self.imageViewProfilePic.image = croppedImage
        dismiss(animated: true, completion: nil)
    }
}

// MARK:- Assemble Registration
class UpdateProfilePicViewControllerAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.storyboardInitCompleted(UpdateProfilePicViewController.self) { (resolver, viewController) in
            
            let viewModel = resolver.resolve(UpdateProfilePicViewModel.self)!
            viewController.configure(with: viewModel)
        }
    }
}
