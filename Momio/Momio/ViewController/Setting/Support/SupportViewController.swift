//
//  SupportViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 13/10/21.
//

import UIKit
import Swinject
import RxSwift

class SupportViewController: BaseViewController {
    // MARK:- IBOutlets -
    @IBOutlet weak var labelTitle: DesignableLabel!
    
    // ViewModel
    private var viewModel: SupportViewModel!
    // Bag
    private let bag = DisposeBag()
    
    // Swinject Initializer for the SupportViewController
    func configure(with ViewModel: SupportViewModel) {
        self.viewModel = ViewModel
    }
    // MARK:- View Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        //ViewModelConfig
        self.viewModel.supportViewStateObservable()
            .subscribe(onNext: { [weak self] viewState in
                self?.render(viewState: viewState)
            }, onError: { error in
            }).disposed(by: self.bag)
        //        self.viewModel.getLiveTvData(action: "get-timeline-post", page: 1)//, murchant_id: "", userType: ""
        self.setupUI()
    }
}

// MARK:- Custome Method -
extension SupportViewController {
    /**
     For Get Api Response
     */
    func render(viewState: SupportViewState) {
        if viewState.isLoading {
            self.showProgressHud(text: "Loading...")
        } else {
            self.hideProgressHud()
        }
        guard let status = viewState.status else  { return }
        switch (status) {
        case .success:
            print("success")
            
        case .failure:
            print("failure")
            
        case .error( _):
            print("error")
            
        }
    }
    /**
     For UI Setup
     */
    func setupUI() {
        self.labelTitle.text = SettingConstants.support
    }
}
// MARK:- Assemble Registration
class SupportViewControllerAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.storyboardInitCompleted(SupportViewController.self) { (resolver, viewController) in
            
            let viewModel = resolver.resolve(SupportViewModel.self)!
            viewController.configure(with: viewModel)
        }
    }
}

