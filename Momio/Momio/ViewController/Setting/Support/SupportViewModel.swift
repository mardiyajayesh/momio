//
//  SupportViewModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//

import Foundation
import UIKit
import Swinject
import RxSwift
import MBProgressHUD

// MARK:- SupportViewState -
struct SupportViewState {
    
    var isLoading: Bool
    var status: ApiStatus?
    var apiDataModel: LoginModel?
    
    static func initial() -> SupportViewState {
        return SupportViewState(isLoading: false, status: nil)
    }
    
    static func loading() -> SupportViewState {
        return SupportViewState(isLoading: true, status: nil)
    }
}

// MARK:- Protocol -
protocol SupportViewModel {
    func getLiveTvData(action: String, page: Int)
    func supportViewStateObservable() -> Observable<SupportViewState>
}
// MARK:- SupportViewModelImpl -
class SupportViewModelImpl {
    
    // DisposeBag for RxSwift (i.e.: RxKeyboard)
    private var disposeBag = DisposeBag()
    
    // SupportViewState Publisher
    private var supportViewStatePublisher = PublishSubject<SupportViewState>()
    
    // NetworkModule Object
    private let networkManager: NetworkModuleDao
    
    /**
     Designated Initializer
     - parameter networkManager: Network Manager instance used to make the requests
     */
    init(with networkManager: NetworkModuleDao) {
        self.networkManager = networkManager
    }
}
// MARK:- SupportViewModelImpl Extension -
extension SupportViewModelImpl: SupportViewModel {
    /**
     View State Observable
     */
    func supportViewStateObservable() -> Observable<SupportViewState> {
        return supportViewStatePublisher.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .startWith(.initial())
    }
    /**
     For Api Calling
     */
    func getLiveTvData(action: String, page: Int) {
        /*
        print("getLiveTvData param: action: \(action), page: \(page)")
        self.supportViewStatePublisher.onNext(SupportViewState.loading())
        
        self.networkManager.getTimeLineList(action: action, page: page)
            .map { [unowned self] in
                self.mapResponse(apiDataModel: $0)
            }
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { (SupportViewState) in
                self.supportViewStatePublisher.onNext(SupportViewState)
            }, onError: { (error) in
                print(error)
                self.supportViewStatePublisher.onNext(
                    SupportViewState(
                        isLoading: false, status: ApiStatus.error(error: error.localizedDescription)
                    )
                )
            }).disposed(by: self.disposeBag)
        */
    }
    /**
     For Mapping Api Response
     */
    private func mapResponse(apiDataModel: LoginModel) -> SupportViewState {
        
        guard let success = apiDataModel.code, success == 200 || success == 1 else {
            return SupportViewState(isLoading: false, status: .failure, apiDataModel: apiDataModel)
        }
        return SupportViewState(isLoading: false, status: .success, apiDataModel: apiDataModel)
    }
}

// MARK:- Swinject Assembly -
class SupportViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        
        //  NetworkModule
        container.register(SupportViewModel.self) { resolver in
            let networkManager = resolver.resolve(NetworkModuleDao.self)!
            return SupportViewModelImpl(with: networkManager)
        }
    }
}
