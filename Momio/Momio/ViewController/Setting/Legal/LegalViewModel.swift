//
//  LegalViewModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//

import Foundation
import UIKit
import Swinject
import RxSwift
import MBProgressHUD

// MARK:- LegalViewState -
struct LegalViewState {
    
    var isLoading: Bool
    var status: ApiStatus?
    var apiDataModel: LoginModel?
    
    static func initial() -> LegalViewState {
        return LegalViewState(isLoading: false, status: nil)
    }
    
    static func loading() -> LegalViewState {
        return LegalViewState(isLoading: true, status: nil)
    }
}

// MARK:- Protocol -
protocol LegalViewModel {
    func getLiveTvData(action: String, page: Int)
    func legalViewStateObservable() -> Observable<LegalViewState>
}
// MARK:- LegalViewModelImpl -
class LegalViewModelImpl {
    
    // DisposeBag for RxSwift (i.e.: RxKeyboard)
    private var disposeBag = DisposeBag()
    
    // LegalViewState Publisher
    private var legalViewStatePublisher = PublishSubject<LegalViewState>()
    
    // NetworkModule Object
    private let networkManager: NetworkModuleDao
    
    /**
     Designated Initializer
     - parameter networkManager: Network Manager instance used to make the requests
     */
    init(with networkManager: NetworkModuleDao) {
        self.networkManager = networkManager
    }
}
// MARK:- LegalViewModelImpl Extension -
extension LegalViewModelImpl: LegalViewModel {
    /**
     View State Observable
     */
    func legalViewStateObservable() -> Observable<LegalViewState> {
        return legalViewStatePublisher.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .startWith(.initial())
    }
    /**
     For Api Calling
     */
    func getLiveTvData(action: String, page: Int) {
        /*
        print("getLiveTvData param: action: \(action), page: \(page)")
        self.legalViewStatePublisher.onNext(LegalViewState.loading())
        
        self.networkManager.getTimeLineList(action: action, page: page)
            .map { [unowned self] in
                self.mapResponse(apiDataModel: $0)
            }
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { (LegalViewState) in
                self.legalViewStatePublisher.onNext(LegalViewState)
            }, onError: { (error) in
                print(error)
                self.legalViewStatePublisher.onNext(
                    LegalViewState(
                        isLoading: false, status: ApiStatus.error(error: error.localizedDescription)
                    )
                )
            }).disposed(by: self.disposeBag)
        */
    }
    /**
     For Mapping Api Response
     */
    private func mapResponse(apiDataModel: LoginModel) -> LegalViewState {
        
        guard let success = apiDataModel.code, success == 200 || success == 1 else {
            return LegalViewState(isLoading: false, status: .failure, apiDataModel: apiDataModel)
        }
        return LegalViewState(isLoading: false, status: .success, apiDataModel: apiDataModel)
    }
}

// MARK:- Swinject Assembly -
class LegalViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        
        //  NetworkModule
        container.register(LegalViewModel.self) { resolver in
            let networkManager = resolver.resolve(NetworkModuleDao.self)!
            return LegalViewModelImpl(with: networkManager)
        }
    }
}
