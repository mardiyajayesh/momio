//
//  LegalViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 13/10/21.
//

import UIKit
import Swinject
import RxSwift

class LegalViewController: BaseViewController {
    // MARK:- IBOutlets -
    @IBOutlet weak var labelTitle: DesignableLabel!
    @IBOutlet weak var labelNote: DesignableLabel!
    @IBOutlet weak var labelMadeInJapan: UILabel!
    @IBOutlet weak var labelCopyright: UILabel!
    
    @IBOutlet weak var buttonTermOfService: DesignableButton!
    @IBOutlet weak var buttonPrivacyPolicy: DesignableButton!
    @IBOutlet weak var buttonLegalNotice: DesignableButton!
    
    // ViewModel
    private var viewModel: LegalViewModel!
    // Bag
    private let bag = DisposeBag()
    
    // Swinject Initializer for the LegalViewController
    func configure(with ViewModel: LegalViewModel) {
        self.viewModel = ViewModel
    }
    // MARK:- View Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        //ViewModelConfig
        self.viewModel.legalViewStateObservable()
            .subscribe(onNext: { [weak self] viewState in
                self?.render(viewState: viewState)
            }, onError: { error in
            }).disposed(by: self.bag)
        //        self.viewModel.getLiveTvData(action: "get-timeline-post", page: 1)//, murchant_id: "", userType: ""
        self.setupUI()
        // Do any additional setup after loading the view.
    }
}

// MARK:- Custom Method -
extension LegalViewController {
    /**
     For Get Api Response
     */
    func render(viewState: LegalViewState) {
        if viewState.isLoading {
            self.showProgressHud(text: "Loading...")
        } else {
            self.hideProgressHud()
        }
        guard let status = viewState.status else  { return }
        switch (status) {
        case .success:
            print("success")
            
        case .failure:
            print("failure")
            
        case .error( _):
            print("error")
            
        }
    }
    /**
     For UI Setup
     */
    func setupUI() {
        self.labelTitle.text = SettingConstants.legal
        self.labelMadeInJapan.text = SettingConstants.medInJapan
        self.labelCopyright.text = SettingConstants.copyright
    }
}

// MARK:- Button Action -
extension LegalViewController {
    /**
     For open Terms Of Service
     */
    @IBAction func buttonTapTermOfService(_ sender: DesignableButton) {
        self.openWebViewWith(title: SettingConstants.termOfService.uppercased(), strUrl: "https://www.google.co.in/")
    }
    /**
     For open PrivacyPolicy
     */
    @IBAction func buttonTapPrivacyPolicy(_ sender: DesignableButton) {
        self.openWebViewWith(title: SettingConstants.privecyPolicy.uppercased(), strUrl: "https://www.google.co.in/")
    }
    /**
     For open Legal Notice
     */
    @IBAction func buttonTapLegalNotice(_ sender: DesignableButton) {
        self.openWebViewWith(title: SettingConstants.legalNotice.uppercased(), strUrl: "https://www.google.co.in/")
    }
}
// MARK:- Assemble Registration
class LegalViewControllerAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.storyboardInitCompleted(LegalViewController.self) { (resolver, viewController) in
            
            let viewModel = resolver.resolve(LegalViewModel.self)!
            viewController.configure(with: viewModel)
        }
    }
}
