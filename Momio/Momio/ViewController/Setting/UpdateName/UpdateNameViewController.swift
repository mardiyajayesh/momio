//
//  UpdateNameViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 08/10/21.
//

import UIKit
import Swinject
import RxSwift

class UpdateNameViewController: BaseViewController {
    // MARK:- IBOutlets -
    @IBOutlet weak var labelTitle: DesignableLabel!
    
    @IBOutlet weak var labelNote: UILabel!
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var buttonRight: DesignableButton!
    
    @IBOutlet weak var textFieldWidthConstraint: NSLayoutConstraint!
    // MARK:- Properties -
    private var text: String = ""
    public var nameText: String = ""
    public var updateType: UpdateType?
    
    // ViewModel
    private var viewModel: UpdateNameViewModel!
    // Bag
    private let bag = DisposeBag()
    
    // Swinject Initializer for the UpdateNameViewController
    func configure(with ViewModel: UpdateNameViewModel) {
        self.viewModel = ViewModel
    }
    // MARK:- View Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textFieldName.becomeFirstResponder()
        //ViewModelConfig
        self.viewModel.updateNameViewStateObservable()
            .subscribe(onNext: { [weak self] viewState in
                self?.render(viewState: viewState)
            }, onError: { error in
                
            }).disposed(by: self.bag)
        //        self.viewModel.getLiveTvData(action: "get-timeline-post", page: 1)//, murchant_id: "", userType: ""
        self.setupUI()
    }
    
    //Custome Alert Button Method
    override func buttonTapAlertOk() {
        
    }
}

// MARK:- Custome Methods -
extension UpdateNameViewController {
    /**
     For Get Api Response
     */
    func render(viewState: UpdateNameViewState) {
        if viewState.isLoading {
            self.showProgressHud(text: "Loading...")
        } else {
            self.hideProgressHud()
        }
        guard let status = viewState.status else  { return }
        switch (status) {
        case .success:
            print("success")
            
        case .failure:
            print("failure")
            
        case .error( _):
            print("error")
            
        }
    }
    /**
     For UI Setup
     */
    func setupUI() {
        
        self.textFieldName.text = self.nameText
        if self.updateType == .username {
            self.labelTitle.text = SettingConstants.myUserName
            self.labelNote.text = SettingConstants.userNameThatOnlyYou
            self.textFieldWidthConstraint.constant = 50
        } else if self.updateType == .email {
            self.labelTitle.text = SettingConstants.myEmail
            let fullNoteText = SettingConstants.setEmailAddress + (self.textFieldName.text ?? "")
            self.labelNote.text = fullNoteText
            self.textFieldName.keyboardType = .emailAddress
        } else {
            self.labelTitle.text = SettingConstants.myName
            self.labelNote.text = SettingConstants.nameVisibleInYourFollower
        }
        self.labelErrorMessage?.text = Validation.wrongUserName
    }
    /**
     For Call Api
     */
    func callApi() {
        
    }
}

// MARK:- Button Actions -
extension UpdateNameViewController {
    
    @IBAction func buttonTapRight(_ sender: DesignableButton) {
        
        switch self.updateType {
        case .username:
            if textFieldName.text?.count == 0 {
                self.labelErrorMessage?.text = Validation.enptyUserName
                self.labelErrorMessage?.isHidden = false
            } else {
                self.labelErrorMessage?.isHidden = true
                self.keyboardDismissAndHideError()
                self.callApi()
            }
        case .email:
            if textFieldName.text?.count == 0 {
                self.labelErrorMessage?.text = Validation.email
                self.labelErrorMessage?.isHidden = false
            } else if self.isValidEmail(self.textFieldName.text ?? "") == false {
                self.labelErrorMessage?.text = Validation.email
                self.labelErrorMessage?.isHidden = false
            } else if textFieldName.text == "Jason.sh.yang@outlook.com" {
                self.labelErrorMessage?.text = Validation.wrongEmail
                self.labelErrorMessage?.isHidden = false
            }
            else {
                self.labelErrorMessage?.isHidden = true
                self.keyboardDismissAndHideError()
                self.showCustomAlertAction(title: SettingConstants.sendVerificationEmail, buttonType: .yesButton, from: self, topImageName: "ic_mail_alert", buttonOneTitleText: GeneralConstants.iGotIt)
            }
        default:
            if textFieldName.text?.count == 0 {
                self.labelErrorMessage?.text = Validation.enptyName
                self.labelErrorMessage?.isHidden = false
            } else {
                self.labelErrorMessage?.isHidden = true
                self.keyboardDismissAndHideError()
                self.callApi()
            }
        }
    }
}

// MARK:- TextField Delegates -
extension UpdateNameViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.keyboardDismissAndHideError()
        if textField == textFieldName {
            let fullNoteText = SettingConstants.setEmailAddress + (self.textFieldName.text ?? "")
            self.labelNote.text = fullNoteText
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string.isEmpty {
            text = String(text.dropLast())
        } else {
            text = textField.text! + string
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {}
}

// MARK:- Assemble Registration
class UpdateNameViewControllerAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.storyboardInitCompleted(UpdateNameViewController.self) { (resolver, viewController) in
            
            let viewModel = resolver.resolve(UpdateNameViewModel.self)!
            viewController.configure(with: viewModel)
        }
    }
}
