//
//  UpdateNameViewModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//

import Foundation
import UIKit
import Swinject
import RxSwift
import MBProgressHUD

// MARK:- UpdateNameViewState -
struct UpdateNameViewState {
    
    var isLoading: Bool
    var status: ApiStatus?
    var apiDataModel: LoginModel?
    
    static func initial() -> UpdateNameViewState {
        return UpdateNameViewState(isLoading: false, status: nil)
    }
    
    static func loading() -> UpdateNameViewState {
        return UpdateNameViewState(isLoading: true, status: nil)
    }
}

// MARK:- Protocol -
protocol UpdateNameViewModel {
    func getLiveTvData(action: String, page: Int)
    func updateNameViewStateObservable() -> Observable<UpdateNameViewState>
}
// MARK:- UpdateNameViewModelImpl -
class UpdateNameViewModelImpl {
    
    // DisposeBag for RxSwift (i.e.: RxKeyboard)
    private var disposeBag = DisposeBag()
    
    // UpdateNameViewState Publisher
    private var updateNameViewStatePublisher = PublishSubject<UpdateNameViewState>()
    
    // NetworkModule Object
    private let networkManager: NetworkModuleDao
    
    /**
     Designated Initializer
     - parameter networkManager: Network Manager instance used to make the requests
     */
    init(with networkManager: NetworkModuleDao) {
        self.networkManager = networkManager
    }
}
// MARK:- UpdateNameViewModelImpl Extension -
extension UpdateNameViewModelImpl: UpdateNameViewModel {
    /**
     View State Observable
     */
    func updateNameViewStateObservable() -> Observable<UpdateNameViewState> {
        return updateNameViewStatePublisher.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .startWith(.initial())
    }
    /**
     For Api Calling
     */
    func getLiveTvData(action: String, page: Int) {
        /*
        print("getLiveTvData param: action: \(action), page: \(page)")
        self.updateNameViewStatePublisher.onNext(UpdateNameViewState.loading())
        
        self.networkManager.getTimeLineList(action: action, page: page)
            .map { [unowned self] in
                self.mapResponse(apiDataModel: $0)
            }
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { (UpdateNameViewState) in
                self.updateNameViewStatePublisher.onNext(UpdateNameViewState)
            }, onError: { (error) in
                print(error)
                self.updateNameViewStatePublisher.onNext(
                    UpdateNameViewState(
                        isLoading: false, status: ApiStatus.error(error: error.localizedDescription)
                    )
                )
            }).disposed(by: self.disposeBag)
        */
    }
    /**
     For Mapping Api Response
     */
    private func mapResponse(apiDataModel: LoginModel) -> UpdateNameViewState {
        
        guard let success = apiDataModel.code, success == 200 || success == 1 else {
            return UpdateNameViewState(isLoading: false, status: .failure, apiDataModel: apiDataModel)
        }
        return UpdateNameViewState(isLoading: false, status: .success, apiDataModel: apiDataModel)
    }
}

// MARK:- Swinject Assembly -
class UpdateNameViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        
        //  NetworkModule
        container.register(UpdateNameViewModel.self) { resolver in
            let networkManager = resolver.resolve(NetworkModuleDao.self)!
            return UpdateNameViewModelImpl(with: networkManager)
        }
    }
}
