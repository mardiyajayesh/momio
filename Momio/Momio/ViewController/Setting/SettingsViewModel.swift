//
//  SettingsViewModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//

import Foundation
import UIKit
import Swinject
import RxSwift
import MBProgressHUD

// MARK:- SettingsViewState -
struct SettingsViewState {
    
    var isLoading: Bool
    var status: ApiStatus?
    var apiDataModel: LoginModel?
    
    static func initial() -> SettingsViewState {
        return SettingsViewState(isLoading: false, status: nil)
    }
    
    static func loading() -> SettingsViewState {
        return SettingsViewState(isLoading: true, status: nil)
    }
}

// MARK:- Protocol -
protocol SettingsViewModel {
    func getLiveTvData(action: String, page: Int)
    func settingsViewStateObservable() -> Observable<SettingsViewState>
}
// MARK:- SettingsViewModelImpl -
class SettingsViewModelImpl {
    
    // DisposeBag for RxSwift (i.e.: RxKeyboard)
    private var disposeBag = DisposeBag()
    
    // SettingsViewState Publisher
    private var settingsViewStatePublisher = PublishSubject<SettingsViewState>()
    
    // NetworkModule Object
    private let networkManager: NetworkModuleDao
    
    /**
     Designated Initializer
     - parameter networkManager: Network Manager instance used to make the requests
     */
    init(with networkManager: NetworkModuleDao) {
        self.networkManager = networkManager
    }
}
// MARK:- SettingsViewModelImpl Extension -
extension SettingsViewModelImpl: SettingsViewModel {
    /**
     View State Observable
     */
    func settingsViewStateObservable() -> Observable<SettingsViewState> {
        return settingsViewStatePublisher.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .startWith(.initial())
    }
    /**
     For Api Calling
     */
    func getLiveTvData(action: String, page: Int) {
        /*
        print("getLiveTvData param: action: \(action), page: \(page)")
        self.settingsViewStatePublisher.onNext(SettingsViewState.loading())
        
        self.networkManager.getTimeLineList(action: action, page: page)
            .map { [unowned self] in
                self.mapResponse(apiDataModel: $0)
            }
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { (SettingsViewState) in
                self.settingsViewStatePublisher.onNext(SettingsViewState)
            }, onError: { (error) in
                print(error)
                self.settingsViewStatePublisher.onNext(
                    SettingsViewState(
                        isLoading: false, status: ApiStatus.error(error: error.localizedDescription)
                    )
                )
            }).disposed(by: self.disposeBag)
        */
    }
    /**
     For Mapping Api Response
     */
    private func mapResponse(apiDataModel: LoginModel) -> SettingsViewState {
        
        guard let success = apiDataModel.code, success == 200 || success == 1 else {
            return SettingsViewState(isLoading: false, status: .failure, apiDataModel: apiDataModel)
        }
        return SettingsViewState(isLoading: false, status: .success, apiDataModel: apiDataModel)
    }
}

// MARK:- Swinject Assembly -
class SettingsViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        
        //  NetworkModule
        container.register(SettingsViewModel.self) { resolver in
            let networkManager = resolver.resolve(NetworkModuleDao.self)!
            return SettingsViewModelImpl(with: networkManager)
        }
    }
}
