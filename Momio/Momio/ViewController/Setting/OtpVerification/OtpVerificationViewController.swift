//
//  OtpVerificationViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 12/10/21.
//

import UIKit
import KAPinField
import Swinject
import RxSwift

class OtpVerificationViewController: BaseViewController {
    // MARK:- IBOutlets -
    @IBOutlet weak var labelTitle: DesignableLabel!
    @IBOutlet weak var labelNote: UILabel!
    @IBOutlet weak var textFieldOtp: KAPinField!
    
    @IBOutlet weak var buttonRight: DesignableButton!
    
    // MARK:- Properties -
    var targetCode = ""
    
    // ViewModel
    private var viewModel: OtpVerificationViewModel!
    // Bag
    private let bag = DisposeBag()
    
    // Swinject Initializer for the OtpVerificationViewController
    func configure(with ViewModel: OtpVerificationViewModel) {
        self.viewModel = ViewModel
    }
    
    // MARK:- View Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        //ViewModelConfig
        self.viewModel.otpVerificationViewStateObservable()
            .subscribe(onNext: { [weak self] viewState in
                self?.render(viewState: viewState)
            }, onError: { error in
            }).disposed(by: self.bag)
        //        self.viewModel.getLiveTvData(action: "get-timeline-post", page: 1)//, murchant_id: "", userType: ""
        self.setupUI()
    }
    
}

// MARK:- Custome Method -
extension OtpVerificationViewController {
    /**
     For Api Calling
     */
    func render(viewState: OtpVerificationViewState) {
        if viewState.isLoading {
            self.showProgressHud(text: "Loading...")
        } else {
            self.hideProgressHud()
        }
        guard let status = viewState.status else  { return }
        switch (status) {
        case .success:
            print("success")
            
        case .failure:
            print("failure")
            
        case .error( _):
            print("error")
            
        }
    }
    /**
     For UI Setup
     */
    func setupUI() {
        print("Target Code :+++=======+++++ \(self.targetCode)")
        // -- Appearance --
        self.updateStyle()
        // -- Properties --
        self.textFieldOtp.properties.delegate = self
        self.refreshPinField()
        
        // Get focus
        self.textFieldOtp.becomeFirstResponder()
    }
    /**
     For Refresh Otp TextField
     */
    func refreshPinField() {
        
        // Random numberOfCharacters
        self.textFieldOtp.text = ""
        //        self.textFieldOtp.properties.numberOfCharacters = [4,5].randomElement()!
        UIPasteboard.general.string = targetCode
        
        self.updateStyle()
    }
    /**
     For Upate Otp TextField Style
     */
    func updateStyle() {
        
        self.textFieldOtp.properties.isSecure = false // Secure pinField will hide actual input
        self.textFieldOtp.properties.animateFocus = false
        self.textFieldOtp.properties.isUppercased = false
        self.textFieldOtp.appearance.tokenColor = UIColor(named: "ThemButtonBGLightGrayColor")//UIColor.label.withAlphaComponent(0.2)
        self.textFieldOtp.appearance.tokenFocusColor = UIColor(named: "ThemButtonBGLightGrayColor")
        if #available(iOS 13.0, *) {
            self.textFieldOtp.appearance.textColor = UIColor.label
        } else {
            // Fallback on earlier versions
        }
        self.textFieldOtp.appearance.font = .courierBold(40)
        self.textFieldOtp.appearance.kerning = 24
        self.textFieldOtp.appearance.backOffset = 5
        self.textFieldOtp.appearance.backColor = UIColor.clear
        self.textFieldOtp.appearance.backCornerRadius = 4
        self.textFieldOtp.appearance.backFocusColor = UIColor.clear
        self.textFieldOtp.appearance.backActiveColor = UIColor.clear
        self.textFieldOtp.appearance.backRounded = false
    }
}

// MARK:- Button Action -
extension OtpVerificationViewController {
    @IBAction func buttonTapRight(_ sender: DesignableButton) {
        
        let destination = UIStoryboard(name: "UpdatePasswordTypePasswordViewController", bundle: nil)
        let initialViewController = destination.instantiateViewController(withIdentifier: "UpdatePasswordTypePasswordViewController") as! UpdatePasswordTypePasswordViewController
        //        initialViewController.email = self.labelEmailValue.text ?? ""
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
}

// MARK:- KAPinFieldDelegate -
extension OtpVerificationViewController: KAPinFieldDelegate {
    func pinField(_ field: KAPinField, didChangeTo string: String, isValid: Bool) {
        if isValid {
            print("Valid input: \(string) ")
        } else {
            print("Invalid input: \(string) ")
            self.textFieldOtp.animateFailure()
        }
        if string.count <= 4 {
            self.buttonRight.isUserInteractionEnabled = false
            self.buttonRight.backgroundColor = UIColor(named: "ThemButtonBGLightGrayColor")
        }
    }
    
    func pinField(_ field: KAPinField, didFinishWith code: String) {
        print("didFinishWith: \(code)")
        
        // Randomly show success or failure
        if code != targetCode {
            print("Failure")
            field.animateFailure()
            self.buttonRight.isUserInteractionEnabled = false
            self.buttonRight.backgroundColor = UIColor(named: "ThemButtonBGLightGrayColor")
            self.labelErrorMessage?.text = Validation.wrongOtp
            self.labelErrorMessage?.isHidden = false
        } else {
            print("Success")
            //            field.animateSuccess(with: "") {
            self.buttonRight.isUserInteractionEnabled = true
            self.buttonRight.backgroundColor = UIColor(named: "ThemButtonBGColor")
            self.labelErrorMessage?.isHidden = true
            //            }
        }
    }
}

// MARK:- Assemble Registration
class OtpVerificationViewControllerAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.storyboardInitCompleted(OtpVerificationViewController.self) { (resolver, viewController) in
            
            let viewModel = resolver.resolve(OtpVerificationViewModel.self)!
            viewController.configure(with: viewModel)
        }
    }
}


