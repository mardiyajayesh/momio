//
//  OtpVerificationViewModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//

import Foundation
import UIKit
import Swinject
import RxSwift
import MBProgressHUD

// MARK:- OtpVerificationViewState -
struct OtpVerificationViewState {
    
    var isLoading: Bool
    var status: ApiStatus?
    var apiDataModel: LoginModel?
    
    static func initial() -> OtpVerificationViewState {
        return OtpVerificationViewState(isLoading: false, status: nil)
    }
    
    static func loading() -> OtpVerificationViewState {
        return OtpVerificationViewState(isLoading: true, status: nil)
    }
}

// MARK:- Protocol -
protocol OtpVerificationViewModel {
    func getLiveTvData(action: String, page: Int)
    func otpVerificationViewStateObservable() -> Observable<OtpVerificationViewState>
}
// MARK:- OtpVerificationViewModelImpl -
class OtpVerificationViewModelImpl {
    
    // DisposeBag for RxSwift (i.e.: RxKeyboard)
    private var disposeBag = DisposeBag()
    
    // OtpVerificationViewState Publisher
    private var otpVerificationViewStatePublisher = PublishSubject<OtpVerificationViewState>()
    
    // NetworkModule Object
    private let networkManager: NetworkModuleDao
    
    /**
     Designated Initializer
     - parameter networkManager: Network Manager instance used to make the requests
     */
    init(with networkManager: NetworkModuleDao) {
        self.networkManager = networkManager
    }
}
// MARK:- OtpVerificationViewModelImpl Extension -
extension OtpVerificationViewModelImpl: OtpVerificationViewModel {
    /**
     View State Observable
     */
    func otpVerificationViewStateObservable() -> Observable<OtpVerificationViewState> {
        return otpVerificationViewStatePublisher.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .startWith(.initial())
    }
    /**
     For Api Calling
     */
    func getLiveTvData(action: String, page: Int) {
        /*
        print("getLiveTvData param: action: \(action), page: \(page)")
        self.otpVerificationViewStatePublisher.onNext(OtpVerificationViewState.loading())
        
        self.networkManager.getTimeLineList(action: action, page: page)
            .map { [unowned self] in
                self.mapResponse(apiDataModel: $0)
            }
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { (OtpVerificationViewState) in
                self.otpVerificationViewStatePublisher.onNext(OtpVerificationViewState)
            }, onError: { (error) in
                print(error)
                self.otpVerificationViewStatePublisher.onNext(
                    OtpVerificationViewState(
                        isLoading: false, status: ApiStatus.error(error: error.localizedDescription)
                    )
                )
            }).disposed(by: self.disposeBag)
        */
    }
    /**
     For Mapping Api Response
     */
    private func mapResponse(apiDataModel: LoginModel) -> OtpVerificationViewState {
        
        guard let success = apiDataModel.code, success == 200 || success == 1 else {
            return OtpVerificationViewState(isLoading: false, status: .failure, apiDataModel: apiDataModel)
        }
        return OtpVerificationViewState(isLoading: false, status: .success, apiDataModel: apiDataModel)
    }
}

// MARK:- Swinject Assembly -
class OtpVerificationViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        
        //  NetworkModule
        container.register(OtpVerificationViewModel.self) { resolver in
            let networkManager = resolver.resolve(NetworkModuleDao.self)!
            return OtpVerificationViewModelImpl(with: networkManager)
        }
    }
}
