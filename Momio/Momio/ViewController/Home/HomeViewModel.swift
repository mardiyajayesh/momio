//
//  HomeViewModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//

import Foundation
import UIKit
import Swinject
import RxSwift
import MBProgressHUD

// MARK:- HomeViewState -
struct HomeViewState {
    
    var isLoading: Bool
    var status: ApiStatus?
    var apiDataModel: LoginModel?
    
    static func initial() -> HomeViewState {
        return HomeViewState(isLoading: false, status: nil)
    }
    
    static func loading() -> HomeViewState {
        return HomeViewState(isLoading: true, status: nil)
    }
}

// MARK:- Protocol -
protocol HomeViewModel {
    func getLiveTvData(action: String, page: Int)
    func homeViewStateObservable() -> Observable<HomeViewState>
}
// MARK:- HomeViewModelImpl -
class HomeViewModelImpl {
    
    // DisposeBag for RxSwift (i.e.: RxKeyboard)
    private var disposeBag = DisposeBag()
    
    // HomeViewState Publisher
    private var homeViewStatePublisher = PublishSubject<HomeViewState>()
    
    // NetworkModule Object
    private let networkManager: NetworkModuleDao
    
    /**
     Designated Initializer
     - parameter networkManager: Network Manager instance used to make the requests
     */
    init(with networkManager: NetworkModuleDao) {
        self.networkManager = networkManager
    }
}
// MARK:- HomeViewModelImpl Extension -
extension HomeViewModelImpl: HomeViewModel {
    /**
     View  State Observable
     */
    func homeViewStateObservable() -> Observable<HomeViewState> {
        return homeViewStatePublisher.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .startWith(.initial())
    }
    /**
     For Api Calling
     */
    func getLiveTvData(action: String, page: Int) {
        /*
        print("getLiveTvData param: action: \(action), page: \(page)")
        self.homeViewStatePublisher.onNext(HomeViewState.loading())
        
        self.networkManager.getTimeLineList(action: action, page: page)
            .map { [unowned self] in
                self.mapResponse(apiDataModel: $0)
            }
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { (HomeViewState) in
                self.homeViewStatePublisher.onNext(HomeViewState)
            }, onError: { (error) in
                print(error)
                self.homeViewStatePublisher.onNext(
                    HomeViewState(
                        isLoading: false, status: ApiStatus.error(error: error.localizedDescription)
                    )
                )
            }).disposed(by: self.disposeBag)*/
    }
    /**
     For Mapping Api Response
     */
    private func mapResponse(apiDataModel: LoginModel) -> HomeViewState {
        
        guard let success = apiDataModel.code, success == 200 || success == 1 else {
            return HomeViewState(isLoading: false, status: .failure, apiDataModel: apiDataModel)
        }
        return HomeViewState(isLoading: false, status: .success, apiDataModel: apiDataModel)
    }
}

// MARK:- Swinject Assembly -
class HomeViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        
        //  NetworkModule
        container.register(HomeViewModel.self) { resolver in
            let networkManager = resolver.resolve(NetworkModuleDao.self)!
            return HomeViewModelImpl(with: networkManager)
        }
    }
}
