//
//  HomeViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 30/09/21.
//

import UIKit
import FBSDKLoginKit
import Swinject
import RxSwift

class HomeViewController: BaseViewController {
    
    // MARK:- IBOutlets -
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelByTapping: UILabel!
    @IBOutlet weak var buttonFacebookLogin: DesignableButton!
    @IBOutlet weak var buttonAppleLogin: DesignableButton!
    @IBOutlet weak var buttonStart: DesignableButton!
    
    // ViewModel
    private var viewModel: HomeViewModel!
    
    // Bag
    private let bag = DisposeBag()
    
    // Swinject Initializer for the HomeViewController
    func configure(with ViewModel: HomeViewModel) {
        self.viewModel = ViewModel
    }
    
    // MARK:- View Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        //ViewModelConfig
        self.viewModel.homeViewStateObservable()
            .subscribe(onNext: { [weak self] viewState in
                self?.render(viewState: viewState)
            }, onError: { error in
            }).disposed(by: self.bag)
        //        self.viewModel.getLiveTvData(action: "get-timeline-post", page: 1)//, murchant_id: "", userType: ""
        self.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
}

// MARK:- Custom Methods -
private extension HomeViewController {
    
    /**
     For Get Api Response
     */
    func render(viewState: HomeViewState) {
        if viewState.isLoading {
            self.showProgressHud(text: "Loading...")
        } else {
            self.hideProgressHud()
        }
        guard let status = viewState.status else  { return }
        switch (status) {
        case .success:
            print("success")
            
        case .failure:
            print("failure")
            
        case .error( _):
            print("error")
        }
    }
    
    /**
     Fro UI Setup
     */
    func setupUI() {
        
        self.labelByTapping.text = HomeConstants.byTappingOn
        let string              = HomeConstants.byTappingOn//self.labelByTapping.text
        
        let range               = (string as NSString).range(of: "privacy policy")
        let range1               = (string as NSString).range(of: "Terms of Service")
        let attributedString    = NSMutableAttributedString(string: string )
        
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSNumber(value: 1), range: range)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSNumber(value: 1), range: range1)
        
        self.labelByTapping.attributedText = attributedString
        self.labelByTapping.isUserInteractionEnabled = true
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(tapLabel(_:)))
        self.labelByTapping.addGestureRecognizer(tapgesture)
    }
    
    /**
     For tap On Attribute Label
     */
    @objc func tapLabel(_ gesture: UITapGestureRecognizer) {
        guard let text = self.labelByTapping.text else { return }
        let privacyPolicyRange = (text as NSString).range(of: "privacy policy")
        let termsAndConditionRange = (text as NSString).range(of: "Terms of Service")
        
        if gesture.didTapAttributedTextInLabel(label: self.labelByTapping ?? UILabel(), inRange: privacyPolicyRange) {
            
        } else if gesture.didTapAttributedTextInLabel(label: self.labelByTapping ?? UILabel(), inRange: termsAndConditionRange) {
            
        }
    }
}

// MARK:- Button Actions -
extension HomeViewController {
    
    /**
     For Login With Facebook
     */
    @IBAction func buttonTapFacebookLogin(_ sender: DesignableButton) {
        FacebookLoginManager.shared.login(vc: self) { (profile) in
            print("FacebookLogin profile: \(String(describing: profile.name)), \(String(describing: profile.email))")
        } onFailure: { (error) in
            print("FacebookLogin error: \(String(describing: error.localizedDescription))")
        }
    }
    
    /**
     For Login With Apple
     */
    @IBAction func buttonTapAppleLogin(_ sender: DesignableButton) {
        
        if #available(iOS 13.0, *) {
            AppleLoginManager.shared.signIn(controller: self) { (profile) in
                print("AppleLogin profile: \(String(describing: profile.name)), \(String(describing: profile.email))")
            } onFailure: { (error) in
                print("AppleLogin error: \(String(describing: error.localizedDescription))")
            }
        }
    }
    
    /**
     For Star Login
     */
    @IBAction func buttonTapStartLogin(_ sender: DesignableButton) {
        let destination = UIStoryboard(name: "LoginEmailViewController", bundle: nil)
        let initialViewController = destination.instantiateViewController(withIdentifier: "LoginEmailViewController") as! LoginEmailViewController
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    
    /**
     For Open Setting
     */
    @IBAction func buttonTapSetting(_ sender: DesignableButton) {
        let destination = UIStoryboard(name: "SettingsViewController", bundle: nil)
        let initialViewController = destination.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
}

// MARK:- UITapGestureRecognizer Method -
extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(
            x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
            y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y
        )
        let locationOfTouchInTextContainer = CGPoint(
            x: locationOfTouchInLabel.x - textContainerOffset.x,
            y: locationOfTouchInLabel.y - textContainerOffset.y
        )
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
}
// MARK:- Assemble Registration
class HomeViewControllerAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.storyboardInitCompleted(HomeViewController.self) { (resolver, viewController) in
            
            let viewModel = resolver.resolve(HomeViewModel.self)!
            viewController.configure(with: viewModel)
        }
    }
}
