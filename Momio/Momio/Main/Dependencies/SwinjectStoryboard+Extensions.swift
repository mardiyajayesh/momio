//
//  SwinjectStoryboard+Extensions.swift
//  VoW
//
//  Created by Niraj Gambhava on 16/05/19.
//  Copyright © 2019 Niraj Gambhava. All rights reserved.
//

import UIKit
import Swinject
import SwinjectStoryboard

// This enables injection of the initial view controller from the app's main
//   storyboard project settings. So, this is the starting point of the
//   dependency tree.
extension SwinjectStoryboard {
    
    public class func setup() {
        
        if AppDelegate.dependencyRegistry == nil {
            AppDelegate.dependencyRegistry = DependencyRegistryImpl(container: defaultContainer)
        }
    }
}
