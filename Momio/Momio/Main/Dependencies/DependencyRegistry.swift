//
//  DependencyRegistry.swift
//  VoW
//
//  Created by Niraj Gambhava on 16/05/19.
//  Copyright © 2019 Niraj Gambhava. All rights reserved.
//

import UIKit
import Swinject
import SwinjectStoryboard

protocol DependencyRegistry {}

class DependencyRegistryImpl: DependencyRegistry {
    
    private var container: Container
    private let assembler: Assembler!
    
    init(container: Container) {
        
        Container.loggingFunction = nil
        
        self.container = container
        self.assembler = Assembler(container: self.container)
        
        self.assembleDepenencies()
        self.assebleUI()
    }
}

// MARK:- Registrations
private extension DependencyRegistryImpl {
    
    func assembleDepenencies() {
        self.assembler.apply(assembly: NetworkModuleDaoAssembly())
    }
}

private extension DependencyRegistryImpl {
    
    func assebleUI() {
        
        self.assembler.apply(assembly: LoginEmailViewModelAssembly())
        self.assembler.apply(assembly: LoginEmailViewControllerAssembly())
        
        self.assembler.apply(assembly: LoginPasswordViewModelAssembly())
        self.assembler.apply(assembly: LoginPasswordViewControllerAssembly())
        
        self.assembler.apply(assembly: SignUpNameViewModelAssembly())
        self.assembler.apply(assembly: SignUpNameViewControllerAssembly())
        
        self.assembler.apply(assembly: SignUpPasswordViewModelAssembly())
        self.assembler.apply(assembly: SignUpPasswordViewControllerAssembly())
        
        self.assembler.apply(assembly: SignUpBirthdayViewModelAssembly())
        self.assembler.apply(assembly: SignUpBirthdayViewControllerAssembly())
        
        self.assembler.apply(assembly: SignUpProfilePicViewModelAssembly())
        self.assembler.apply(assembly: SignUpProfilePicViewControllerAssembly())
        
        self.assembler.apply(assembly: SignUpWelcomeViewModelAssembly())
        self.assembler.apply(assembly: SignUpWelcomeViewControllerAssembly())
        
        self.assembler.apply(assembly: HomeViewModelAssembly())
        self.assembler.apply(assembly: HomeViewControllerAssembly())
        
        self.assembler.apply(assembly: HighLightListViewModelAssembly())
        self.assembler.apply(assembly: HighLightListViewControllerAssembly())
        
        self.assembler.apply(assembly: UpdateNameViewModelAssembly())
        self.assembler.apply(assembly: UpdateNameViewControllerAssembly())
        
        self.assembler.apply(assembly: UpdatePasswordTypePasswordViewModelAssembly())
        self.assembler.apply(assembly: UpdatePasswordTypePasswordViewControllerAssembly())
        
        self.assembler.apply(assembly: NotificationSettingViewModelAssembly())
        self.assembler.apply(assembly: NotificationSettingViewControllerAssembly())
        
        self.assembler.apply(assembly: UpdatePasswordViewModelAssembly())
        self.assembler.apply(assembly: UpdatePasswordViewControllerAssembly())
        
        self.assembler.apply(assembly: ChangeLanguageViewModelAssembly())
        self.assembler.apply(assembly: ChangeLanguageViewControllerAssembly())
        
        self.assembler.apply(assembly: WebViewModelAssembly())
        self.assembler.apply(assembly: WebViewControllerAssembly())
        
        self.assembler.apply(assembly: SupportViewModelAssembly())
        self.assembler.apply(assembly: SupportViewControllerAssembly())
        
        self.assembler.apply(assembly: OtpVerificationViewModelAssembly())
        self.assembler.apply(assembly: OtpVerificationViewControllerAssembly())
        
        self.assembler.apply(assembly: DeleteAccountViewModelAssembly())
        self.assembler.apply(assembly: DeleteAccountViewControllerAssembly())
        
        self.assembler.apply(assembly: LegalViewModelAssembly())
        self.assembler.apply(assembly: LegalViewControllerAssembly())
        
        self.assembler.apply(assembly: UpdateProfilePicViewModelAssembly())
        self.assembler.apply(assembly: UpdateProfilePicViewControllerAssembly())
        
        self.assembler.apply(assembly: SettingsViewModelAssembly())
        self.assembler.apply(assembly: SettingsViewControllerAssembly())
        
        self.assembler.apply(assembly: FriendListViewModelAssembly())
        self.assembler.apply(assembly: FriendListViewControllerAssembly())
        
        self.assembler.apply(assembly: SearchFriendViewModelAssembly())
        self.assembler.apply(assembly: SearchFriendViewControllerAssembly())
        
    }
}

// MARK:- Makers
extension DependencyRegistryImpl {
    
    func makeHomeViewController() -> HomeViewController {
        return container.resolve(HomeViewController.self)!
    }
}
