//
//  CustomeAlertView.swift
//  Momio
//
//  Created by Jayesh Mardiya on 01/10/21.
//

import UIKit

protocol CustomeAlertViewDelegate {
    func didTapOnOk(action: String)
    func didTapOnCancel(action: String)
    //    func didTapOnClose(action: String)
}

class CustomeAlertView: UIView {
    
    // MARK:- Variables
    static let instance = CustomeAlertView()
    var delegate: CustomeAlertViewDelegate?
    
    @IBOutlet var parentView: UIView!
    @IBOutlet weak var baseView: UIView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    
    @IBOutlet weak var imageViewTop: UIImageView!
    
    // MARK:- LifeCycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("CustomeAlertView", owner: self, options: nil)
        commomInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    //    required init?(coder aDecoder: NSCoder) {
    //       super.init(coder: aDecoder)
    //    }
    
    private func commomInit() {
        
        // Parentview Properties
        parentView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        parentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        baseView.layer.cornerRadius = 10
    }
    
    enum CustomAlertType {
        case success
        case failure
    }
    
    // MARK:- Show Custom Alert with Actions
    
    func showCustomAlertWithActions(title: String?, message: String = "", actions: [CustomAlertAction], from vc: UIViewController, topImageName: String = "ic_mail_alert", buttonOneTitleColor: UIColor = UIColor.white, buttonOneBGColor: UIColor, buttonTwoTitleColor: UIColor = UIColor.white, buttonTwoBGColor: UIColor = UIColor(named: "ThemButtonBGBlueColor") ?? .blue, isCloseButtonHide: Bool = true) {//, buttonOneTitle: String , buttonTwoTitle: String = ""
        self.imageViewTop.image = UIImage(named: topImageName)
        self.lblTitle.text = title
        self.lblMessage.text = message
        self.btnOk.setTitle(actions.first?.title ?? "OK", for: .normal)
        self.btnOk.backgroundColor = buttonOneBGColor
        self.btnCancel.setTitle(actions.last?.title ?? "Cancel", for: .normal)
        self.btnCancel.backgroundColor = buttonTwoBGColor
        self.btnClose.isHidden = isCloseButtonHide
        self.delegate = vc as? CustomeAlertViewDelegate
        UIApplication.shared.keyWindow?.addSubview(parentView)
        self.baseView.layer.cornerRadius = 30
        self.baseView.layer.masksToBounds = true
    }
    
    @IBAction func didTapOnOkButton(_ sender: UIButton) {
        delegate?.didTapOnOk(action: sender.title(for: .normal) ?? "Ok")
        parentView.removeFromSuperview()
    }
    
    @IBAction func didTapOnCancelButton(_ sender: UIButton) {
        delegate?.didTapOnCancel(action: sender.title(for: .normal) ?? "Cancel")
        parentView.removeFromSuperview()
    }
    @IBAction func didTapOnCloseButton(_ sender: UIButton) {
        //        delegate?.didTapOnClose(action: sender.title(for: .normal) ?? "Close")
        parentView.removeFromSuperview()
    }
}
struct CustomAlertAction {
    var title: String?
    var style: UIAlertAction.Style
    
    init(title: String, style: UIAlertAction.Style = .default) {
        self.title = title
        self.style = style
    }
}

