//
//  MyQrCodePopUpViewController.swift
//  Momio
//
//  Created by Jayesh Mardiya on 28/10/21.
//

import UIKit

class MyQrCodePopUpViewController: BaseViewController {

    //MARK: - IBOutlets -
    @IBOutlet weak var butttonClose: DesignableButton!
    @IBOutlet weak var buttonDownload: DesignableButton!
    @IBOutlet weak var buttonShare: DesignableButton!
    
    @IBOutlet weak var imageViewQrCode: UIImageView!
    @IBOutlet weak var imageViewProfilePicture: DesignableImageView!
    
    @IBOutlet weak var labelTitle: DesignableLabel!
    @IBOutlet weak var labelDescription: DesignableLabel!
    
    //MARK: - View Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}

//MARK: - Custom Methods -
extension MyQrCodePopUpViewController {
    
}

//MARK: - Button Actions -
extension MyQrCodePopUpViewController {
    @IBAction func buttonTapClose(_ sender: DesignableButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonTapDownload(_ sender: DesignableButton) {
    }
    
    @IBAction func buttonTapShare(_ sender: DesignableButton) {

        self.openShareDocumentController(message: FriendList.findMyQrCode)
    }
}
