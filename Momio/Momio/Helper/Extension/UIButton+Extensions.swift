//
//  UIButton+Extensions.swift
//  Momio
//
//  Created by Jayesh Mardiya on 16/10/21.
//

import UIKit

extension UIButton {
    
    func setActive(_ isActive: Bool) {
        if isActive {
            self.enable()
        } else {
            self.deActive()
        }
    }
    
    
    func setEnabled(_ isEnabled: Bool) {
        if isEnabled {
            self.enable()
        } else {
            self.disable()
        }
    }
    
    private func enable() {
        self.isEnabled = true
        self.isUserInteractionEnabled = true
        self.tintColor = UIColor(named: "ThemButtonTextColor")
        self.backgroundColor = UIColor(named: "ThemButtonBGRedColor")
    }
    
    private func disable() {
        self.isEnabled = false
        self.isUserInteractionEnabled = false
        self.tintColor = UIColor(named: "ThemeTextBlueColor")
        self.backgroundColor = UIColor(named: "ThemBGColor")
    }
    
    private func deActive() {
        self.isEnabled = false
        self.isUserInteractionEnabled = false
        self.tintColor = UIColor.white
        self.backgroundColor = UIColor(named: "ThemButtonBGLightGrayColor")
    }
}
