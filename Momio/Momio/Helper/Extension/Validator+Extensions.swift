//
//  Validator+Extensions.swift
//  Momio
//
//  Created by Jayesh Mardiya on 16/10/21.
//

import Foundation

final class EmailValidator {
    
    func validate(_ input: String) -> Bool {
        guard
            let regex = try? NSRegularExpression(
                pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}",
                options: [.caseInsensitive]
            )
        else {
            assertionFailure("Regex not valid")
            return false
        }
        
        let regexFirstMatch = regex
            .firstMatch(
                in: input,
                options: [],
                range: NSRange(location: 0, length: input.count)
            )
        
        return regexFirstMatch != nil
    }
}

final class PasswordValidator {
    
    func validate(password: String) -> Bool {
        
        guard
            let regex = try? NSRegularExpression(
                pattern: "(?=.*[$@$!%*?&#])[A-Za-z\\d$@$!%*?&#]{6,}"
            )
        else {
            assertionFailure("Regex not valid")
            return false
        }
        
        let regexFirstMatch = regex
            .firstMatch(
                in: password,
                options: [],
                range: NSRange(location: 0, length: password.count)
            )
        
        return regexFirstMatch != nil
    }
}
