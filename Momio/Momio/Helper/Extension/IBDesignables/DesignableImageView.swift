//
//  DesignableImageView.swift
// Momio
//
//  Created by Jayesh Mardiya on 06/09/20.
//  Copyright © 2020 Jayesh Mardiya. All rights reserved.
//

import UIKit
//@IBDesignable
open class DesignableImageView: UIImageView, Designable, DesignableShadow, DesignableFitImage {
    
    open var cornerMask:CAShapeLayer?
    @IBInspectable open var cornerRadius: CGFloat {
        get {
            return self.getCornerRadius()
        }
        set {
            self.setCornerRadius(newValue)
            self.layer.masksToBounds = true
        }
    }
    @IBInspectable open var borderWidth: CGFloat {
        get {
            return self.getBorderWidth()
        }
        set {
            self.setBorderWidth(newValue)
        }
    }
    @IBInspectable open var borderColor: UIColor? {
        get {
            return self.getBorderColor()
        }
        set {
            self.setBorderColor(newValue)
        }
    }
    open var shadowColor: UIColor? {
        didSet {
            self.setShadowColor(shadowColor)
        }
    }
    @IBInspectable open var shadowOffset: CGSize {
        get {
            return self.getShadowOffset()
        }
        set {
            self.setShadowOffset(newValue)
        }
    }
    @IBInspectable open var shadowOpacity: CGFloat {
        get {
            return self.getShadowOpacity()
        }
        set {
            self.setShadowOpacity(newValue)
        }
    }
    @IBInspectable open var shadowBlur: CGFloat {
        get {
            return self.getShadowBlur()
        }
        set {
            self.setShadowBlur(newValue)
        }
    }
    @IBInspectable open var shadowSpread: CGFloat = 0{
        didSet {
            self.updateShadowPath()
        }
    }
    override open var bounds: CGRect {
        didSet {
            self.updateShadowPath()
        }
    }
    
    var ratioConstraintPrority:Float = 250
    @IBInspectable open var imageRatioConstraintPrority: Float {
        get {
            return ratioConstraintPrority
        }
        set {
            ratioConstraintPrority = newValue
        }
    }
    var ratioConstraint: NSLayoutConstraint?
    override open var image: UIImage? {
        didSet {
            if let image = image {
                self.computeImageRatioConstraint(image)
                
            }
            else {
                self.removeImageRatioConstraint()
            }
            
        }
    }
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    open func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        self.doRoundCorners(corners: corners, radius: radius)
    }
}
