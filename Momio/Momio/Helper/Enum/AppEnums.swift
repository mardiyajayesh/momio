//
//  Enums.swift
//  Momio
//
//  Created by Jayesh Mardiya on 13/10/21.
//

import Foundation

enum AlertButtonType: String {
    case yesButton
    case noButton
    case bothButton
}

enum UpdateType {
    case name
    case username
    case email
}

enum SelectedLanguage: String {
    case english = "en"
    case japanese = "ja"
    case korean = "kr"
    case latin = "la"
    case chinese = "ch"
    case thai = "th"
}
