//
//  Constent.swift
//  Momio
//
//  Created by Jayesh Mardiya on 30/09/21.
//

import Foundation

// MARK:- homeCOnstant
struct HomeConstants {
    
    static let byTappingOn = "By tapping on ‘Sign up’, you agree to the privacy policy and Terms of Service"
    static let pleaseSetPassword = "Ouch! Please set password at least 6-digit include special symbol. For example, abcd1%"
}

// MARK:- generalConstant
struct GeneralConstants {
    static let appName = "Momio"
    static let error = "Error"
    static let ok = "Ok"
    static let cancel = "Cancel"
    static let goToSupport = "Go to Support center"
    static let sendEmail = "Send email"
    static let notSet = "No Set"
    static let iGotIt = "I got it!"
    static let okPleaseSend = "Ok,Please send"
    static let yesDelete = "Yes, Delete"
    static let logOut = "Log out"
    static let following = "Following"
    static let unFollow = "Unfollow"
}

// MARK:- settingConstant
struct SettingConstants {
    static let myName = "MY NAME"
    static let nameVisibleInYourFollower = "🧐Name visible to your followers"
    static let myUserName = "MY USER NAME"
    static let userNameThatOnlyYou = "😆username that only you have in momio world"
    static let myEmail = "MY EMAIL"
    static let setEmailAddress = "Set email address: "
    static let sendVerificationEmail = "Momio sent verification email to your new email address. Please check and confirm"
    static let password = "PASSWORD"
    static let weWillSendVerificationCodeToYourEmail = "😊We will send Verification code to your email to proceed reset password."
    static let pleaseInputNewPassword = "Please input new password"
    static let myBirthDay = "MY BIRTHDAY"
    static let language = "LANGUAGE"
    static let languageNote = String(format: "You can change %@ application language here. We’re working on adding more languages quickly!", GeneralConstants.appName.lowercased())
    static let english = "English"
    static let japanese = "Japanese 日本語"
    static let korean = "Korean 한국어"
    static let latin = "Latin"
    static let chinese = "Chinese"
    static let thai = "Thai"
    static let pushNotification = "PUSH NOTIFICATION"
    static let legal = "LEGAL"
    static let medInJapan = "Made in Japan - Tokyo 2021"
    static let copyright = "copyright © TOUCH Corporation All Rights Reserved."
    static let termOfService = "Terms of service"
    static let privecyPolicy = "Privacy Policy"
    static let legalNotice = "Legal notice"
    static let support = "SUPPORT"
    static let deleteAccount = "DELETE ACCOUNT"
    static let iAmFeelingHappy = "I’m feeling HAPPY!"
}

// MARK:- validation
struct Validation {
    static let email = "Please enter valid email"
    static let accountInvalid = "Your account has problem now. Please contact to momio Support center"
    static let forgotPassword = "We will send link to your email to reset password."
    static let forgotPasswordMailSend = "Sent! Please check your email ro reset password"
    static let noCamera = "You don't have camera"
    static let emptyFeeling = "Please enter Feeling text"
    static let emptyProfileImage = "Please select Profile pic"
    static let enptyName = "Please enter name"
    static let enptyUserName = "Please enter username"
    static let wrongUserName = "Ouch! Already taken by other mom user"
    static let wrongEmail = "Ouch! Already used email address"
    static let wrongOtp = "Ouch! Wrong code"
    static let passwordValidation = "Ouch! Please set password at least 6-digit include special symbol. For example, abcd1%"
    static let pleaseSetPasswordAtLeast = "Ouch! Please set password at least 6-digit include special symbol."
    static let passwordForExample = " For example"
    static let commaSpace = ", "
    static let passwordFormate = "abcd1%"
    static let wrongPassword = "Ehh, Something wrong!"
    static let resetMyFeelingHistory = "Are you sure delete all your feeling history? You and all your friends can not see your feeling history anymore."
    static let logOut = "We are waiting you to come back!"
    static let emptyProfilePic = "Please select profile pic"
}

// MARK:- loginRagister
struct LoginRagister {
    static let inputYourEmail = "HI! COULD YOU INPIUT YOUR EMAIL?"
}
struct FriendList {
    static let findAndConnect = "FIND AND CONNECT"
    static let youMyInterrest = "YOU MAY INTEREST"
    static let inviteFriendHere = "INVITE FRIEND HERE"
    static let searchByName = "SEARCH BY NAME"
    static let findMyQrCode = "Please find my QR Code"
}

// MARK:- userDefaultKeyName
struct UserDefaultKeysName {
    static let language = "language"
    static let isLogin = "isLogin"
    static let token = "token"
}
