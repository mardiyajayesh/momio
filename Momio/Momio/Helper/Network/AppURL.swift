//
//  AppURL.swift
//  Momio
//
//  Created by Jayesh Mardiya on 30/09/21.
//

import UIKit

/// BaseURL
var baseURL: String {
    return "https://www.crownid.app/api/v2/"
}
