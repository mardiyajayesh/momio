//
//  NetworkModule.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//
//

import Foundation
import RxSwift
import Moya

class NetworkModule {
    
    /// Singleton
    internal static let shared: NetworkModule = NetworkModule()
    
    /// Hide init()
    private init() {}
    
    /// RxSwift
    private var disposeBag = DisposeBag()
    
    /// Vox Connect Provider
    private let kavingsProvider: MoyaProvider<MomioProvider>
        = MoyaProvider<MomioProvider>(plugins: [StatusBarActivityIndicatorPlugin()])
    
    private lazy var unknownError: NSError = {
        let userInfo = [NSLocalizedDescriptionKey: "networkModule_error_unknown",
                        NSLocalizedFailureReasonErrorKey: "networkModule_error_unknown",
                        NSLocalizedRecoverySuggestionErrorKey: "Please check your credentials and try again"]
        return NSError(domain: "com.MomioProvider", code: 42, userInfo: userInfo)
    }()
}

extension NetworkModule {
    
    func cancelRequest() {
        disposeBag = DisposeBag()
    }
}
