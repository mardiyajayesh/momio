//
//  NetworkModuleDao.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//
//

import UIKit
import RxSwift
import Moya
import Swinject

class NetworkModuleDaoAssembly: Assembly {
    
    func assemble(container: Container) {
        
        // NetworkManager
        container.register(NetworkModuleDao.self) { _ in
            NetworkModuleDaoImpl()
        }.inObjectScope(.container)
    }
}

protocol NetworkModuleDao {
    
    func login(email: String, password: String)  -> Single<LoginModel>
    func uploadProfilePicture(file: String, image: UIImage)  -> Single<FileUploadModel>
    func checkEmail(email: String) -> Single<CheckEmailModel>
    func register(email: String, password: String, name: String, dob: String, profilePic: String, feeling: String) -> Single<RegisterModel>
    func getPostListData(limit: Int, page: Int) -> Single<UsersPostListModel>
}

class NetworkModuleDaoImpl {
    
    /// RxSwift
    private var disposeBag = DisposeBag()
    
    /// Vox Connect Provider
    private var MomioProvider: MoyaProvider<MomioProvider> = MoyaProvider<MomioProvider>(plugins: [StatusBarActivityIndicatorPlugin()])
    
    /// Initialiser
    init() {}
}

extension NetworkModuleDaoImpl: NetworkModuleDao {
    
    // Get TimeLineList
    func login(email: String, password: String)  -> Single<LoginModel> {
        return MomioProvider.rx.request(.login(email: email, password: password))
            .flatMap { response in
                if let headerFields = response.response?.allHeaderFields {
                    if let authorization = headerFields["Authorization"] as? String {
                        if authorization != "" {
                            UserDefaults.standard.set(authorization, forKey: "authorization")
                            UserDefaults.standard.synchronize()
                        }
                    }
                }
                
                print(try? response.mapJSON())
                guard (200...299).contains(response.statusCode) else {
                    throw MoyaError.statusCode(response)
                }
                
                return .just(response)
            }
            .map(LoginModel.self)
    }
    
    func uploadProfilePicture(file: String, image: UIImage)  -> Single<FileUploadModel> {
        return MomioProvider.rx.request(.uploadProfilePicture(file: file, image: image))
            .flatMap { response in
                if let headerFields = response.response?.allHeaderFields {
                    if let authorization = headerFields["Authorization"] as? String {
                        if authorization != "" {
                            UserDefaults.standard.set(authorization, forKey: "authorization")
                            UserDefaults.standard.synchronize()
                        }
                    }
                }
                
                print(try? response.mapJSON())
                guard (200...299).contains(response.statusCode) else {
                    throw MoyaError.statusCode(response)
                }
                
                return .just(response)
            }
            .map(FileUploadModel.self)
    }
    
    func checkEmail(email: String) -> Single<CheckEmailModel> {
        return MomioProvider.rx.request(.checkEmail(email: email))
            .flatMap { response in
                if let headerFields = response.response?.allHeaderFields {
                    if let authorization = headerFields["Authorization"] as? String {
                        if authorization != "" {
                            UserDefaults.standard.set(authorization, forKey: "authorization")
                            UserDefaults.standard.synchronize()
                        }
                    }
                }
                
                print(try? response.mapJSON())
                guard (200...500).contains(response.statusCode) else {
                    throw MoyaError.statusCode(response)
                }
                
                return .just(response)
            }
            .map(CheckEmailModel.self)
    }
    
    func register(email: String, password: String, name: String, dob: String, profilePic: String, feeling: String) -> Single<RegisterModel> {
        return MomioProvider.rx.request(.register(email: email, password: password, name: name, dob: dob, profilePic: profilePic, feeling: feeling))
            .flatMap { response in
                if let headerFields = response.response?.allHeaderFields {
                    if let authorization = headerFields["Authorization"] as? String {
                        if authorization != "" {
                            UserDefaults.standard.set(authorization, forKey: "authorization")
                            UserDefaults.standard.synchronize()
                        }
                    }
                }
                
                print(try? response.mapJSON())
                guard (200...299).contains(response.statusCode) else {
                    throw MoyaError.statusCode(response)
                }
                
                return .just(response)
            }
            .map(RegisterModel.self)
    }
    
    func getPostListData(limit: Int, page: Int) -> Single<UsersPostListModel> {
        return MomioProvider.rx.request(.getPosts(limit: limit, page: page))
            .flatMap { response in
                if let headerFields = response.response?.allHeaderFields {
                    if let authorization = headerFields["Authorization"] as? String {
                        if authorization != "" {
                            UserDefaults.standard.set(authorization, forKey: "authorization")
                            UserDefaults.standard.synchronize()
                        }
                    }
                }
                
                print(try? response.mapJSON())
                guard (200...299).contains(response.statusCode) else {
                    throw MoyaError.statusCode(response)
                }
                
                return .just(response)
            }
            .map(UsersPostListModel.self)
    }
}
