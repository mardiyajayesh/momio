//
//  ApiBody.swift
//  Momio
//
//  Created by Jayesh Mardiya on 30/09/21.
//  
//

import UIKit

/// APIBody
struct ApiBody {
    
    /// Lets
    private static let installationId = UIDevice.current.identifierForVendor!.uuidString
    private static let language: String = "en"
    
    /// Base
    internal static var base: [String: Any] {
        return [:]
    }
    
    // Login
    internal static func login(email: String, password: String) -> [String: Any] {
        var returnable = ApiBody.base
        returnable["email"] = email
        returnable["password"] = password
        return returnable
    }
    // check Email
    internal static func checkEmail(email: String) -> [String: Any] {
        var returnable = ApiBody.base
        returnable["email"] = email
        
        return returnable
    }
    // check Email
    internal static func register(email: String, password: String, name: String, dob: String, profilePic: String, feeling: String) -> [String: Any] {
        var returnable = ApiBody.base
        returnable["email"] = email
        returnable["password"] = password
        returnable["name"] = name
        returnable["dob"] = dob
        returnable["profilePic"] = profilePic
        returnable["feeling"] = feeling
        return returnable
    }
    
    // get Posts
    internal static func getPosts(limit: Int, page: Int) -> [String: Any] {
        var returnable = ApiBody.base
        returnable["limit"] = "10"
        returnable["page"] = "1"
        return returnable
    }
    
}
