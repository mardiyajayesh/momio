//
//  LoginModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//

import Foundation
struct LoginModel: Codable {
    let code : Int?
    let data : LoginModelData?
    let success : Bool?
    //Error
    let errorMessage : String?
    let error : ErrorData?

    enum CodingKeys: String, CodingKey {

        case code = "code"
        case data = "data"
        case success = "success"
        //Error
        case errorMessage = "errorMessage"
        case error = "error"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        data = try values.decodeIfPresent(LoginModelData.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        //Error
        errorMessage = try values.decodeIfPresent(String.self, forKey: .errorMessage)
        error = try values.decodeIfPresent(ErrorData.self, forKey: .error)
    }
}

struct ErrorData : Codable {

}


struct LoginModelData : Codable {
    let user : LoginModelUser?
    let token : String?

    enum CodingKeys: String, CodingKey {

        case user = "user"
        case token = "token"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user = try values.decodeIfPresent(LoginModelUser.self, forKey: .user)
        token = try values.decodeIfPresent(String.self, forKey: .token)
    }
}

struct LoginModelUser : Codable {
    let profilePic : String?
    let id : Int?
    let name : String?
    let email : String?
    let dob : String?
    let feeling : String?
    let isAdmin : Bool?
    let verifyToken : String?
    let isVerified : Bool?
    let createdAt : String?
    let updatedAt : String?

    enum CodingKeys: String, CodingKey {

        case profilePic = "profilePic"
        case id = "id"
        case name = "name"
        case email = "email"
        case dob = "dob"
        case feeling = "feeling"
        case isAdmin = "isAdmin"
        case verifyToken = "verifyToken"
        case isVerified = "isVerified"
        case createdAt = "createdAt"
        case updatedAt = "updatedAt"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        profilePic = try values.decodeIfPresent(String.self, forKey: .profilePic)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        dob = try values.decodeIfPresent(String.self, forKey: .dob)
        feeling = try values.decodeIfPresent(String.self, forKey: .feeling)
        isAdmin = try values.decodeIfPresent(Bool.self, forKey: .isAdmin)
        verifyToken = try values.decodeIfPresent(String.self, forKey: .verifyToken)
        isVerified = try values.decodeIfPresent(Bool.self, forKey: .isVerified)
        createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt)
        updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt)
    }

}
