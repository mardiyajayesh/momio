//
//  StatusBarActivityIndicatorPlugin.swift
//  Momio
//
//  Created by Jayesh Mardiya on 14/10/21.
//
//

import Foundation
import Moya

internal final class StatusBarActivityIndicatorPlugin: PluginType {
    
    /// This is executed before request starts
    func willSend(_ request: RequestType, target: TargetType) {
        
    }
}
