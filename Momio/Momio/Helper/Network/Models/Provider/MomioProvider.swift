//
//  MomioProvider.swift
//  Momio

//
//  Created by Jayesh Mardiya on 15/07/21.
//

import Foundation
import Moya

/// Endpoints
enum MomioProvider {
    case login(email: String, password: String)
    case uploadProfilePicture(file: String, image: UIImage)
    case checkEmail(email: String)
    case register(email: String, password: String, name: String, dob: String, profilePic: String, feeling: String)
    case getPosts(limit: Int, page: Int)
}

/// Extends TargetType
extension MomioProvider: TargetType {
    /// BaseURL
    var baseURL: URL {
        
        switch self {
        
        case .login, .uploadProfilePicture, .checkEmail, .register, .getPosts:// .login,
            let url = "https://api.momio.jp"
            
            guard let finalUrl = URL(string: url) else {
                Log.fatalError(message: "[MomioProvider]: URL is not valid.", event: .nullPointer)
            }
            return finalUrl
            
        }
    }
    
    /// Path
    var path: String {
        switch self {
        case .login:
            return "/pub/login"
        case .uploadProfilePicture:
            return "/pub/file-upload"
        case .checkEmail:
            return "/pub/check-email"
        case .register:
            return "/pub/register"
        case .getPosts:
            return "/api/posts"
        }
    }
    
    /// Method
    var method: Moya.Method {
        switch self {
        case .login, .uploadProfilePicture, .checkEmail, .register, .getPosts:
            return .post
            
        }
    }
    
    /// Task
    var task: Task {
        switch self {
        
        case .login(let email, let password):
            return .requestParameters(parameters: ApiBody.login(email: email, password: password), encoding: URLEncoding.httpBody)
        case .uploadProfilePicture(let file, let image):
            var formData = [MultipartFormData]()
            
            if let data = image.jpegData(compressionQuality: 0.5) {
                formData.append(MultipartFormData(provider: .data(data), name: file, fileName: "photo.jpg", mimeType:"image/jpeg"))
            }
            return .uploadMultipart(formData)
        //            return .requestParameters(parameters: ApiBody.login(file: file, image: image), encoding: URLEncoding.httpBody)
        case .checkEmail(let email):
            return .requestParameters(parameters: ApiBody.checkEmail(email: email), encoding: URLEncoding.httpBody)
        case .register(let email, let password, let name, let dob, let profilePic, let feeling):
            return .requestParameters(parameters: ApiBody.register(email: email, password: password, name: name, dob: dob, profilePic: profilePic, feeling: feeling), encoding: URLEncoding.httpBody)
        case .getPosts(let limit, let page):
            return .requestParameters(parameters: ApiBody.getPosts(limit: limit, page: page), encoding: URLEncoding.httpBody)
        }
    }
    
    /// SampleData: This is not necessary
    var sampleData: Data {
        return "{\"success\": false,\"action\": null,\"failure_reason\": \"wrong_credentials\"}".data(using: .utf8)!
    }
    
    /// Headers
    var headers: [String: String]? {
        switch self {
        case .login, .uploadProfilePicture, .checkEmail, .register:
            var dict = [String: String]()
            dict["Content-Type"] = "application/x-www-form-urlencoded"
            if let token = UserDefaults.standard.value(forKey: UserDefaultKeysName.token) {
                dict["x-token"] = token as? String
            }
            return dict
        case .getPosts:
            var dict = [String: String]()
            dict["Content-Type"] = "application/form-data"
            if let token = UserDefaults.standard.value(forKey: UserDefaultKeysName.token) {
                dict["x-token"] = token as? String
            }
            return dict
        }
    }
}
