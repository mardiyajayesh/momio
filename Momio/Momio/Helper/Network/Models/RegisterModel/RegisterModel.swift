//
//  RegisterModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 19/10/21.
//

import Foundation
struct RegisterModel : Codable {
    let code : Int?
    let data : RegisterModelData?
    let success : Bool?
    //Error
    let errorMessage : String?
    let error : RegisterModelError?

    enum CodingKeys: String, CodingKey {

        case code = "code"
        case data = "data"
        case success = "success"
        case errorMessage = "errorMessage"
        case error = "error"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        data = try values.decodeIfPresent(RegisterModelData.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        errorMessage = try values.decodeIfPresent(String.self, forKey: .errorMessage)
        error = try values.decodeIfPresent(RegisterModelError.self, forKey: .error)
    }
}

struct RegisterModelError : Codable {

}


struct RegisterModelData : Codable {
    let user : RegisterModelUser?

    enum CodingKeys: String, CodingKey {

        case user = "user"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user = try values.decodeIfPresent(RegisterModelUser.self, forKey: .user)
    }

}
struct RegisterModelUser : Codable {
    let profilePic : String?
    let isAdmin : Bool?
    let id : Int?
    let email : String?
    let name : String?
    let dob : String?
    let feeling : String?
    let password : String?
    let isVerified : Bool?
    let verifyToken : String?
    let updatedAt : String?
    let createdAt : String?

    enum CodingKeys: String, CodingKey {

        case profilePic = "profilePic"
        case isAdmin = "isAdmin"
        case id = "id"
        case email = "email"
        case name = "name"
        case dob = "dob"
        case feeling = "feeling"
        case password = "password"
        case isVerified = "isVerified"
        case verifyToken = "verifyToken"
        case updatedAt = "updatedAt"
        case createdAt = "createdAt"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        profilePic = try values.decodeIfPresent(String.self, forKey: .profilePic)
        isAdmin = try values.decodeIfPresent(Bool.self, forKey: .isAdmin)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        dob = try values.decodeIfPresent(String.self, forKey: .dob)
        feeling = try values.decodeIfPresent(String.self, forKey: .feeling)
        password = try values.decodeIfPresent(String.self, forKey: .password)
        isVerified = try values.decodeIfPresent(Bool.self, forKey: .isVerified)
        verifyToken = try values.decodeIfPresent(String.self, forKey: .verifyToken)
        updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt)
        createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt)
    }

}

