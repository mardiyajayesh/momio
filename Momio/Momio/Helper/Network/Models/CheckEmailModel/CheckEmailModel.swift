//
//  CheckEmailModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 18/10/21.
//

import Foundation
struct CheckEmailModel : Codable {
    let code : Int?
    let data : CheckEmailModelData?
    let success : Bool?
    //Error
    let errorMessage : String?
    let error : ErrorData?

    enum CodingKeys: String, CodingKey {

        case code = "code"
        case data = "data"
        case success = "success"
        //Error
        case errorMessage = "errorMessage"
        case error = "error"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        data = try values.decodeIfPresent(CheckEmailModelData.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        //Error
        errorMessage = try values.decodeIfPresent(String.self, forKey: .errorMessage)
        error = try values.decodeIfPresent(ErrorData.self, forKey: .error)
    }

}


struct CheckEmailModelData : Codable {

    
}
