//
//  SignUpStaticModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 18/10/21.
//

import Foundation

struct SignUp: Codable {
    var email: String?
    var password: String?
    var name: String?
    var dob: String?
    var profilePic: String?
    var feeling: String?
}
