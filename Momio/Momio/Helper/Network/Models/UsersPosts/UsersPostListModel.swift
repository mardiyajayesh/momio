//
//  UsersPostListModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 28/10/21.
//

import Foundation
struct UsersPostListModel : Codable {
    let code : Int?
    let data : UsersPostListData?
    let success : Bool?
    //Error
    let errorMessage : String?
    let error : ErrorData?

    enum CodingKeys: String, CodingKey {

        case code = "code"
        case data = "data"
        case success = "success"
        //Error
        case errorMessage = "errorMessage"
        case error = "error"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        data = try values.decodeIfPresent(UsersPostListData.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        //Error
        errorMessage = try values.decodeIfPresent(String.self, forKey: .errorMessage)
        error = try values.decodeIfPresent(ErrorData.self, forKey: .error)
    }
}
struct UsersPostListData : Codable {
    let posts : [Posts]?

    enum CodingKeys: String, CodingKey {

        case posts = "posts"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        posts = try values.decodeIfPresent([Posts].self, forKey: .posts)
    }

}
struct Posts : Codable {
    let createdAt : String?
    let id : Int?
    let feelingId : String?
    let feeling : String?
    let isActive : Int?
    let whatsUp : String?
    let userId : String?
    let updatedAt : String?
    let postImages : [String]?
    let user_follow : [String]?
    let likes : [String]?

    enum CodingKeys: String, CodingKey {

        case createdAt = "createdAt"
        case id = "id"
        case feelingId = "feelingId"
        case feeling = "feeling"
        case isActive = "isActive"
        case whatsUp = "whatsUp"
        case userId = "userId"
        case updatedAt = "updatedAt"
        case postImages = "postImages"
        case user_follow = "user_follow"
        case likes = "likes"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        feelingId = try values.decodeIfPresent(String.self, forKey: .feelingId)
        feeling = try values.decodeIfPresent(String.self, forKey: .feeling)
        isActive = try values.decodeIfPresent(Int.self, forKey: .isActive)
        whatsUp = try values.decodeIfPresent(String.self, forKey: .whatsUp)
        userId = try values.decodeIfPresent(String.self, forKey: .userId)
        updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt)
        postImages = try values.decodeIfPresent([String].self, forKey: .postImages)
        user_follow = try values.decodeIfPresent([String].self, forKey: .user_follow)
        likes = try values.decodeIfPresent([String].self, forKey: .likes)
    }

}



