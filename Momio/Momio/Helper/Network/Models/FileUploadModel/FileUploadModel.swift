//
//  FileUploadModel.swift
//  Momio
//
//  Created by Jayesh Mardiya on 18/10/21.
//

import Foundation
struct FileUploadModel: Codable {
    
    let code: Int?
    let data: FileUploadModelData?
    let success: Bool?

    enum CodingKeys: String, CodingKey {

        case code = "code"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        data = try values.decodeIfPresent(FileUploadModelData.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }
}

struct FileUploadModelData: Codable {
    
    let fileName: String?

    enum CodingKeys: String, CodingKey {
        case fileName = "fileName"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        fileName = try values.decodeIfPresent(String.self, forKey: .fileName)
    }
}
