//
//  PrefsDao.swift
//  VoW
//
//  Created by Jayesh Mardiya on 31/07/19.
//  Copyright © 2019 Jayesh Mardiya. All rights reserved.
//

import UIKit
import Swinject

private let K_INSTALLATION_ID = "InstallationId"
private let K_RECORDING_IS_ACTIVE = "RecordingIsActive"
private let K_LOGIN_INFO = "LoginInfo"
private let K_USER_INFO = "UserInfo"
private let K_ACCOUNT_INFO = "AccountInfo"
private let K_REMEMBER_LOGIN = "RememberLogin"
private let K_STAGING_ACTIVE = "StaggingIsActive"
private let K_FROM_VOXBOX_CONNECTION_INFO = "FromVoxBoxConnectionInfo"
private let K_MESSAGE_NAME = "MessageName"
private let K_BASE_URL = "BaseURL"
private let K_SESSION_INFO = "SessionInfo"
private let K_BOX_LIST = "VoxBoxList"
private let K_ROOM_LIST = "VoxBoxList"
private let K_BOX_ID = "VoxBoxID"
let appstoreLink = "https://itunes.apple.com/app/id1499323695"
private let K_REMEMBER_ANTMEDIA = "AntMedia"

protocol PrefsDao {

    // Recording is deActive by default
    var recordingIsActive: Bool { get set }
    
    // Remember Login is Deselect by default
    var rememberLoginIsSelected: Bool { get set }

    // Set Installation ID
    func setInstallationId(installationId: String)
    
    // Get Installation ID
    func getInstallationId() -> String
    
//    // Get UserInfo
//    func getUserInfo() -> UserInfo?
//
//    // Set UserInfo
//    func setUserInfo(userInfo: UserInfo?)

    // Get LoginInfo
    func getLoginInfo() -> LoginModel?
    
    // Set LoginInfo
    func setLoginInfo(loginInfo: LoginModel?)
    
//    // Get AccountInfo
//    func getAccountInfo() -> AccountInfo?
//
//    // Set AccountInfo
//    func setAccountInfo(accountInfo: AccountInfo?)
    
    // Clear UserInfo
    func clearLoginInfo()
    
    // Stagging URL is deActive by default
    var stagingIsActive: Bool { get set }
    
    // Set Name for send message
    func setMessageName(name: String)
    
    // Get Name for send message
    func getMessageName() -> String
    
//    // Set SessionLogs
//    func setSessionLogData(log: SessionLog)
//
//    // Get SessionLogs
//    func getSessionLogData() -> SessionLog?
//
//    // Set VoxBoxes
//    func setVoxBoxList(list: [VoxBox]?)
//
//    // Get VoxBoxes
//    func getVoxBoxList() -> [VoxBox]?
//
//    // Clear SessionLogs
//    func clearSessionLogData()
//
//    // Set Box Id name
//    func setBox(voxbox: VoxBox?)
//
//    // Get Box Id name
//    func getBox() -> VoxBox?
//
//    // Set Room List
//    func setRoomList(list: [Room]?)
//
//    // Get Room List
//    func getRoomList() -> [Room]?
//
//    // Navigate from Connection Info by default false
//    var isFromConnectionInfo: Bool { get set }
}

class PrefsDaoImpl {

    /// Initialiser
    init() {

    }
}

extension PrefsDaoImpl: PrefsDao {
    
    // Recording is deActive by default
    var recordingIsActive: Bool {
        get { return UserDefaults.standard.bool(forKey: K_RECORDING_IS_ACTIVE) }
        set { UserDefaults.standard.set(newValue, forKey: K_RECORDING_IS_ACTIVE) }
    }
    
    // Remember Login is Deselect by default
    var rememberLoginIsSelected: Bool {
        get { return UserDefaults.standard.bool(forKey: K_REMEMBER_LOGIN) }
        set { UserDefaults.standard.set(newValue, forKey: K_REMEMBER_LOGIN) }
    }
    
    // Navigate from Connection Info by default false
    var isFromConnectionInfo: Bool {
        get { return UserDefaults.standard.bool(forKey: K_FROM_VOXBOX_CONNECTION_INFO) }
        set { UserDefaults.standard.set(newValue, forKey: K_FROM_VOXBOX_CONNECTION_INFO) }
    }

    /// Set Installation Id
    func setInstallationId(installationId: String) {
        UserDefaults.standard.set(installationId, forKey: K_INSTALLATION_ID)
    }

    // Get Installation Id
    func getInstallationId() -> String {

        guard let installationId = UserDefaults.standard.object(forKey: K_INSTALLATION_ID) as? String
        else {

            let installationId  = UIDevice.current.identifierForVendor!.uuidString
            self.setInstallationId(installationId: installationId)
            return (UserDefaults.standard.object(forKey: K_INSTALLATION_ID) as? String)!
        }

        return installationId
    }
    
    func getLoginInfo() -> LoginModel? {
        let defaults = UserDefaults.standard
        if let savedLoginInfo = defaults.object(forKey: K_LOGIN_INFO) as? Data {
            let decoder = JSONDecoder()
            if let loadedLoginInfo = try? decoder.decode(LoginModel.self, from: savedLoginInfo) {
                return loadedLoginInfo
            }
        }
        
        return nil
    }
    
    func setLoginInfo(loginInfo: LoginModel?) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(loginInfo) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: K_LOGIN_INFO)
        }
    }
    
    /*func getUserInfo() -> UserInfo? {
        let defaults = UserDefaults.standard
        if let savedUserInfo = defaults.object(forKey: K_USER_INFO) as? Data {
            let decoder = JSONDecoder()
            if let loadedUserInfo = try? decoder.decode(UserInfo.self, from: savedUserInfo) {
                return loadedUserInfo
            }
        }
        
        return nil
    }
    
    func setUserInfo(userInfo: UserInfo?) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(userInfo) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: K_USER_INFO)
        }
    }
    
    func getAccountInfo() -> AccountInfo? {
        let defaults = UserDefaults.standard
        if let savedAccountInfo = defaults.object(forKey: K_ACCOUNT_INFO) as? Data {
            let decoder = JSONDecoder()
            if let loadedAccountInfo = try? decoder.decode(AccountInfo.self, from: savedAccountInfo) {
                return loadedAccountInfo
            }
        }
        
        return nil
    }
    
    func setAccountInfo(accountInfo: AccountInfo?) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(accountInfo) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: K_ACCOUNT_INFO)
        }
    }*/
    
    func clearLoginInfo() {
        let defaults = UserDefaults.standard
        defaults.set(nil, forKey: K_LOGIN_INFO)
    }
    
    // Staging URL is deActive by default
    var stagingIsActive: Bool {
        get { return UserDefaults.standard.bool(forKey: K_STAGING_ACTIVE) }
        set { UserDefaults.standard.set(newValue, forKey: K_STAGING_ACTIVE) }
    }
    
    /// Set Name for send message
    func setMessageName(name: String) {
        UserDefaults.standard.set(name, forKey: K_MESSAGE_NAME)
    }

    /// Get Name for send message
    func getMessageName() -> String {

        guard let name = UserDefaults.standard.object(forKey: K_MESSAGE_NAME) as? String
        else {
            return ""
        }
        return name
    }
    
    /*/// Set Box ID
    func setBox(voxbox: VoxBox?) {
        
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(voxbox) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: K_BOX_ID)
        }
    }
    
    /// Get Box ID
    func getBox() -> VoxBox? {
        
        let defaults = UserDefaults.standard
        if let savedVoxBox = defaults.object(forKey: K_BOX_ID) as? Data {
            let decoder = JSONDecoder()
            if let loadedVoxBox = try? decoder.decode(VoxBox.self, from: savedVoxBox) {
                return loadedVoxBox
            }
        }
        
        return nil
    }
    
    /// Set SessionLogs
    func setSessionLogData(log: SessionLog) {
        
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(log) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: K_SESSION_INFO)
        }
    }
    
    /// Get SessionLogs
    func getSessionLogData() -> SessionLog? {
        let defaults = UserDefaults.standard
        if let savedSessionData = defaults.object(forKey: K_SESSION_INFO) as? Data {
            let decoder = JSONDecoder()
            if let loadedSessionData = try? decoder.decode(SessionLog.self, from: savedSessionData) {
                return loadedSessionData
            }
        }
        
        return nil
    }*/
    
    /// Clear SessionLogData
    func clearSessionLogData() {
        let defaults = UserDefaults.standard
        defaults.set(nil, forKey: K_SESSION_INFO)
        defaults.removeObject(forKey: K_SESSION_INFO)
        defaults.synchronize()
    }
    
    /*// Get VoxBoxes
    func getVoxBoxList() -> [VoxBox]? {
        let defaults = UserDefaults.standard
        if let savedVoxboxs = defaults.object(forKey: K_BOX_LIST) as? Data {
            let decoder = JSONDecoder()
            if let loadedVoxboxes = try? decoder.decode([VoxBox].self, from: savedVoxboxs) {
                return loadedVoxboxes
            }
        }
        
        return nil
    }
    
    // Set Room List
    func setRoomList(list: [Room]?) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(list) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: K_ROOM_LIST)
        }
    }
    
    // Get Room List
    func getRoomList() -> [Room]? {
        let defaults = UserDefaults.standard
        if let savedRooms = defaults.object(forKey: K_ROOM_LIST) as? Data {
            let decoder = JSONDecoder()
            if let loadedRooms = try? decoder.decode([Room].self, from: savedRooms) {
                return loadedRooms
            }
        }
        
        return nil
    }
    
    // Set VoxBoxes
    func setVoxBoxList(list: [VoxBox]?) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(list) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: K_BOX_LIST)
        }
    }*/
}

// MARK: - Swinject Assembly -
class PrefsDaoAssembly: Assembly {

    func assemble(container: Container) {

        container.register(PrefsDao.self) { _ in
            PrefsDaoImpl()
        }.inObjectScope(.container)
    }
}
/*
extension UserDefaults {
    
    // Get VoxBoxes
    func getVoxBoxList() -> [VoxBox]? {
        let defaults = UserDefaults.standard
        if let savedVoxboxs = defaults.object(forKey: K_BOX_LIST) as? Data {
            let decoder = JSONDecoder()
            if let loadedVoxboxes = try? decoder.decode([VoxBox].self, from: savedVoxboxs) {
                return loadedVoxboxes
            }
        }
        
        return nil
    }
    
    // Set VoxBoxes
    func setVoxBoxList(list: [VoxBox]?) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(list) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: K_BOX_LIST)
        }
    }
}*/
/*
extension UserDefaults {
    
    private struct Keys {
        static let baseURL = "baseURL"
    }
    
    /// Set BaseURL
    func setBaseURL(url: String) {
        UserDefaults.standard.set(url, forKey: Keys.baseURL)
    }

    /// Get BaseURL
    func getBaseURL() -> String {

        guard let baseURL = UserDefaults.standard.object(forKey: Keys.baseURL) as? String
        else {
            let baseURL: String = ""
            self.setBaseURL(url: baseURL)
            return UserDefaults.standard.string(forKey: Keys.baseURL)!
        }

        return baseURL
    }
}*/
