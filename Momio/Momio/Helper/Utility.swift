//
//  Utility.swift
//  Airbuk
//
//  Created by Ebenezer Eshun on 05/04/17.
//

import UIKit
import SystemConfiguration

enum UIUserInterfaceIdiom: Int {
    case unspecified
    case phone // iPhone and iPod touch style UI
    case pad // iPad style UI
}

protocol Utilities {
}

class Utility: NSObject {
    
    @objc static let sharedInstance = Utility()
    
    @objc func RGBColor(r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor {
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: 1.0)
    }
    
    @objc func convertDateToString (date: Date, originalFormat: String, convertFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = convertFormat
        let dateObj = dateFormatter.string(from: date)
        return dateObj
    }
    
    @objc func setUserDefault(_ obj: Any, _ key: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(obj, forKey: key)
        userDefaults.synchronize()
    }
    
    @objc func getUserDefault(_ key: String) -> Any? {
        let userDefaults = UserDefaults.standard
        if let tmpValue = userDefaults.object(forKey: key) {
            return tmpValue
        } else {
            return nil
        }
    }
    
    @objc func getUserDefaultBool(_ key: String) -> Bool {
        let userDefaults = UserDefaults.standard
        if let flag = userDefaults.object(forKey: key) {
            return flag as! Bool
        }
        return false
    }
    
}

extension Utility {
    
    
    enum ReachabilityStatus {
        case notReachable
        case reachableViaWWAN
        case reachableViaWiFi
    }
    
    class var currentReachabilityStatus: ReachabilityStatus {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return .notReachable
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return .notReachable
        }
        
        if flags.contains(.reachable) == false {
            // The target host is not reachable.
            return .notReachable
        }
        else if flags.contains(.isWWAN) == true {
            // WWAN connections are OK if the calling application is using the CFNetwork APIs.
            return .reachableViaWWAN
        }
        else if flags.contains(.connectionRequired) == false {
            // If the target host is reachable and no connection is required then we'll assume that you're on Wi-Fi...
            return .reachableViaWiFi
        }
        else if (flags.contains(.connectionOnDemand) == true || flags.contains(.connectionOnTraffic) == true) && flags.contains(.interventionRequired) == false {
            // The connection is on-demand (or on-traffic) if the calling application is using the CFSocketStream or higher APIs and no [user] intervention is needed
            return .reachableViaWiFi
        }
        else {
            return .notReachable
        }
    }
}

public class Log {
    
    // MARK: Enum for message event
    public enum LogEvent: String {
        case info           = "[ℹ️]"
        case warning        = "[⚠️]"
        case error          = "[🛑]"
        case success        = "[✅]"
        case nullPointer    = "[❓]"
        case emptyArray     = "[📭]"
    }
    
    /// Hide initialiser
    @available(*, unavailable)
    init() {}
    
    /// Class function to print our log message in DEBUG mode only
    /// - parameter message: [Required]: The custom message to be printed
    /// - parameter event: [Required]: The event of the message (default is `.info`)
    /// - parameter fileName: [Optional]: The name of the file (default is last component of `#file` path)
    /// - parameter line: [Optional]: The number of the line where the log is placed (default is `#line`)
    /// - parameter funcName: [Optional]: The name of the function where the log is placed in (default is `#function`)
    public class func debug(message: String,
                            event: LogEvent,
                            fileName: String      = #file,
                            line: Int         = #line,
                            funcName: String      = #function) {
        /// Print message
        #if DEBUG
        print("\(event.rawValue)")
        print("[\(Log.dateAsString())]")
        print("[\(Log.fileName(path: fileName)):\(line)]")
        print("[\(funcName)] → \"\(message)\"")
        #endif
    }
    
    /// Class function to print our log message both in DEBUG and PRODUCTION mode
    /// - parameter message: [Required]: The custom message to be printed
    /// - parameter event: [Required]: The event of the message (default is `.info`)
    /// - parameter fileName: [Optional]: The name of the file (default is last component of `#file` path)
    /// - parameter line: [Optional]: The number of the line where the log is placed (default is `#line`)
    /// - parameter funcName: [Optional]: The name of the function where the log is placed in (default is `#function`)
    public class func prod(message: String,
                           event: LogEvent,
                           fileName: String      = #file,
                           line: Int         = #line,
                           funcName: String      = #function) {
        /// Print message
        print("\(event.rawValue)")
        print("[\(Log.dateAsString())]")
        print("[\(Log.fileName(path: fileName)):\(line)]")
        print("[\(funcName)] → \"\(message)\"")
    }
    
    /// Class function to print a `fatalError` with custom message and event
    /// - parameter message: [Required]: The custom message to be printed
    /// - parameter event: [Required]: The event of the message (default is `.info`)
    /// - parameter fileName: [Optional]: The name of the file (default is last component of `#file` path)
    /// - parameter line: [Optional]: The number of the line where the log is placed (default is `#line`)
    /// - parameter funcName: [Optional]: The name of the function where the log is placed in (default is `#function`)
    public class func fatalError(message: String,
                                 event: LogEvent,
                                 fileName: String = #file,
                                 line: Int = #line,
                                 funcName: String = #function) -> Never {
        /// Print message
        Swift.fatalError("""
                        \(event.rawValue)
                        [\(Log.dateAsString())]
                        [\(Log.fileName(path: fileName)):\(line)]
                        [\(funcName)] → \"\(message)\"
                        """)
    }
    
    // MARK: Private vars
    // Create date formatter
    static var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        formatter.locale = Locale.current
        formatter.timeZone = TimeZone.current
        return formatter
    }
    
    // MARK: Private functions
    ///
    /// Get `Date()` formatted as `String`
    private class func dateAsString() -> String {
        return Log.dateFormatter.string(from: Date())
    }
    
    /// Get filename
    private class func fileName(path: String?) -> String {
        let components = (path ?? #file).components(separatedBy: "/")
        return components.isEmpty ? "": components.last!
    }
}
